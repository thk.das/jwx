![logojwx2](https://gitlab.com/thk.imp/jwx/uploads/e40b7cbd53dd9f07ae198c7781c84dbb/logojwx2.jpg) 

JWx is a Java library for JSON Object Signing and Encryption (JOSE).It is an easy to use API for signing and encryption of arbitrary data, especially JSON structures with JWS and JWE respectively. It is so far the only implementation that supports all serializations specified in the current working drafts.

For examples and more, [see the wiki] (https://gitlab.com/thk.das/jwx/wikis/home)

Please visit https://das.th-koeln.de/developments to access to the binaries (JARs).