/*
 * Copyright 2015 Peter Leo Gorski, Luigi Lo Iacono, Daniel Behnam Torkian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fhk.jwx.cookbook;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.security.Security;
import java.security.interfaces.ECPrivateKey;
import java.security.interfaces.ECPublicKey;
import java.util.List;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.Before;
import org.junit.Test;

import de.fhk.jwx.jwa.JwaAlgorithms;
import de.fhk.jwx.jwe.JweDocument;
import de.fhk.jwx.jwe.JweMaker;
import de.fhk.jwx.jwe.JwePerRecipientUnprotectedHeader;
import de.fhk.jwx.jwe.JweProtectedHeader;
import de.fhk.jwx.jwe.JweRecipient;
import de.fhk.jwx.jwe.JweSharedUnprotectedHeader;
import de.fhk.jwx.jwe.crypto.JweCookbookBypass;
import de.fhk.jwx.jwk.JwkKey;
import de.fhk.jwx.jwk.JwkKeySet;
import de.fhk.jwx.util.JoseUtils;
import de.fhk.jwx.util.JwkUtils;

/**
 * Testing the: Internet Engineering Task Force (IETF) Request for Comments:
 * 7520 Examples of Protecting Content Using JSON Object Signing and Encryption
 * (JOSE) Chapter 5. JSON Web Encryption Examples
 */
public class JweJoseCookBookTests {

	// Figure 72: Plaintext content
	private static final String FIGURE72 = "You can trust us to stick with you through thick and "
			+ "thin–to the bitter end. And you can trust us to "
			+ "keep any secret of yours–closer than you keep it "
			+ "yourself. But you cannot trust us to let you face trouble "
			+ "alone, and go off without a word. We are your friends, Frodo.";

	// Figure 74: Content Encryption Key, base64url-encoded
	private static final String FIGURE74 = "3qyTVhIWt5juqZUCpfRqpvauwB956MEJL2Rt-8qXKSo";

	// Figure 75: Initialization Vector, base64url-encoded
	private static final String FIGURE75 = "bbd5sTkYwhAIqfHsx8DayA";

	// Figure 81: Compact Serialization
	private static final String FIGURE81 = 
			  "eyJhbGciOiJSU0ExXzUiLCJraWQiOiJmcm9kby5iYWdnaW5zQGhvYmJpdG9uLm"
			+ "V4YW1wbGUiLCJlbmMiOiJBMTI4Q0JDLUhTMjU2In0"
			+ "."
			+ "laLxI0j-nLH-_BgLOXMozKxmy9gffy2gTdvqzfTihJBuuzxg0V7yk1WClnQePF"
			+ "vG2K-pvSlWc9BRIazDrn50RcRai__3TDON395H3c62tIouJJ4XaRvYHFjZTZ2G"
			+ "Xfz8YAImcc91Tfk0WXC2F5Xbb71ClQ1DDH151tlpH77f2ff7xiSxh9oSewYrcG"
			+ "TSLUeeCt36r1Kt3OSj7EyBQXoZlN7IxbyhMAfgIe7Mv1rOTOI5I8NQqeXXW8Vl"
			+ "zNmoxaGMny3YnGir5Wf6Qt2nBq4qDaPdnaAuuGUGEecelIO1wx1BpyIfgvfjOh"
			+ "MBs9M8XL223Fg47xlGsMXdfuY-4jaqVw"
			+ "."
			+ "bbd5sTkYwhAIqfHsx8DayA"
			+ "."
			+ "0fys_TY_na7f8dwSfXLiYdHaA2DxUjD67ieF7fcVbIR62JhJvGZ4_FNVSiGc_r"
			+ "aa0HnLQ6s1P2sv3Xzl1p1l_o5wR_RsSzrS8Z-wnI3Jvo0mkpEEnlDmZvDu_k8O"
			+ "WzJv7eZVEqiWKdyVzFhPpiyQU28GLOpRc2VbVbK4dQKPdNTjPPEmRqcaGeTWZV"
			+ "yeSUvf5k59yJZxRuSvWFf6KrNtmRdZ8R4mDOjHSrM_s8uwIFcqt4r5GX8TKaI0"
			+ "zT5CbL5Qlw3sRc7u_hg0yKVOiRytEAEs3vZkcfLkP6nbXdC_PkMdNS-ohP78T2"
			+ "O6_7uInMGhFeX4ctHG7VelHGiT93JfWDEQi5_V9UN1rhXNrYu-0fVMkZAKX3VW"
			+ "i7lzA6BP430m" + "." + "kvKuFBXHe5mQr4lqgobAUg";

	// Figure 82: JSON General Serialization
	private static final String FIGURE82 = 
			"{" 
	        + "\"recipients\":[" 
			+ "{"
			+ "\"encrypted_key\":\"laLxI0j-nLH-_BgLOXMozKxmy9gffy2gTdvqzf"
			+ "TihJBuuzxg0V7yk1WClnQePFvG2K-pvSlWc9BRIazDrn50RcRai_"
			+ "_3TDON395H3c62tIouJJ4XaRvYHFjZTZ2GXfz8YAImcc91Tfk0WX"
			+ "C2F5Xbb71ClQ1DDH151tlpH77f2ff7xiSxh9oSewYrcGTSLUeeCt"
			+ "36r1Kt3OSj7EyBQXoZlN7IxbyhMAfgIe7Mv1rOTOI5I8NQqeXXW8"
			+ "VlzNmoxaGMny3YnGir5Wf6Qt2nBq4qDaPdnaAuuGUGEecelIO1wx"
			+ "1BpyIfgvfjOhMBs9M8XL223Fg47xlGsMXdfuY-4jaqVw\"" 
			+ "}" 
			+ "],"
			+ "\"protected\":\"eyJhbGciOiJSU0ExXzUiLCJraWQiOiJmcm9kby5iYWdnaW"
			+ "5zQGhvYmJpdG9uLmV4YW1wbGUiLCJlbmMiOiJBMTI4Q0JDLUhTMjU2In"
			+ "0\"," 
			+ "\"iv\":\"bbd5sTkYwhAIqfHsx8DayA\","
			+ "\"ciphertext\":\"0fys_TY_na7f8dwSfXLiYdHaA2DxUjD67ieF7fcVbIR62"
			+ "JhJvGZ4_FNVSiGc_raa0HnLQ6s1P2sv3Xzl1p1l_o5wR_RsSzrS8Z-wn"
			+ "I3Jvo0mkpEEnlDmZvDu_k8OWzJv7eZVEqiWKdyVzFhPpiyQU28GLOpRc"
			+ "2VbVbK4dQKPdNTjPPEmRqcaGeTWZVyeSUvf5k59yJZxRuSvWFf6KrNtm"
			+ "RdZ8R4mDOjHSrM_s8uwIFcqt4r5GX8TKaI0zT5CbL5Qlw3sRc7u_hg0y"
			+ "KVOiRytEAEs3vZkcfLkP6nbXdC_PkMdNS-ohP78T2O6_7uInMGhFeX4c"
			+ "tHG7VelHGiT93JfWDEQi5_V9UN1rhXNrYu-0fVMkZAKX3VWi7lzA6BP4"
			+ "30m\"," 
			+ "\"tag\":\"kvKuFBXHe5mQr4lqgobAUg\"" 
			+ "}";

	// Figure 83: Flattened JWE JSON Serialization
	private static final String FIGURE83 = 
			  "{"
			+ "\"protected\":\"eyJhbGciOiJSU0ExXzUiLCJraWQiOiJmcm9kby5iYWdnaW"
			+ "5zQGhvYmJpdG9uLmV4YW1wbGUiLCJlbmMiOiJBMTI4Q0JDLUhTMjU2In"
			+ "0\","
			+ "\"encrypted_key\":\"laLxI0j-nLH-_BgLOXMozKxmy9gffy2gTdvqzfTihJ"
			+ "Buuzxg0V7yk1WClnQePFvG2K-pvSlWc9BRIazDrn50RcRai__3TDON39"
			+ "5H3c62tIouJJ4XaRvYHFjZTZ2GXfz8YAImcc91Tfk0WXC2F5Xbb71ClQ"
			+ "1DDH151tlpH77f2ff7xiSxh9oSewYrcGTSLUeeCt36r1Kt3OSj7EyBQX"
			+ "oZlN7IxbyhMAfgIe7Mv1rOTOI5I8NQqeXXW8VlzNmoxaGMny3YnGir5W"
			+ "f6Qt2nBq4qDaPdnaAuuGUGEecelIO1wx1BpyIfgvfjOhMBs9M8XL223F"
			+ "g47xlGsMXdfuY-4jaqVw\"," 
			+ "\"iv\":\"bbd5sTkYwhAIqfHsx8DayA\","
			+ "\"ciphertext\":\"0fys_TY_na7f8dwSfXLiYdHaA2DxUjD67ieF7fcVbIR62"
			+ "JhJvGZ4_FNVSiGc_raa0HnLQ6s1P2sv3Xzl1p1l_o5wR_RsSzrS8Z-wn"
			+ "I3Jvo0mkpEEnlDmZvDu_k8OWzJv7eZVEqiWKdyVzFhPpiyQU28GLOpRc"
			+ "2VbVbK4dQKPdNTjPPEmRqcaGeTWZVyeSUvf5k59yJZxRuSvWFf6KrNtm"
			+ "RdZ8R4mDOjHSrM_s8uwIFcqt4r5GX8TKaI0zT5CbL5Qlw3sRc7u_hg0y"
			+ "KVOiRytEAEs3vZkcfLkP6nbXdC_PkMdNS-ohP78T2O6_7uInMGhFeX4c"
			+ "tHG7VelHGiT93JfWDEQi5_V9UN1rhXNrYu-0fVMkZAKX3VWi7lzA6BP4"
			+ "30m\"," 
			+ "\"tag\":\"kvKuFBXHe5mQr4lqgobAUg\"" 
			+ "}";

	// Figure 85: Content Encryption Key, base64url-encoded
	private static final String FIGURE85 = "mYMfsggkTAm0TbvtlFh2hyoXnbEzJQjMxmgLN3d8xXA";

	// Figure 86: Initialization Vector, base64url-encoded
	private static final String FIGURE86 = "-nBoKLH0YkLZPSI9";

	// Figure 92: JWE Compact Serialization
	private static final String FIGURE92 = 
			  "eyJhbGciOiJSU0EtT0FFUCIsImtpZCI6InNhbXdpc2UuZ2FtZ2VlQGhvYmJpdG"
			+ "9uLmV4YW1wbGUiLCJlbmMiOiJBMjU2R0NNIn0"
			+ "."
			+ "rT99rwrBTbTI7IJM8fU3Eli7226HEB7IchCxNuh7lCiud48LxeolRdtFF4nzQi"
			+ "beYOl5S_PJsAXZwSXtDePz9hk-BbtsTBqC2UsPOdwjC9NhNupNNu9uHIVftDyu"
			+ "cvI6hvALeZ6OGnhNV4v1zx2k7O1D89mAzfw-_kT3tkuorpDU-CpBENfIHX1Q58"
			+ "-Aad3FzMuo3Fn9buEP2yXakLXYa15BUXQsupM4A1GD4_H4Bd7V3u9h8Gkg8Bpx"
			+ "KdUV9ScfJQTcYm6eJEBz3aSwIaK4T3-dwWpuBOhROQXBosJzS1asnuHtVMt2pK"
			+ "IIfux5BC6huIvmY7kzV7W7aIUrpYm_3H4zYvyMeq5pGqFmW2k8zpO878TRlZx7"
			+ "pZfPYDSXZyS0CfKKkMozT_qiCwZTSz4duYnt8hS4Z9sGthXn9uDqd6wycMagnQ"
			+ "fOTs_lycTWmY-aqWVDKhjYNRf03NiwRtb5BE-tOdFwCASQj3uuAgPGrO2AWBe3"
			+ "8UjQb0lvXn1SpyvYZ3WFc7WOJYaTa7A8DRn6MC6T-xDmMuxC0G7S2rscw5lQQU"
			+ "06MvZTlFOt0UvfuKBa03cxA_nIBIhLMjY2kOTxQMmpDPTr6Cbo8aKaOnx6ASE5"
			+ "Jx9paBpnNmOOKH35j_QlrQhDWUN6A2Gg8iFayJ69xDEdHAVCGRzN3woEI2ozDR"
			+ "s"
			+ "."
			+ "-nBoKLH0YkLZPSI9"
			+ "."
			+ "o4k2cnGN8rSSw3IDo1YuySkqeS_t2m1GXklSgqBdpACm6UJuJowOHC5ytjqYgR"
			+ "L-I-soPlwqMUf4UgRWWeaOGNw6vGW-xyM01lTYxrXfVzIIaRdhYtEMRBvBWbEw"
			+ "P7ua1DRfvaOjgZv6Ifa3brcAM64d8p5lhhNcizPersuhw5f-pGYzseva-TUaL8"
			+ "iWnctc-sSwy7SQmRkfhDjwbz0fz6kFovEgj64X1I5s7E6GLp5fnbYGLa1QUiML"
			+ "7Cc2GxgvI7zqWo0YIEc7aCflLG1-8BboVWFdZKLK9vNoycrYHumwzKluLWEbSV"
			+ "maPpOslY2n525DxDfWaVFUfKQxMF56vn4B9QMpWAbnypNimbM8zVOw"
			+ "."
			+ "UCGiqJxhBI3IFVdPalHHvA";

	// Figure 93: JSON General Serialization
	private static final String FIGURE93 = 
			  "{" 
	        + "\"recipients\":[" 
	        + "{"
			+ "\"encrypted_key\":\"rT99rwrBTbTI7IJM8fU3Eli7226HEB7IchCxNu"
			+ "h7lCiud48LxeolRdtFF4nzQibeYOl5S_PJsAXZwSXtDePz9hk-Bb"
			+ "tsTBqC2UsPOdwjC9NhNupNNu9uHIVftDyucvI6hvALeZ6OGnhNV4"
			+ "v1zx2k7O1D89mAzfw-_kT3tkuorpDU-CpBENfIHX1Q58-Aad3FzM"
			+ "uo3Fn9buEP2yXakLXYa15BUXQsupM4A1GD4_H4Bd7V3u9h8Gkg8B"
			+ "pxKdUV9ScfJQTcYm6eJEBz3aSwIaK4T3-dwWpuBOhROQXBosJzS1"
			+ "asnuHtVMt2pKIIfux5BC6huIvmY7kzV7W7aIUrpYm_3H4zYvyMeq"
			+ "5pGqFmW2k8zpO878TRlZx7pZfPYDSXZyS0CfKKkMozT_qiCwZTSz"
			+ "4duYnt8hS4Z9sGthXn9uDqd6wycMagnQfOTs_lycTWmY-aqWVDKh"
			+ "jYNRf03NiwRtb5BE-tOdFwCASQj3uuAgPGrO2AWBe38UjQb0lvXn"
			+ "1SpyvYZ3WFc7WOJYaTa7A8DRn6MC6T-xDmMuxC0G7S2rscw5lQQU"
			+ "06MvZTlFOt0UvfuKBa03cxA_nIBIhLMjY2kOTxQMmpDPTr6Cbo8a"
			+ "KaOnx6ASE5Jx9paBpnNmOOKH35j_QlrQhDWUN6A2Gg8iFayJ69xD"
			+ "EdHAVCGRzN3woEI2ozDRs\"" 
			+ "}" 
			+ "],"
			+ "\"protected\":\"eyJhbGciOiJSU0EtT0FFUCIsImtpZCI6InNhbXdpc2UuZ2"
			+ "FtZ2VlQGhvYmJpdG9uLmV4YW1wbGUiLCJlbmMiOiJBMjU2R0NNIn0\","
			+ "\"iv\":\"-nBoKLH0YkLZPSI9\","
			+ "\"ciphertext\":\"o4k2cnGN8rSSw3IDo1YuySkqeS_t2m1GXklSgqBdpACm6"
			+ "UJuJowOHC5ytjqYgRL-I-soPlwqMUf4UgRWWeaOGNw6vGW-xyM01lTYx"
			+ "rXfVzIIaRdhYtEMRBvBWbEwP7ua1DRfvaOjgZv6Ifa3brcAM64d8p5lh"
			+ "hNcizPersuhw5f-pGYzseva-TUaL8iWnctc-sSwy7SQmRkfhDjwbz0fz"
			+ "6kFovEgj64X1I5s7E6GLp5fnbYGLa1QUiML7Cc2GxgvI7zqWo0YIEc7a"
			+ "CflLG1-8BboVWFdZKLK9vNoycrYHumwzKluLWEbSVmaPpOslY2n525Dx"
			+ "DfWaVFUfKQxMF56vn4B9QMpWAbnypNimbM8zVOw\","
			+ "\"tag\":\"UCGiqJxhBI3IFVdPalHHvA\"" 
			+ "}";

	// Figure 94: Flattened JWE JSON Serialization
	private static final String FIGURE94 = 
			  "{"
			+ "\"protected\":\"eyJhbGciOiJSU0EtT0FFUCIsImtpZCI6InNhbXdpc2UuZ2"
			+ "FtZ2VlQGhvYmJpdG9uLmV4YW1wbGUiLCJlbmMiOiJBMjU2R0NNIn0\","
			+ "\"encrypted_key\":\"rT99rwrBTbTI7IJM8fU3Eli7226HEB7IchCxNuh7lC"
			+ "iud48LxeolRdtFF4nzQibeYOl5S_PJsAXZwSXtDePz9hk-BbtsTBqC2U"
			+ "sPOdwjC9NhNupNNu9uHIVftDyucvI6hvALeZ6OGnhNV4v1zx2k7O1D89"
			+ "mAzfw-_kT3tkuorpDU-CpBENfIHX1Q58-Aad3FzMuo3Fn9buEP2yXakL"
			+ "XYa15BUXQsupM4A1GD4_H4Bd7V3u9h8Gkg8BpxKdUV9ScfJQTcYm6eJE"
			+ "Bz3aSwIaK4T3-dwWpuBOhROQXBosJzS1asnuHtVMt2pKIIfux5BC6huI"
			+ "vmY7kzV7W7aIUrpYm_3H4zYvyMeq5pGqFmW2k8zpO878TRlZx7pZfPYD"
			+ "SXZyS0CfKKkMozT_qiCwZTSz4duYnt8hS4Z9sGthXn9uDqd6wycMagnQ"
			+ "fOTs_lycTWmY-aqWVDKhjYNRf03NiwRtb5BE-tOdFwCASQj3uuAgPGrO"
			+ "2AWBe38UjQb0lvXn1SpyvYZ3WFc7WOJYaTa7A8DRn6MC6T-xDmMuxC0G"
			+ "7S2rscw5lQQU06MvZTlFOt0UvfuKBa03cxA_nIBIhLMjY2kOTxQMmpDP"
			+ "Tr6Cbo8aKaOnx6ASE5Jx9paBpnNmOOKH35j_QlrQhDWUN6A2Gg8iFayJ"
			+ "69xDEdHAVCGRzN3woEI2ozDRs\"," 
			+ "\"iv\":\"-nBoKLH0YkLZPSI9\","
			+ "\"ciphertext\":\"o4k2cnGN8rSSw3IDo1YuySkqeS_t2m1GXklSgqBdpACm6"
			+ "UJuJowOHC5ytjqYgRL-I-soPlwqMUf4UgRWWeaOGNw6vGW-xyM01lTYx"
			+ "rXfVzIIaRdhYtEMRBvBWbEwP7ua1DRfvaOjgZv6Ifa3brcAM64d8p5lh"
			+ "hNcizPersuhw5f-pGYzseva-TUaL8iWnctc-sSwy7SQmRkfhDjwbz0fz"
			+ "6kFovEgj64X1I5s7E6GLp5fnbYGLa1QUiML7Cc2GxgvI7zqWo0YIEc7a"
			+ "CflLG1-8BboVWFdZKLK9vNoycrYHumwzKluLWEbSVmaPpOslY2n525Dx"
			+ "DfWaVFUfKQxMF56vn4B9QMpWAbnypNimbM8zVOw\","
			+ "\"tag\":\"UCGiqJxhBI3IFVdPalHHvA\"" 
			+ "}";

	// Figure 95: Plaintext Content
	private static final String FIGURE95 = 
			"{" 
	        + "\"keys\":[" 
			+ "{"
			+ "\"kty\":\"oct\","
			+ "\"kid\":\"77c7e2b8-6e13-45cf-8672-617b5b45243a\","
			+ "\"use\":\"enc\"," 
			+ "\"alg\":\"A128GCM\","
			+ "\"k\":\"XctOhJAkA-pD9Lh7ZgW_2A\"" 
			+ "}," 
			+ "{"
			+ "\"kty\":\"oct\","
			+ "\"kid\":\"81b20965-8332-43d9-a468-82160ad91ac8\","
			+ "\"use\":\"enc\"," 
			+ "\"alg\":\"A128KW\","
			+ "\"k\":\"GZy6sIZ6wl9NJOKB-jnmVQ\"" 
			+ "}," 
			+ "{"
			+ "\"kty\":\"oct\","
			+ "\"kid\":\"18ec08e1-bfa9-4d95-b205-2b4dd1d4321d\","
			+ "\"use\":\"enc\"," 
			+ "\"alg\":\"A256GCMKW\","
			+ "\"k\":\"qC57l_uxcm7Nm3K-ct4GFjx8tM1U8CZ0NLBvdQstiS8\"" 
			+ "}"
			+ "]" 
			+ "}";

	// Figure 96: Password
	private static final String FIGURE96 = "entrap_o" + "\u2013" + "peter_long" + "\u2013" + "credit_tun";
//	private static final String FIGURE96 = "entrap_o\xe2\x80\x93peter_long\xe2\x80\x93credit_tun";
	

	
	
	// Figure 97: Content Encryption Key, base64url-encoded
	private static final String FIGURE97 = "uwsjJXaBK407Qaf0_zpcpmr1Cs0CC50hIUEyGNEt3m0";

	// Figure 98: Initialization Vector, base64url-encoded
	private static final String FIGURE98 = "VBiCzVHNoLiR3F4V82uoTQ";

	// Figure 99: Salt, base64url-encoded
	private static final String FIGURE99 = "8Q1SzinasR3xchYz6ZZcHA";

	// Figure 105: Compact Serialization
	private static final String FIGURE105 = 
			  "eyJhbGciOiJQQkVTMi1IUzUxMitBMjU2S1ciLCJwMnMiOiI4UTFTemluYXNSM3"
			+ "hjaFl6NlpaY0hBIiwicDJjIjo4MTkyLCJjdHkiOiJqd2stc2V0K2pzb24iLCJl"
			+ "bmMiOiJBMTI4Q0JDLUhTMjU2In0"
			+ ".d3qNhUWfqheyPp4H8sjOWsDYajoej4c5Je6rlUtFPWdgtURtmeDV1g"
			+ "."
			+ "VBiCzVHNoLiR3F4V82uoTQ"
			+ "."
			+ "23i-Tb1AV4n0WKVSSgcQrdg6GRqsUKxjruHXYsTHAJLZ2nsnGIX86vMXqIi6IR"
			+ "sfywCRFzLxEcZBRnTvG3nhzPk0GDD7FMyXhUHpDjEYCNA_XOmzg8yZR9oyjo6l"
			+ "TF6si4q9FZ2EhzgFQCLO_6h5EVg3vR75_hkBsnuoqoM3dwejXBtIodN84PeqMb"
			+ "6asmas_dpSsz7H10fC5ni9xIz424givB1YLldF6exVmL93R3fOoOJbmk2GBQZL"
			+ "_SEGllv2cQsBgeprARsaQ7Bq99tT80coH8ItBjgV08AtzXFFsx9qKvC982KLKd"
			+ "PQMTlVJKkqtV4Ru5LEVpBZXBnZrtViSOgyg6AiuwaS-rCrcD_ePOGSuxvgtrok"
			+ "AKYPqmXUeRdjFJwafkYEkiuDCV9vWGAi1DH2xTafhJwcmywIyzi4BqRpmdn_N-"
			+ "zl5tuJYyuvKhjKv6ihbsV_k1hJGPGAxJ6wUpmwC4PTQ2izEm0TuSE8oMKdTw8V"
			+ "3kobXZ77ulMwDs4p" 
			+ "." 
			+ "0HlwodAhOCILG5SQ2LQ9dg";

	// Figure 106: JSON General Serialization
	private static final String FIGURE106 = 
			  "{" 
			+ "\"recipients\":[" 
			+ "{"
			+ "\"encrypted_key\":\"d3qNhUWfqheyPp4H8sjOWsDYajoej4c5Je6rlU"
			+ "tFPWdgtURtmeDV1g\"" 
			+ "}" 
			+ "],"
			+ "\"protected\":\"eyJhbGciOiJQQkVTMi1IUzUxMitBMjU2S1ciLCJwMnMiOi"
			+ "I4UTFTemluYXNSM3hjaFl6NlpaY0hBIiwicDJjIjo4MTkyLCJjdHkiOi"
			+ "Jqd2stc2V0K2pzb24iLCJlbmMiOiJBMTI4Q0JDLUhTMjU2In0\","
			+ "\"iv\":\"VBiCzVHNoLiR3F4V82uoTQ\","
			+ "\"ciphertext\":\"23i-Tb1AV4n0WKVSSgcQrdg6GRqsUKxjruHXYsTHAJLZ2"
			+ "nsnGIX86vMXqIi6IRsfywCRFzLxEcZBRnTvG3nhzPk0GDD7FMyXhUHpD"
			+ "jEYCNA_XOmzg8yZR9oyjo6lTF6si4q9FZ2EhzgFQCLO_6h5EVg3vR75_"
			+ "hkBsnuoqoM3dwejXBtIodN84PeqMb6asmas_dpSsz7H10fC5ni9xIz42"
			+ "4givB1YLldF6exVmL93R3fOoOJbmk2GBQZL_SEGllv2cQsBgeprARsaQ"
			+ "7Bq99tT80coH8ItBjgV08AtzXFFsx9qKvC982KLKdPQMTlVJKkqtV4Ru"
			+ "5LEVpBZXBnZrtViSOgyg6AiuwaS-rCrcD_ePOGSuxvgtrokAKYPqmXUe"
			+ "RdjFJwafkYEkiuDCV9vWGAi1DH2xTafhJwcmywIyzi4BqRpmdn_N-zl5"
			+ "tuJYyuvKhjKv6ihbsV_k1hJGPGAxJ6wUpmwC4PTQ2izEm0TuSE8oMKdT"
			+ "w8V3kobXZ77ulMwDs4p\"," 
			+ "\"tag\":\"0HlwodAhOCILG5SQ2LQ9dg\""
			+ "}";

	// Figure 107: Flattened JWE JSON Serialization
	private static final String FIGURE107 = 
			  "{"
			+ "\"protected\":\"eyJhbGciOiJQQkVTMi1IUzUxMitBMjU2S1ciLCJwMnMiOi"
			+ "I4UTFTemluYXNSM3hjaFl6NlpaY0hBIiwicDJjIjo4MTkyLCJjdHkiOi"
			+ "Jqd2stc2V0K2pzb24iLCJlbmMiOiJBMTI4Q0JDLUhTMjU2In0\","
			+ "\"encrypted_key\":\"d3qNhUWfqheyPp4H8sjOWsDYajoej4c5Je6rlUtFPW"
			+ "dgtURtmeDV1g\"," 
			+ "\"iv\":\"VBiCzVHNoLiR3F4V82uoTQ\","
			+ "\"ciphertext\":\"23i-Tb1AV4n0WKVSSgcQrdg6GRqsUKxjruHXYsTHAJLZ2"
			+ "nsnGIX86vMXqIi6IRsfywCRFzLxEcZBRnTvG3nhzPk0GDD7FMyXhUHpD"
			+ "jEYCNA_XOmzg8yZR9oyjo6lTF6si4q9FZ2EhzgFQCLO_6h5EVg3vR75_"
			+ "hkBsnuoqoM3dwejXBtIodN84PeqMb6asmas_dpSsz7H10fC5ni9xIz42"
			+ "4givB1YLldF6exVmL93R3fOoOJbmk2GBQZL_SEGllv2cQsBgeprARsaQ"
			+ "7Bq99tT80coH8ItBjgV08AtzXFFsx9qKvC982KLKdPQMTlVJKkqtV4Ru"
			+ "5LEVpBZXBnZrtViSOgyg6AiuwaS-rCrcD_ePOGSuxvgtrokAKYPqmXUe"
			+ "RdjFJwafkYEkiuDCV9vWGAi1DH2xTafhJwcmywIyzi4BqRpmdn_N-zl5"
			+ "tuJYyuvKhjKv6ihbsV_k1hJGPGAxJ6wUpmwC4PTQ2izEm0TuSE8oMKdT"
			+ "w8V3kobXZ77ulMwDs4p\"," 
			+ "\"tag\":\"0HlwodAhOCILG5SQ2LQ9dg\""
			+ "}";

	// Figure 109: Content Encryption Key, base64url-encoded
	private static final String FIGURE109 = "Nou2ueKlP70ZXDbq9UrRwg";

	// Figure 110: Initialization Vector, base64url-encoded
	private static final String FIGURE110 = "mH-G2zVqgztUtnW_";

	// Figure 117: Compact Serialization
	private static final String FIGURE117 = 
			  "eyJhbGciOiJFQ0RILUVTK0ExMjhLVyIsImtpZCI6InBlcmVncmluLnRvb2tAdH"
			+ "Vja2Jvcm91Z2guZXhhbXBsZSIsImVwayI6eyJrdHkiOiJFQyIsImNydiI6IlAt"
			+ "Mzg0IiwieCI6InVCbzRrSFB3Nmtiang1bDB4b3dyZF9vWXpCbWF6LUdLRlp1NH"
			+ "hBRkZrYllpV2d1dEVLNml1RURzUTZ3TmROZzMiLCJ5Ijoic3AzcDVTR2haVkMy"
			+ "ZmFYdW1JLWU5SlUyTW84S3BvWXJGRHI1eVBOVnRXNFBnRXdaT3lRVEEtSmRhWT"
			+ "h0YjdFMCJ9LCJlbmMiOiJBMTI4R0NNIn0"
			+ "."
			+ "0DJjBXri_kBcC46IkU5_Jk9BqaQeHdv2"
			+ "."
			+ "mH-G2zVqgztUtnW_"
			+ "."
			+ "tkZuOO9h95OgHJmkkrfLBisku8rGf6nzVxhRM3sVOhXgz5NJ76oID7lpnAi_cP"
			+ "WJRCjSpAaUZ5dOR3Spy7QuEkmKx8-3RCMhSYMzsXaEwDdXta9Mn5B7cCBoJKB0"
			+ "IgEnj_qfo1hIi-uEkUpOZ8aLTZGHfpl05jMwbKkTe2yK3mjF6SBAsgicQDVCkc"
			+ "Y9BLluzx1RmC3ORXaM0JaHPB93YcdSDGgpgBWMVrNU1ErkjcMqMoT_wtCex3w0"
			+ "3XdLkjXIuEr2hWgeP-nkUZTPU9EoGSPj6fAS-bSz87RCPrxZdj_iVyC6QWcqAu"
			+ "07WNhjzJEPc4jVntRJ6K53NgPQ5p99l3Z408OUqj4ioYezbS6vTPlQ"
			+ "."
			+ "WuGzxmcreYjpHGJoa17EBg";

	// Figure 118: General JWE JSON Serialization
	private static final String FIGURE118 = 
			  "{" 
			+ "\"recipients\":[" 
			+ "{"
			+ "\"encrypted_key\":\"0DJjBXri_kBcC46IkU5_Jk9BqaQeHdv2\""
			+ "}" + 
			"],"
			+ "\"protected\":\"eyJhbGciOiJFQ0RILUVTK0ExMjhLVyIsImtpZCI6InBlcm"
			+ "VncmluLnRvb2tAdHVja2Jvcm91Z2guZXhhbXBsZSIsImVwayI6eyJrdH"
			+ "kiOiJFQyIsImNydiI6IlAtMzg0IiwieCI6InVCbzRrSFB3Nmtiang1bD"
	        + "B4b3dyZF9vWXpCbWF6LUdLRlp1NHhBRkZrYllpV2d1dEVLNml1RURzUT"
	        + "Z3TmROZzMiLCJ5Ijoic3AzcDVTR2haVkMyZmFYdW1JLWU5SlUyTW84S3"
	        + "BvWXJGRHI1eVBOVnRXNFBnRXdaT3lRVEEtSmRhWTh0YjdFMCJ9LCJlbm"
	        + "MiOiJBMTI4R0NNIn0\","
			+ "\"iv\":\"mH-G2zVqgztUtnW_\","
			+ "\"ciphertext\":\"tkZuOO9h95OgHJmkkrfLBisku8rGf6nzVxhRM3sVOhXgz"
	        + "5NJ76oID7lpnAi_cPWJRCjSpAaUZ5dOR3Spy7QuEkmKx8-3RCMhSYMzs"
	        + "XaEwDdXta9Mn5B7cCBoJKB0IgEnj_qfo1hIi-uEkUpOZ8aLTZGHfpl05"
	        + "jMwbKkTe2yK3mjF6SBAsgicQDVCkcY9BLluzx1RmC3ORXaM0JaHPB93Y"
	        + "cdSDGgpgBWMVrNU1ErkjcMqMoT_wtCex3w03XdLkjXIuEr2hWgeP-nkU"
	        + "ZTPU9EoGSPj6fAS-bSz87RCPrxZdj_iVyC6QWcqAu07WNhjzJEPc4jVn"
	        + "tRJ6K53NgPQ5p99l3Z408OUqj4ioYezbS6vTPlQ\","
			+ "\"tag\":\"WuGzxmcreYjpHGJoa17EBg\""
			+ "}";

	// Figure 119: Flattened JWE JSON Serialization
	private static final String FIGURE119 =  
			  "{"
			+ "\"protected\":\"eyJhbGciOiJFQ0RILUVTK0ExMjhLVyIsImtpZCI6InBlcm"
		    + "VncmluLnRvb2tAdHVja2Jvcm91Z2guZXhhbXBsZSIsImVwayI6eyJrdH"
		    + "kiOiJFQyIsImNydiI6IlAtMzg0IiwieCI6InVCbzRrSFB3Nmtiang1bD"
		    + "B4b3dyZF9vWXpCbWF6LUdLRlp1NHhBRkZrYllpV2d1dEVLNml1RURzUT"
		    + "Z3TmROZzMiLCJ5Ijoic3AzcDVTR2haVkMyZmFYdW1JLWU5SlUyTW84S3"
		    + "BvWXJGRHI1eVBOVnRXNFBnRXdaT3lRVEEtSmRhWTh0YjdFMCJ9LCJlbm"
		    + "MiOiJBMTI4R0NNIn0\","
		    + "\"encrypted_key\":\"0DJjBXri_kBcC46IkU5_Jk9BqaQeHdv2\","
		    + "\"iv\":\"mH-G2zVqgztUtnW_\","
		    + "\"ciphertext\":\"tkZuOO9h95OgHJmkkrfLBisku8rGf6nzVxhRM3sVOhXgz"
		    + "5NJ76oID7lpnAi_cPWJRCjSpAaUZ5dOR3Spy7QuEkmKx8-3RCMhSYMzs"
		    + "XaEwDdXta9Mn5B7cCBoJKB0IgEnj_qfo1hIi-uEkUpOZ8aLTZGHfpl05"
		    + "jMwbKkTe2yK3mjF6SBAsgicQDVCkcY9BLluzx1RmC3ORXaM0JaHPB93Y"
		    + "cdSDGgpgBWMVrNU1ErkjcMqMoT_wtCex3w03XdLkjXIuEr2hWgeP-nkU"
		    + "ZTPU9EoGSPj6fAS-bSz87RCPrxZdj_iVyC6QWcqAu07WNhjzJEPc4jVn"
		    + "tRJ6K53NgPQ5p99l3Z408OUqj4ioYezbS6vTPlQ\","
		    +  "\"tag\":\"WuGzxmcreYjpHGJoa17EBg\""
		    + "}";

	// Figure 121: Initialization Vector, base64url-encoded
	private static final String FIGURE121 = "yc9N8v5sYyv3iGQT926IUg";
	
	// Figure 128: JWE Compact Serialization
	private static final String FIGURE128 = 
			  "eyJhbGciOiJFQ0RILUVTIiwia2lkIjoibWVyaWFkb2MuYnJhbmR5YnVja0BidW"
		    + "NrbGFuZC5leGFtcGxlIiwiZXBrIjp7Imt0eSI6IkVDIiwiY3J2IjoiUC0yNTYi"
		    + "LCJ4IjoibVBVS1RfYkFXR0hJaGcwVHBqanFWc1AxclhXUXVfdndWT0hIdE5rZF"
		    + "lvQSIsInkiOiI4QlFBc0ltR2VBUzQ2ZnlXdzVNaFlmR1RUMElqQnBGdzJTUzM0"
		    + "RHY0SXJzIn0sImVuYyI6IkExMjhDQkMtSFMyNTYifQ"
		    + "."
		    + "."
		    + "yc9N8v5sYyv3iGQT926IUg"
		    + "."
		    + "BoDlwPnTypYq-ivjmQvAYJLb5Q6l-F3LIgQomlz87yW4OPKbWE1zSTEFjDfhU9"
		    + "IPIOSA9Bml4m7iDFwA-1ZXvHteLDtw4R1XRGMEsDIqAYtskTTmzmzNa-_q4F_e"
		    + "vAPUmwlO-ZG45Mnq4uhM1fm_D9rBtWolqZSF3xGNNkpOMQKF1Cl8i8wjzRli7-"
		    + "IXgyirlKQsbhhqRzkv8IcY6aHl24j03C-AR2le1r7URUhArM79BY8soZU0lzwI"
		    + "-sD5PZ3l4NDCCei9XkoIAfsXJWmySPoeRb2Ni5UZL4mYpvKDiwmyzGd65KqVw7"
		    + "MsFfI_K767G9C9Azp73gKZD0DyUn1mn0WW5LmyX_yJ-3AROq8p1WZBfG-ZyJ61"
		    + "95_JGG2m9Csg"
		    + "."
		    + "WCCkNa-x4BeB9hIDIfFuhg";

	// Figure 129: General JWE JSON Serialization   
	private static final String FIGURE129 = 
			  "{" 
			+ "\"protected\":\"eyJhbGciOiJFQ0RILUVTIiwia2lkIjoibWVyaWFkb2MuYn"
			+ "JhbmR5YnVja0BidWNrbGFuZC5leGFtcGxlIiwiZXBrIjp7Imt0eSI6Ik"
			+ "VDIiwiY3J2IjoiUC0yNTYiLCJ4IjoibVBVS1RfYkFXR0hJaGcwVHBqan"
	        + "FWc1AxclhXUXVfdndWT0hIdE5rZFlvQSIsInkiOiI4QlFBc0ltR2VBUz"
	        + "Q2ZnlXdzVNaFlmR1RUMElqQnBGdzJTUzM0RHY0SXJzIn0sImVuYyI6Ik"
	        + "ExMjhDQkMtSFMyNTYifQ\","
			+ "\"iv\":\"yc9N8v5sYyv3iGQT926IUg\","
			+ "\"ciphertext\":\"BoDlwPnTypYq-ivjmQvAYJLb5Q6l-F3LIgQomlz87yW4O"
			+ "PKbWE1zSTEFjDfhU9IPIOSA9Bml4m7iDFwA-1ZXvHteLDtw4R1XRGMEs"
			+ "DIqAYtskTTmzmzNa-_q4F_evAPUmwlO-ZG45Mnq4uhM1fm_D9rBtWolq"
	        + "ZSF3xGNNkpOMQKF1Cl8i8wjzRli7-IXgyirlKQsbhhqRzkv8IcY6aHl2"
	        + "4j03C-AR2le1r7URUhArM79BY8soZU0lzwI-sD5PZ3l4NDCCei9XkoIA"
	        + "fsXJWmySPoeRb2Ni5UZL4mYpvKDiwmyzGd65KqVw7MsFfI_K767G9C9A"
	        + "zp73gKZD0DyUn1mn0WW5LmyX_yJ-3AROq8p1WZBfG-ZyJ6195_JGG2m9"
	        + "Csg\","
			+ "\"tag\":\"WCCkNa-x4BeB9hIDIfFuhg\""
			+ "}";
		           
	// Figure 131: Initialization Vector, base64url-encoded
	private static final String FIGURE131 = "refa467QzzKx6QAB";

	// Figure 136: Compact Serialization
	private static final String FIGURE136 = 
			  "eyJhbGciOiJkaXIiLCJraWQiOiI3N2M3ZTJiOC02ZTEzLTQ1Y2YtODY3Mi02MT"
			+ "diNWI0NTI0M2EiLCJlbmMiOiJBMTI4R0NNIn0"
			+ "."
			+ "."
			+ "refa467QzzKx6QAB"
			+ "."
			+ "JW_i_f52hww_ELQPGaYyeAB6HYGcR559l9TYnSovc23XJoBcW29rHP8yZOZG7Y"
			+ "hLpT1bjFuvZPjQS-m0IFtVcXkZXdH_lr_FrdYt9HRUYkshtrMmIUAyGmUnd9zM"
			+ "DB2n0cRDIHAzFVeJUDxkUwVAE7_YGRPdcqMyiBoCO-FBdE-Nceb4h3-FtBP-c_"
			+ "BIwCPTjb9o0SbdcdREEMJMyZBH8ySWMVi1gPD9yxi-aQpGbSv_F9N4IZAxscj5"
			+ "g-NJsUPbjk29-s7LJAGb15wEBtXphVCgyy53CoIKLHHeJHXex45Uz9aKZSRSIn"
			+ "ZI-wjsY0yu3cT4_aQ3i1o-tiE-F8Ios61EKgyIQ4CWao8PFMj8TTnp"
			+ "."
			+ "vbb32Xvllea2OtmHAdccRQ";
	
	// Figure 137: JSON General Serialization
	private static final String FIGURE137 = 
			  "{"
			+ "\"protected\":\"eyJhbGciOiJkaXIiLCJraWQiOiI3N2M3ZTJiOC02ZTEzLT"
			+ "Q1Y2YtODY3Mi02MTdiNWI0NTI0M2EiLCJlbmMiOiJBMTI4R0NNIn0\","
			+ "\"iv\":\"refa467QzzKx6QAB\","
			+ "\"ciphertext\":\"JW_i_f52hww_ELQPGaYyeAB6HYGcR559l9TYnSovc23XJ"
			+ "oBcW29rHP8yZOZG7YhLpT1bjFuvZPjQS-m0IFtVcXkZXdH_lr_FrdYt9"
			+ "HRUYkshtrMmIUAyGmUnd9zMDB2n0cRDIHAzFVeJUDxkUwVAE7_YGRPdc"
			+ "qMyiBoCO-FBdE-Nceb4h3-FtBP-c_BIwCPTjb9o0SbdcdREEMJMyZBH8"
			+ "ySWMVi1gPD9yxi-aQpGbSv_F9N4IZAxscj5g-NJsUPbjk29-s7LJAGb1"
			+ "5wEBtXphVCgyy53CoIKLHHeJHXex45Uz9aKZSRSInZI-wjsY0yu3cT4_"
			+ "aQ3i1o-tiE-F8Ios61EKgyIQ4CWao8PFMj8TTnp\","
			+ "\"tag\":\"vbb32Xvllea2OtmHAdccRQ\"" 
			+ "}";

	// Figure 139: Content Encryption Key, base64url-encoded
	private static final String FIGURE139 = "UWxARpat23nL9ReIj4WG3D1ee9I4r-Mv5QLuFXdy_rE";

	// Figure 140: Initialization Vector, base64url-encoded
	private static final String FIGURE140 = "gz6NjyEFNm_vm8Gj6FwoFQ";

	// Figure 141: Initialization Vector for Key Wrapping, base64url-encoded
	private static final String FIGURE141 = "KkYT0GX_2jHlfqN_";
	   
	// Figure 148: Compact Serialization
	private static final String FIGURE148 = 
			  "eyJhbGciOiJBMjU2R0NNS1ciLCJraWQiOiIxOGVjMDhlMS1iZmE5LTRkOTUtYj"
			+ "IwNS0yYjRkZDFkNDMyMWQiLCJ0YWciOiJrZlBkdVZRM1QzSDZ2bmV3dC0ta3N3"
			+ "IiwiaXYiOiJLa1lUMEdYXzJqSGxmcU5fIiwiZW5jIjoiQTEyOENCQy1IUzI1Ni"
			+ "J9"
			+ "."
			+ "lJf3HbOApxMEBkCMOoTnnABxs_CvTWUmZQ2ElLvYNok"
			+ "."
			+ "gz6NjyEFNm_vm8Gj6FwoFQ"
			+ "."
			+ "Jf5p9-ZhJlJy_IQ_byKFmI0Ro7w7G1QiaZpI8OaiVgD8EqoDZHyFKFBupS8iaE"
			+ "eVIgMqWmsuJKuoVgzR3YfzoMd3GxEm3VxNhzWyWtZKX0gxKdy6HgLvqoGNbZCz"
			+ "LjqcpDiF8q2_62EVAbr2uSc2oaxFmFuIQHLcqAHxy51449xkjZ7ewzZaGV3eFq"
			+ "hpco8o4DijXaG5_7kp3h2cajRfDgymuxUbWgLqaeNQaJtvJmSMFuEOSAzw9Hde"
			+ "b6yhdTynCRmu-kqtO5Dec4lT2OMZKpnxc_F1_4yDJFcqb5CiDSmA-psB2k0Jtj"
			+ "xAj4UPI61oONK7zzFIu4gBfjJCndsZfdvG7h8wGjV98QhrKEnR7xKZ3KCr0_qR"
			+ "1B-gxpNk3xWU" 
			+ "." 
			+ "DKW7jrb4WaRSNfbXVPlT5g";

	// Figure 149: JSON General Serialization
	private static final String FIGURE149 = 
			  "{"
			+ "\"recipients\":["
			+ "{"
			+ "\"encrypted_key\":\"lJf3HbOApxMEBkCMOoTnnABxs_CvTWUmZQ2ElL"
			+ "vYNok\""
			+ "}"
			+ "],"
			+ "\"protected\":\"eyJhbGciOiJBMjU2R0NNS1ciLCJraWQiOiIxOGVjMDhlMS"
			+ "1iZmE5LTRkOTUtYjIwNS0yYjRkZDFkNDMyMWQiLCJ0YWciOiJrZlBkdV"
			+ "ZRM1QzSDZ2bmV3dC0ta3N3IiwiaXYiOiJLa1lUMEdYXzJqSGxmcU5fIi"
			+ "wiZW5jIjoiQTEyOENCQy1IUzI1NiJ9\","
			+ "\"iv\":\"gz6NjyEFNm_vm8Gj6FwoFQ\","
			+ "\"ciphertext\":\"Jf5p9-ZhJlJy_IQ_byKFmI0Ro7w7G1QiaZpI8OaiVgD8E"
			+ "qoDZHyFKFBupS8iaEeVIgMqWmsuJKuoVgzR3YfzoMd3GxEm3VxNhzWyW"
			+ "tZKX0gxKdy6HgLvqoGNbZCzLjqcpDiF8q2_62EVAbr2uSc2oaxFmFuIQ"
			+ "HLcqAHxy51449xkjZ7ewzZaGV3eFqhpco8o4DijXaG5_7kp3h2cajRfD"
			+ "gymuxUbWgLqaeNQaJtvJmSMFuEOSAzw9Hdeb6yhdTynCRmu-kqtO5Dec"
			+ "4lT2OMZKpnxc_F1_4yDJFcqb5CiDSmA-psB2k0JtjxAj4UPI61oONK7z"
			+ "zFIu4gBfjJCndsZfdvG7h8wGjV98QhrKEnR7xKZ3KCr0_qR1B-gxpNk3"
			+ "xWU\"," 
			+ "\"tag\":\"DKW7jrb4WaRSNfbXVPlT5g\"}";

	// Figure 150: Flattened JWE JSON Serialization
	private static final String FIGURE150 =  
			  "{"
			+ "\"protected\":\"eyJhbGciOiJBMjU2R0NNS1ciLCJpdiI6IktrWVQwR1hfMm"
	        + "pIbGZxTl8iLCJraWQiOiIxOGVjMDhlMS1iZmE5LTRkOTUtYjIwNS0yYj"
	        + "RkZDFkNDMyMWQiLCJ0YWciOiJrZlBkdVZRM1QzSDZ2bmV3dC0ta3N3Ii"
	        + "wiZW5jIjoiQTEyOENCQy1IUzI1NiJ9\","
		    + "\"encrypted_key\":\"lJf3HbOApxMEBkCMOoTnnABxs_CvTWUmZQ2ElLvYNo"
	        + "k\","
		    + "\"iv\":\"gz6NjyEFNm_vm8Gj6FwoFQ\","
		    + "\"ciphertext\":\"Jf5p9-ZhJlJy_IQ_byKFmI0Ro7w7G1QiaZpI8OaiVgD8E"
	        + "qoDZHyFKFBupS8iaEeVIgMqWmsuJKuoVgzR3YfzoMd3GxEm3VxNhzWyW"
	        + "tZKX0gxKdy6HgLvqoGNbZCzLjqcpDiF8q2_62EVAbr2uSc2oaxFmFuIQ"
	        + "HLcqAHxy51449xkjZ7ewzZaGV3eFqhpco8o4DijXaG5_7kp3h2cajRfD"
	        + "gymuxUbWgLqaeNQaJtvJmSMFuEOSAzw9Hdeb6yhdTynCRmu-kqtO5Dec"
	        + "4lT2OMZKpnxc_F1_4yDJFcqb5CiDSmA-psB2k0JtjxAj4UPI61oONK7z"
	        + "zFIu4gBfjJCndsZfdvG7h8wGjV98QhrKEnR7xKZ3KCr0_qR1B-gxpNk3"
	        + "xWU\","		
		    +  "\"tag\":\"NvBveHr_vonkvflfnUrmBQ\""
		    + "}";
	          
	// Figure 152: Content Encryption Key, base64url-encoded
	private static final String FIGURE152 = "aY5_Ghmk9KxWPBLu_glx1w";

	// Figure 153: Initialization Vector, base64url-encoded
	private static final String FIGURE153 = "Qx0pmsDa8KnJc9Jo";

	// Figure 159: Compact Serialization
	private static final String FIGURE159 = 
			  "eyJhbGciOiJBMTI4S1ciLCJraWQiOiI4MWIyMDk2NS04MzMyLTQzZDktYTQ2OC"
			+ "04MjE2MGFkOTFhYzgiLCJlbmMiOiJBMTI4R0NNIn0"
			+ "."
			+ "CBI6oDw8MydIx1IBntf_lQcw2MmJKIQx"
			+ "."
			+ "Qx0pmsDa8KnJc9Jo"
			+ "."
			+ "AwliP-KmWgsZ37BvzCefNen6VTbRK3QMA4TkvRkH0tP1bTdhtFJgJxeVmJkLD6"
			+ "1A1hnWGetdg11c9ADsnWgL56NyxwSYjU1ZEHcGkd3EkU0vjHi9gTlb90qSYFfe"
			+ "F0LwkcTtjbYKCsiNJQkcIp1yeM03OmuiYSoYJVSpf7ej6zaYcMv3WwdxDFl8RE"
			+ "wOhNImk2Xld2JXq6BR53TSFkyT7PwVLuq-1GwtGHlQeg7gDT6xW0JqHDPn_H-p"
			+ "uQsmthc9Zg0ojmJfqqFvETUxLAF-KjcBTS5dNy6egwkYtOt8EIHK-oEsKYtZRa"
			+ "a8Z7MOZ7UGxGIMvEmxrGCPeJa14slv2-gaqK0kEThkaSqdYw0FkQZF"
			+ "."
			+ "ER7MWJZ1FBI_NKvn7Zb1Lw";
	
	// Figure 160: JSON General Serialization
	private static final String FIGURE160 = 
			"{"
			+ "\"recipients\":["
			+ "{"
			+ "\"encrypted_key\":\"CBI6oDw8MydIx1IBntf_lQcw2MmJKIQx\""
			+ "}"
			+ "],"
			+ "\"protected\":\"eyJhbGciOiJBMTI4S1ciLCJraWQiOiI4MWIyMDk2NS04Mz"
			+ "MyLTQzZDktYTQ2OC04MjE2MGFkOTFhYzgiLCJlbmMiOiJBMTI4R0NNIn0\","
			+ "\"iv\":\"Qx0pmsDa8KnJc9Jo\","
			+ "\"ciphertext\":\"AwliP-KmWgsZ37BvzCefNen6VTbRK3QMA4TkvRkH0tP1b"
			+ "TdhtFJgJxeVmJkLD61A1hnWGetdg11c9ADsnWgL56NyxwSYjU1ZEHcGk"
			+ "d3EkU0vjHi9gTlb90qSYFfeF0LwkcTtjbYKCsiNJQkcIp1yeM03OmuiY"
			+ "SoYJVSpf7ej6zaYcMv3WwdxDFl8REwOhNImk2Xld2JXq6BR53TSFkyT7"
			+ "PwVLuq-1GwtGHlQeg7gDT6xW0JqHDPn_H-puQsmthc9Zg0ojmJfqqFvE"
			+ "TUxLAF-KjcBTS5dNy6egwkYtOt8EIHK-oEsKYtZRaa8Z7MOZ7UGxGIMv"
			+ "EmxrGCPeJa14slv2-gaqK0kEThkaSqdYw0FkQZF\","
			+ "\"tag\":\"ER7MWJZ1FBI_NKvn7Zb1Lw\""
			+ "}";

	// Figure 161: Flattened JWE JSON Serialization
	private static final String FIGURE161 =  
			  "{"
			+ "\"protected\":\"eyJhbGciOiJBMTI4S1ciLCJraWQiOiI4MWIyMDk2NS04Mz"
	        + "MyLTQzZDktYTQ2OC04MjE2MGFkOTFhYzgiLCJlbmMiOiJBMTI4R0NNIn"
	        + "0\","
		    + "\"encrypted_key\":\"CBI6oDw8MydIx1IBntf_lQcw2MmJKIQx\","
		    + "\"iv\":\"Qx0pmsDa8KnJc9Jo\","
		    + "\"ciphertext\":\"AwliP-KmWgsZ37BvzCefNen6VTbRK3QMA4TkvRkH0tP1b"
		    + "TdhtFJgJxeVmJkLD61A1hnWGetdg11c9ADsnWgL56NyxwSYjU1ZEHcGk"
		    + "d3EkU0vjHi9gTlb90qSYFfeF0LwkcTtjbYKCsiNJQkcIp1yeM03OmuiY"
	        + "SoYJVSpf7ej6zaYcMv3WwdxDFl8REwOhNImk2Xld2JXq6BR53TSFkyT7"
	        + "PwVLuq-1GwtGHlQeg7gDT6xW0JqHDPn_H-puQsmthc9Zg0ojmJfqqFvE"
	        + "TUxLAF-KjcBTS5dNy6egwkYtOt8EIHK-oEsKYtZRaa8Z7MOZ7UGxGIMv"
	        + "EmxrGCPeJa14slv2-gaqK0kEThkaSqdYw0FkQZF\","
		    +  "\"tag\":\"ER7MWJZ1FBI_NKvn7Zb1Lw\""
		    + "}";
	            
	// Figure 163: Content Encryption Key, base64url-encoded
	private static final String FIGURE163 = "hC-MpLZSuwWv8sexS6ydfw";

	// Figure 164: Initialization Vector, base64url-encoded
	private static final String FIGURE164 = "p9pUq6XHY0jfEZIl";

	// Figure 170: Compact Serialization
	private static final String FIGURE170 = 
			  "eyJhbGciOiJBMTI4S1ciLCJraWQiOiI4MWIyMDk2NS04MzMyLTQzZDktYTQ2OC"
			+ "04MjE2MGFkOTFhYzgiLCJlbmMiOiJBMTI4R0NNIiwiemlwIjoiREVGIn0"
			+ "."
			+ "5vUT2WOtQxKWcekM_IzVQwkGgzlFDwPi"
			+ "."
			+ "p9pUq6XHY0jfEZIl"
			+ "."
			+ "HbDtOsdai1oYziSx25KEeTxmwnh8L8jKMFNc1k3zmMI6VB8hry57tDZ61jXyez"
			+ "SPt0fdLVfe6Jf5y5-JaCap_JQBcb5opbmT60uWGml8blyiMQmOn9J--XhhlYg0"
			+ "m-BHaqfDO5iTOWxPxFMUedx7WCy8mxgDHj0aBMG6152PsM-w5E_o2B3jDbrYBK"
			+ "hpYA7qi3AyijnCJ7BP9rr3U8kxExCpG3mK420TjOw"
			+ "."
			+ "VILuUwuIxaLVmh5X-T7kmA";

	// Figure 171: JSON General Serialization
	private static final String FIGURE171 = 
			  "{" 
			+ "\"recipients\":["
			+ "{"
			+ "\"encrypted_key\":\"5vUT2WOtQxKWcekM_IzVQwkGgzlFDwPi\""
			+ "}"
			+ "],"
			+ "\"protected\":\"eyJhbGciOiJBMTI4S1ciLCJraWQiOiI4MWIyMDk2NS04Mz"
			+ "MyLTQzZDktYTQ2OC04MjE2MGFkOTFhYzgiLCJlbmMiOiJBMTI4R0NNIi"
			+ "wiemlwIjoiREVGIn0\"," 
			+ "\"iv\":\"p9pUq6XHY0jfEZIl\","
			+ "\"ciphertext\":\"HbDtOsdai1oYziSx25KEeTxmwnh8L8jKMFNc1k3zmMI6V"
			+ "B8hry57tDZ61jXyezSPt0fdLVfe6Jf5y5-JaCap_JQBcb5opbmT60uWG"
			+ "ml8blyiMQmOn9J--XhhlYg0m-BHaqfDO5iTOWxPxFMUedx7WCy8mxgDH"
			+ "j0aBMG6152PsM-w5E_o2B3jDbrYBKhpYA7qi3AyijnCJ7BP9rr3U8kxE"
			+ "xCpG3mK420TjOw\"," 
			+ "\"tag\":\"VILuUwuIxaLVmh5X-T7kmA\""
			+ "}";

	// Figure 172: Flattened JWE JSON Serialization
	private static final String FIGURE172 =  
			  "{"
			+ "\"protected\":\"eyJhbGciOiJBMTI4S1ciLCJraWQiOiI4MWIyMDk2NS04Mz"
	        + "MyLTQzZDktYTQ2OC04MjE2MGFkOTFhYzgiLCJlbmMiOiJBMTI4R0NNIi"
	        + "wiemlwIjoiREVGIn0\","
		    + "\"encrypted_key\":\"5vUT2WOtQxKWcekM_IzVQwkGgzlFDwPi\","
		    + "\"iv\":\"p9pUq6XHY0jfEZIl\","
		    + "\"ciphertext\":\"HbDtOsdai1oYziSx25KEeTxmwnh8L8jKMFNc1k3zmMI6V"
		    + "B8hry57tDZ61jXyezSPt0fdLVfe6Jf5y5-JaCap_JQBcb5opbmT60uWG"
		    + "ml8blyiMQmOn9J--XhhlYg0m-BHaqfDO5iTOWxPxFMUedx7WCy8mxgDH"
		    + "j0aBMG6152PsM-w5E_o2B3jDbrYBKhpYA7qi3AyijnCJ7BP9rr3U8kxE"
		    + "xCpG3mK420TjOw\","
		    +  "\"tag\":\"VILuUwuIxaLVmh5X-T7kmA\""
		    + "}";
        
	// Figure 173: Additional Authenticated Data, in JSON format
	private static final String FIGURE173 = 
			  "["
			+ "\"vcard\","
			+ "["
			+ "[\"version\",{},\"text\",\"4.0\"],"
			+ "[\"fn\",{},\"text\",\"Meriadoc Brandybuck\"]," 
			+ "[\"n\",{},"
			+ "\"text\",[" 
			+ "\"Brandybuck\",\"Meriadoc\",\"Mr.\",\"\""
			+ "]"
			+ "],"
			+ "[\"bday\",{},\"text\",\"TA 2982\"],"
			+ "[\"gender\",{},\"text\",\"M\"]"
			+ "]"
			+ "]";

	// Figure 174: Content Encryption Key, base64url-encoded
	private static final String FIGURE174 = "75m1ALsYv10pZTKPWrsqdg";

	// Figure 175: Initialization Vector, base64url-encoded
	private static final String FIGURE175 = "veCx9ece2orS7c_N";

	// Figure 182: JSON General Serialization
	private static final String FIGURE182 = 
			  "{" 
	        + "\"recipients\":[{"
			+ "\"encrypted_key\":\"4YiiQ_ZzH76TaIkJmYfRFgOV9MIpnx4X\"}],"
			+ "\"protected\":\"eyJhbGciOiJBMTI4S1ciLCJraWQiOiI4MWIyMDk2NS04Mz"
			+ "MyLTQzZDktYTQ2OC04MjE2MGFkOTFhYzgiLCJlbmMiOiJBMTI4R0NNIn0\","
			+ "\"iv\":\"veCx9ece2orS7c_N\","
			+ "\"aad\":\"WyJ2Y2FyZCIsW1sidmVyc2lvbiIse30sInRleHQiLCI0LjAiXSxb"
			+ "ImZuIix7fSwidGV4dCIsIk1lcmlhZG9jIEJyYW5keWJ1Y2siXSxbIm4i"
			+ "LHt9LCJ0ZXh0IixbIkJyYW5keWJ1Y2siLCJNZXJpYWRvYyIsIk1yLiIs"
			+ "IiJdXSxbImJkYXkiLHt9LCJ0ZXh0IiwiVEEgMjk4MiJdLFsiZ2VuZGVy"
			+ "Iix7fSwidGV4dCIsIk0iXV1d\","
			+ "\"ciphertext\":\"Z_3cbr0k3bVM6N3oSNmHz7Lyf3iPppGf3Pj17wNZqteJ0"
			+ "Ui8p74SchQP8xygM1oFRWCNzeIa6s6BcEtp8qEFiqTUEyiNkOWDNoF14"
			+ "T_4NFqF-p2Mx8zkbKxI7oPK8KNarFbyxIDvICNqBLba-v3uzXBdB89fz"
			+ "OI-Lv4PjOFAQGHrgv1rjXAmKbgkft9cB4WeyZw8MldbBhc-V_KWZslrs"
			+ "LNygon_JJWd_ek6LQn5NRehvApqf9ZrxB4aq3FXBxOxCys35PhCdaggy"
			+ "2kfUfl2OkwKnWUbgXVD1C6HxLIlqHhCwXDG59weHrRDQeHyMRoBljoV3"
			+ "X_bUTJDnKBFOod7nLz-cj48JMx3SnCZTpbQAkFV\","
			+ "\"tag\":\"vOaH_Rajnpy_3hOtqvZHRA\"}";

	// Figure 183: Flattened JWE JSON Serialization
	private static final String FIGURE183 =  
			  "{"
			+ "\"protected\":\"eyJhbGciOiJBMTI4S1ciLCJraWQiOiI4MWIyMDk2NS04Mz"
	        + "MyLTQzZDktYTQ2OC04MjE2MGFkOTFhYzgiLCJlbmMiOiJBMTI4R0NNIn"
	        + "0\","
		    + "\"encrypted_key\":\"4YiiQ_ZzH76TaIkJmYfRFgOV9MIpnx4X\","
		    + "\"aad\":\"WyJ2Y2FyZCIsW1sidmVyc2lvbiIse30sInRleHQiLCI0LjAiXSxb"
		    + "ImZuIix7fSwidGV4dCIsIk1lcmlhZG9jIEJyYW5keWJ1Y2siXSxbIm4i"
		    + "LHt9LCJ0ZXh0IixbIkJyYW5keWJ1Y2siLCJNZXJpYWRvYyIsIk1yLiIs"
	        + "IiJdXSxbImJkYXkiLHt9LCJ0ZXh0IiwiVEEgMjk4MiJdLFsiZ2VuZGVy"
	        + "Iix7fSwidGV4dCIsIk0iXV1d\","
		    + "\"iv\":\"veCx9ece2orS7c_N\","
		    + "\"ciphertext\":\"Z_3cbr0k3bVM6N3oSNmHz7Lyf3iPppGf3Pj17wNZqteJ0"
		    + "Ui8p74SchQP8xygM1oFRWCNzeIa6s6BcEtp8qEFiqTUEyiNkOWDNoF14"
		    + "T_4NFqF-p2Mx8zkbKxI7oPK8KNarFbyxIDvICNqBLba-v3uzXBdB89fz"
	        + "OI-Lv4PjOFAQGHrgv1rjXAmKbgkft9cB4WeyZw8MldbBhc-V_KWZslrs"
	        + "LNygon_JJWd_ek6LQn5NRehvApqf9ZrxB4aq3FXBxOxCys35PhCdaggy"
	        + "2kfUfl2OkwKnWUbgXVD1C6HxLIlqHhCwXDG59weHrRDQeHyMRoBljoV3"
	        + "X_bUTJDnKBFOod7nLz-cj48JMx3SnCZTpbQAkFV\","
		    +  "\"tag\":\"vOaH_Rajnpy_3hOtqvZHRA\""
		    + "}";
	           
	// Figure 184: Content Encryption Key, base64url-encoded
	private static final String FIGURE184 = "WDgEptBmQs9ouUvArz6x6g";

	// Figure 185: Initialization Vector, base64url-encoded
	private static final String FIGURE185 = "WgEJsDS9bkoXQ3nR";

	// Figure 192: JSON General Serialization
	private static final String FIGURE192 = 
			  "{" 
	        + "\"recipients\":["
	        + "{"
			+ "\"encrypted_key\":\"jJIcM9J-hbx3wnqhf5FlkEYos0sHsF0H\""
			+ "}"
			+ "],"
			+ "\"unprotected\":{" 
			+ "\"alg\":\"A128KW\","
			+ "\"kid\":\"81b20965-8332-43d9-a468-82160ad91ac8\""
			+ "},"
			+ "\"protected\":\"eyJlbmMiOiJBMTI4R0NNIn0\","
			+ "\"iv\":\"WgEJsDS9bkoXQ3nR\","
			+ "\"ciphertext\":\"lIbCyRmRJxnB2yLQOTqjCDKV3H30ossOw3uD9DPsqLL2D"
			+ "M3swKkjOwQyZtWsFLYMj5YeLht_StAn21tHmQJuuNt64T8D4t6C7kC9O"
			+ "CCJ1IHAolUv4MyOt80MoPb8fZYbNKqplzYJgIL58g8N2v46OgyG637d6"
			+ "uuKPwhAnTGm_zWhqc_srOvgiLkzyFXPq1hBAURbc3-8BqeRb48iR1-_5"
			+ "g5UjWVD3lgiLCN_P7AW8mIiFvUNXBPJK3nOWL4teUPS8yHLbWeL83olU"
			+ "4UAgL48x-8dDkH23JykibVSQju-f7e-1xreHWXzWLHs1NqBbre0dEwK3"
			+ "HX_xM0LjUz77Krppgegoutpf5qaKg3l-_xMINmf\","
			+ "\"tag\":\"fNYLqpUe84KD45lvDiaBAQ\""
			+ "}";

	// Figure 193: Flattened JWE JSON Serialization
	private static final String FIGURE193 =  
			  "{"
			+ "\"protected\":\"eyJlbmMiOiJBMTI4R0NNIn0\","
			+ "\"unprotected\":{"
			+ "\"alg\":\"A128KW\","
			+ "\"kid\":\"81b20965-8332-43d9-a468-82160ad91ac8\""
			+ "},"
		    + "\"encrypted_key\":\"jJIcM9J-hbx3wnqhf5FlkEYos0sHsF0H\","
		    + "\"iv\":\"WgEJsDS9bkoXQ3nR\","
		    + "\"ciphertext\":\"lIbCyRmRJxnB2yLQOTqjCDKV3H30ossOw3uD9DPsqLL2D"
		    + "M3swKkjOwQyZtWsFLYMj5YeLht_StAn21tHmQJuuNt64T8D4t6C7kC9O"
		    + "CCJ1IHAolUv4MyOt80MoPb8fZYbNKqplzYJgIL58g8N2v46OgyG637d6"
	        + "uuKPwhAnTGm_zWhqc_srOvgiLkzyFXPq1hBAURbc3-8BqeRb48iR1-_5"
	        + "g5UjWVD3lgiLCN_P7AW8mIiFvUNXBPJK3nOWL4teUPS8yHLbWeL83olU"
	        + "4UAgL48x-8dDkH23JykibVSQju-f7e-1xreHWXzWLHs1NqBbre0dEwK3"
	        + "HX_xM0LjUz77Krppgegoutpf5qaKg3l-_xMINmf\","	    
		    +  "\"tag\":\"fNYLqpUe84KD45lvDiaBAQ\""
		    + "}";		
	   
	// Figure 194: Content Encryption Key, base64url-encoded
	private static final String FIGURE194 = "KBooAFl30QPV3vkcZlXnzQ";

	// Figure 195: Initialization Vector, base64url-encoded
	private static final String FIGURE195 = "YihBoVOGsR1l7jCD";

	// Figure 200: JSON General Serialization
	private static final String FIGURE200 = 
			  "{" 
	        + "\"recipients\":[" 
			+ "{"
			+ "\"encrypted_key\":\"244YHfO_W7RMpQW81UjQrZcq5LSyqiPv\"" 
			+ "}"
			+ "]," 
			+ "\"unprotected\":{" 
			+ "\"alg\":\"A128KW\","
			+ "\"kid\":\"81b20965-8332-43d9-a468-82160ad91ac8\","
			+ "\"enc\":\"A128GCM\"" 
			+ "}," 
			+ "\"iv\":\"YihBoVOGsR1l7jCD\","
			+ "\"ciphertext\":\"qtPIMMaOBRgASL10dNQhOa7Gqrk7Eal1vwht7R4TT1uq-"
			+ "arsVCPaIeFwQfzrSS6oEUWbBtxEasE0vC6r7sphyVziMCVJEuRJyoAHF"
			+ "SP3eqQPb4Ic1SDSqyXjw_L3svybhHYUGyQuTmUQEDjgjJfBOifwHIsDs"
			+ "RPeBz1NomqeifVPq5GTCWFo5k_MNIQURR2Wj0AHC2k7JZfu2iWjUHLF8"
			+ "ExFZLZ4nlmsvJu_mvifMYiikfNfsZAudISOa6O73yPZtL04k_1FI7WDf"
			+ "rb2w7OqKLWDXzlpcxohPVOLQwpA3mFNRKdY-bQz4Z4KX9lfz1cne31N4"
			+ "-8BKmojpw-OdQjKdLOGkC445Fb_K1tlDQXw2sBF\","
			+ "\"tag\":\"e2m0Vm7JvjK2VpCKXS-kyg\"" 
			+ "}";

	// Figure 201: Flattened JWE JSON Serialization
	private static final String FIGURE201 =  
			  "{"
			+ "\"unprotected\":{"
			+ "\"alg\":\"A128KW\","
			+ "\"kid\":\"81b20965-8332-43d9-a468-82160ad91ac8\","
			+ "\"enc\":\"A128GCM\""
			+ "},"
		    + "\"encrypted_key\":\"244YHfO_W7RMpQW81UjQrZcq5LSyqiPv\","
		    + "\"iv\":\"YihBoVOGsR1l7jCD\","
		    + "\"ciphertext\":\"qtPIMMaOBRgASL10dNQhOa7Gqrk7Eal1vwht7R4TT1uq-"
	        + "arsVCPaIeFwQfzrSS6oEUWbBtxEasE0vC6r7sphyVziMCVJEuRJyoAHF"
	        + "SP3eqQPb4Ic1SDSqyXjw_L3svybhHYUGyQuTmUQEDjgjJfBOifwHIsDs"
	        + "RPeBz1NomqeifVPq5GTCWFo5k_MNIQURR2Wj0AHC2k7JZfu2iWjUHLF8"
	        + "ExFZLZ4nlmsvJu_mvifMYiikfNfsZAudISOa6O73yPZtL04k_1FI7WDf"
	        + "rb2w7OqKLWDXzlpcxohPVOLQwpA3mFNRKdY-bQz4Z4KX9lfz1cne31N4"
	        + "-8BKmojpw-OdQjKdLOGkC445Fb_K1tlDQXw2sBF\","
		    +  "\"tag\":\"e2m0Vm7JvjK2VpCKXS-kyg\""
		    + "}";		
	
	// Figure 202: Content Encryption Key, base64url-encoded
	private static final String FIGURE202 = "zXayeJ4gvm8NJr3IUInyokTUO-LbQNKEhe_zWlYbdpQ";

	// Figure 203: Initialization Vector, base64url-encoded
	private static final String FIGURE203 = "VgEIHY20EnzUtZFl2RpB1g";

	// Figure Figure 211: Recipient #2 Initialization Vector for Key Wrapping, base64url-encoded
	private static final String FIGURE211 = "AvpeoPZ9Ncn9mkBn";
    
	// Figure 221: JSON General Serialization
	private static final String FIGURE221 = 
			  "{" 
	        + "\"recipients\":[" 
			+ "{"
			+ "\"encrypted_key\":\"dYOD28kab0Vvf4ODgxVAJXgHcSZICSOp8M51zj"
			+ "wj4w6Y5G4XJQsNNIBiqyvUUAOcpL7S7-cFe7Pio7gV_Q06WmCSa-"
			+ "vhW6me4bWrBf7cHwEQJdXihidAYWVajJIaKMXMvFRMV6iDlRr076"
			+ "DFthg2_AV0_tSiV6xSEIFqt1xnYPpmP91tc5WJDOGb-wqjw0-b-S"
			+ "1laS11QVbuP78dQ7Fa0zAVzzjHX-xvyM2wxj_otxr9clN1LnZMbe"
			+ "YSrRicJK5xodvWgkpIdkMHo4LvdhRRvzoKzlic89jFWPlnBq_V4n"
			+ "5trGuExtp_-dbHcGlihqc_wGgho9fLMK8JOArYLcMDNQ\","
			+ "\"header\":{" 
			+ "\"alg\":\"RSA1_5\","
			+ "\"kid\":\"frodo.baggins@hobbiton.example\""
			+ "}" 
			+ "}," 
			+ "{"
			+ "\"encrypted_key\":\"ExInT0io9BqBMYF6-maw5tZlgoZXThD1zWKsHi"
			+ "xJuw_elY4gSSId_w\"," 
			+ "\"header\":{" 
			+ "\"alg\":\"ECDH-ES+A256KW\","
			+ "\"kid\":\"peregrin.took@tuckborough.example\"," 
			+ "\"epk\":{"
			+ "\"kty\":\"EC\"," 
			+ "\"crv\":\"P-384\","
			+ "\"x\":\"Uzdvk3pi5wKCRc1izp5_r0OjeqT-I68i8g2b8mva8diRhs"
			+ "E2xAn2DtMRb25Ma2CX\","
			+ "\"y\":\"VDrRyFJh-Kwd1EjAgmj5Eo-CTHAZ53MC7PjjpLioy3ylEj"
			+ "I1pOMbw91fzZ84pbfm\""
			+ "}" 
			+ "}" 
			+ "}," 
			+ "{"
			+ "\"encrypted_key\":\"a7CclAejo_7JSuPB8zeagxXRam8dwCfmkt9-Wy"
			+ "TpS1E\"," 
			+ "\"header\":{" 
			+ "\"alg\":\"A256GCMKW\","
			+ "\"kid\":\"18ec08e1-bfa9-4d95-b205-2b4dd1d4321d\","
			+ "\"tag\":\"59Nqh1LlYtVIhfD3pgRGvw\","
			+ "\"iv\":\"AvpeoPZ9Ncn9mkBn\"" 
			+ "}" 
			+ "}" 
			+ "],"
			+ "\"unprotected\":{" 
			+ "\"cty\":\"text/plain\"" 
			+ "},"
			+ "\"protected\":\"eyJlbmMiOiJBMTI4Q0JDLUhTMjU2In0\","
			+ "\"iv\":\"VgEIHY20EnzUtZFl2RpB1g\","
			+ "\"ciphertext\":\"ajm2Q-OpPXCr7-MHXicknb1lsxLdXxK_yLds0KuhJzfWK"
			+ "04SjdxQeSw2L9mu3a_k1C55kCQ_3xlkcVKC5yr__Is48VOoK0k63_QRM"
			+ "9tBURMFqLByJ8vOYQX0oJW4VUHJLmGhF-tVQWB7Kz8mr8zeE7txF0MSa"
			+ "P6ga7-siYxStR7_G07Thd1jh-zGT0wxM5g-VRORtq0K6AXpLlwEqRp7p"
			+ "kt2zRM0ZAXqSpe1O6FJ7FHLDyEFnD-zDIZukLpCbzhzMDLLw2-8I14FQ"
			+ "rgi-iEuzHgIJFIJn2wh9Tj0cg_kOZy9BqMRZbmYXMY9YQjorZ_P_JYG3"
			+ "ARAIF3OjDNqpdYe-K_5Q5crGJSDNyij_ygEiItR5jssQVH2ofDQdLCht"
			+ "azE\"," 
			+ "\"tag\":\"BESYyFN7T09KY7i8zKs5_g\"" + "}";

	// Figure XX: Compact Serialization
	// TODO exceptions
	private final static String NOTVALIDCOMPACT = "a" + "." + "b" + "." + "c"
			+ "." + "d" + "." + "e";

	@Before
	public void before() {
		Security.addProvider(new BouncyCastleProvider());
	    try {
	        Field field = Class.forName("javax.crypto.JceSecurity").getDeclaredField("isRestricted");
	        field.setAccessible(true);
	        field.set(null, java.lang.Boolean.FALSE);
	    } catch (ClassNotFoundException | NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException ex) {
	        ex.printStackTrace(System.err);
	    }
	}

    /**
     * 5.1. Key Encryption using RSA v1.5 and AES-HMAC-SHA2
     */
    @Test
    public void keyEncryptionUsingRsaV15AndAesHmacSha2()
    {
        JweProtectedHeader protectedHeader = new JweProtectedHeader();
        protectedHeader.setAlgorithm(JwaAlgorithms.RSA1_5);
        protectedHeader.setKeyId("frodo.baggins@hobbiton.example");
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_CBC_HMAC_SHA_256);

        JwkKeySet jwks = new JwkKeySet(readKeySet("cookbookPrivateSet.txt"));
        List<JwkKey> keys = jwks.getKeys();
        // Figure 73: RSA 2048-Bit Key, in JWK Format
        JwkKey keyFigure73 = keys.get(2);     

        SecretKey cek = null;
        // Figure 174: Content Encryption Key, base64url-encoded
        cek = new SecretKeySpec(JoseUtils.base64URLTobytes(FIGURE74), "AES");

        JweCookbookBypass cookbookBypass = new JweCookbookBypass();
        cookbookBypass.setBypassContentEncryptionKey(cek);
        cookbookBypass.setBypassInitializationVectorForContentEncryption(FIGURE75);

        // Figure 72: Plaintext content
        JweDocument jwe = JweMaker.generateFromMessageForJoseCookBook(cookbookBypass, FIGURE72.getBytes(), null, keyFigure73, protectedHeader, null,
                (JweRecipient[]) null);

        // RSAES-PKCS1-v1_5 uses random data to generate the encrypted key

        // assertEquals(FIGURE81, jwe.getCompactSerialization());
        assertEquals(FIGURE81.length(), jwe.getCompactSerialization().length());
        JweDocument verifiyJwe = JweMaker.generateFromJwe(jwe.getCompactSerialization());
        assertEquals(FIGURE81.length(), verifiyJwe.getCompactSerialization().length());

        // assertEquals(FIGURE82, jwe.getJsonSerialization());
        assertEquals(FIGURE82.length(), jwe.getJsonSerialization().length());
        verifiyJwe = JweMaker.generateFromJwe(jwe.getJsonSerialization());
        assertEquals(FIGURE82.length(), verifiyJwe.getJsonSerialization().length());

        // assertEquals(FIGURE83, jwe.getJsonFlattenedSerialization());
        assertEquals(FIGURE83.length(), jwe.getJsonFlattenedSerialization().length());
        verifiyJwe = JweMaker.generateFromJwe(jwe.getJsonFlattenedSerialization());
        assertEquals(FIGURE83.length(), verifiyJwe.getJsonFlattenedSerialization().length());

        assertEquals(new String(verifiyJwe.decrypt(keyFigure73)), FIGURE72);
    }

    /**
     * 5.2. Key Encryption using RSA-OAEP with AES-GCM
     */
    @Test
    public void keyEncryptionUsingRsaOaepWithAesGcm()
    {
        JweProtectedHeader protectedHeader = new JweProtectedHeader();
        protectedHeader.setAlgorithm(JwaAlgorithms.RSA_OAEP);
        protectedHeader.setKeyId("samwise.gamgee@hobbiton.example");
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_GCM);

        JwkKeySet jwks = new JwkKeySet(readKeySet("cookbookPrivateSet.txt"));
        List<JwkKey> keys = jwks.getKeys();
        // Figure 84: RSA 4096-Bit Key
        JwkKey keyFigure84 = keys.get(3);

        SecretKey cek = null;
        // Figure 174: Content Encryption Key, base64url-encoded
        cek = new SecretKeySpec(JoseUtils.base64URLTobytes(FIGURE85), "AES");

        JweCookbookBypass cookbookBypass = new JweCookbookBypass();
        cookbookBypass.setBypassContentEncryptionKey(cek);
        cookbookBypass.setBypassInitializationVectorForContentEncryption(FIGURE86);

        // Figure 72: Plaintext content
        JweDocument jwe = JweMaker.generateFromMessageForJoseCookBook(cookbookBypass, FIGURE72.getBytes(), null, keyFigure84, protectedHeader, null,
                (JweRecipient[]) null);

        // RSAES-OAEP uses random data to generate the encrypted key

        // assertEquals(FIGURE92, jwe.getCompactSerialization());
        assertEquals(FIGURE92.length(), jwe.getCompactSerialization().length());
        JweDocument verifiyJwe = JweMaker.generateFromJwe(jwe.getCompactSerialization());
        assertEquals(FIGURE92.length(), verifiyJwe.getCompactSerialization().length());

        // assertEquals(FIGURE93, jwe.getJsonSerialization());
        assertEquals(FIGURE93.length(), jwe.getJsonSerialization().length());
        verifiyJwe = JweMaker.generateFromJwe(jwe.getJsonSerialization());
        assertEquals(FIGURE93.length(), verifiyJwe.getJsonSerialization().length());

        // assertEquals(FIGURE94, jwe.getJsonFlattenedSerialization());
        assertEquals(FIGURE94.length(), jwe.getJsonFlattenedSerialization().length());
        verifiyJwe = JweMaker.generateFromJwe(jwe.getJsonFlattenedSerialization());
        assertEquals(FIGURE94.length(), verifiyJwe.getJsonFlattenedSerialization().length());

        assertEquals(FIGURE72, new String(verifiyJwe.decrypt(keyFigure84)));
    }

    /**
     * 5.3. Key Wrap using PBES2-AES-KeyWrap with AES-CBC-HMAC-SHA2
     */
    @Test
    public void keyWrapUsingPbes2AesKeyWrapWithAesCbcHmacSha2()
    {
        JweProtectedHeader protectedHeader = new JweProtectedHeader();
        protectedHeader.setAlgorithm(JwaAlgorithms.PBES2_HS512_A256KW);
        protectedHeader.setContentType("jwk-set+json");
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_CBC_HMAC_SHA_256);

        String jwk = "{\"k\":\"" + JoseUtils.bytesToBase64URLString(FIGURE96.getBytes()) + "\"" + "}";
        JwkKey pbesSecret = new JwkKey(jwk);
        
        SecretKey cek = new SecretKeySpec(JoseUtils.base64URLTobytes(FIGURE97), "AES");

        JweCookbookBypass cookbookBypass = new JweCookbookBypass();
        cookbookBypass.setBypassContentEncryptionKey(cek);
        cookbookBypass.setBypassInitializationVectorForContentEncryption(FIGURE98);
        cookbookBypass.setBypassPbes2Salt(FIGURE99);
        cookbookBypass.setBypassPbes2IterationCount(8192);

        // Figure 95: Plaintext content
        JweDocument jwe = JweMaker.generateFromMessageForJoseCookBook(cookbookBypass, FIGURE95.getBytes(), null, pbesSecret, protectedHeader, null,
                (JweRecipient[]) null);

        assertEquals(FIGURE105, jwe.getCompactSerialization());
        JweDocument verifiyJwe = JweMaker.generateFromJwe(jwe.getCompactSerialization());
        assertEquals(FIGURE105, verifiyJwe.getCompactSerialization());

        assertEquals(FIGURE106, jwe.getJsonSerialization());
        verifiyJwe = JweMaker.generateFromJwe(jwe.getJsonSerialization());
        assertEquals(FIGURE106, verifiyJwe.getJsonSerialization());

        assertEquals(FIGURE107, jwe.getJsonFlattenedSerialization());
        verifiyJwe = JweMaker.generateFromJwe(jwe.getJsonFlattenedSerialization());
        assertEquals(FIGURE107, verifiyJwe.getJsonFlattenedSerialization());

        assertEquals(FIGURE95, new String(verifiyJwe.decrypt(pbesSecret)));
    }

    /**
     * 5.4. Key Agreement with Key Wrapping using ECDH-ES and AES-KeyWrap with
     * AES-GCM
     */

    @Test
    public void keyAgreementWithKeyWrappingUsingEcdhEsAndAesKeyWrapWithAesGcm()
    {
        JweProtectedHeader protectedHeader = new JweProtectedHeader();
        protectedHeader.setAlgorithm(JwaAlgorithms.ECDH_ES_AES128KW);
        protectedHeader.setKeyId("peregrin.took@tuckborough.example");
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_GCM);
        
        JwkKeySet jwks = new JwkKeySet(readKeySet("cookbookPublicSet.txt"));
        List<JwkKey> keys = jwks.getKeys();
        JwkKey keyFigure108 = keys.get(3);

        jwks = new JwkKeySet(readKeySet("cookbookPrivateSet.txt"));
        keys = jwks.getKeys();
        JwkKey keyFigure111 = keys.get(4);
        ECPrivateKey ephemeralPrivateKey = JwkUtils.convertJwkToECPrivateKey(keyFigure111);
        ECPublicKey publicKey = (ECPublicKey) JwkUtils.convertJwkToECPublicKey(keyFigure111);

        SecretKey cek = new SecretKeySpec(JoseUtils.base64URLTobytes(FIGURE109), "AES");

        JweCookbookBypass cookbookBypass = new JweCookbookBypass();
        cookbookBypass.setBypassContentEncryptionKey(cek);
        cookbookBypass.setBypassInitializationVectorForContentEncryption(FIGURE110);
        cookbookBypass.setBypassEcdhEphemeralPrivateKey(ephemeralPrivateKey);
        cookbookBypass.setBypassEcdhEphemeralPublicKey(publicKey);

        // Figure 72: Plaintext content
        JweDocument jwe = JweMaker.generateFromMessageForJoseCookBook(cookbookBypass, FIGURE72.getBytes(), null, keyFigure108, protectedHeader, null,
                (JweRecipient[]) null);

        assertEquals(FIGURE117, jwe.getCompactSerialization());
        JweDocument verifiyJwe = JweMaker.generateFromJwe(jwe.getCompactSerialization());
        assertEquals(FIGURE117, verifiyJwe.getCompactSerialization());

        assertEquals(FIGURE118, jwe.getJsonSerialization());
        verifiyJwe = JweMaker.generateFromJwe(jwe.getJsonSerialization());
        assertEquals(FIGURE118, verifiyJwe.getJsonSerialization());

        assertEquals(FIGURE119, jwe.getJsonFlattenedSerialization());
        verifiyJwe = JweMaker.generateFromJwe(jwe.getJsonFlattenedSerialization());
        assertEquals(FIGURE119, verifiyJwe.getJsonFlattenedSerialization());

        assertEquals(FIGURE72, new String(verifiyJwe.decrypt(keyFigure108)));
    }

    /**
     * 5.5. Key Agreement using ECDH-ES with AES-CBC-HMAC-SHA2
     */
    @Test
    public void keyAgreementUsingEcdhEsWithAesCbcHmacSha2()
    {
        JweProtectedHeader protectedHeader = new JweProtectedHeader();
        protectedHeader.setAlgorithm(JwaAlgorithms.ECDH_ES);
        protectedHeader.setKeyId("meriadoc.brandybuck@buckland.example");
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_CBC_HMAC_SHA_256);
        
        JwkKeySet jwks = new JwkKeySet(readKeySet("cookbookPublicSet.txt"));
        List<JwkKey> keys = jwks.getKeys();
        JwkKey keyFigure120 = keys.get(4);

        jwks = new JwkKeySet(readKeySet("cookbookPrivateSet.txt"));
        keys = jwks.getKeys();
        JwkKey keyFigure122 = keys.get(5);
        ECPrivateKey ephemeralPrivateKey = JwkUtils.convertJwkToECPrivateKey(keyFigure122);
        ECPublicKey publicKey = (ECPublicKey) JwkUtils.convertJwkToECPublicKey(keyFigure122);

        JweCookbookBypass cookbookBypass = new JweCookbookBypass();
        cookbookBypass.setBypassEcdhEphemeralPrivateKey(ephemeralPrivateKey);
        cookbookBypass.setBypassEcdhEphemeralPublicKey(publicKey);
        cookbookBypass.setBypassInitializationVectorForContentEncryption(FIGURE121);

        // Figure 72: Plaintext content
        JweDocument jwe = JweMaker.generateFromMessageForJoseCookBook(cookbookBypass, FIGURE72.getBytes(), null, keyFigure120, protectedHeader, null,
                (JweRecipient[]) null);

        assertEquals(FIGURE128, jwe.getCompactSerialization());
        JweDocument verifiyJwe = JweMaker.generateFromJwe(jwe.getCompactSerialization());
        assertEquals(FIGURE128, verifiyJwe.getCompactSerialization());

        assertEquals(FIGURE129, jwe.getJsonSerialization());
        verifiyJwe = JweMaker.generateFromJwe(jwe.getJsonSerialization());
        assertEquals(FIGURE129, verifiyJwe.getJsonSerialization());

        // Json Serialization and Json Flattened Serialization are equal
        assertEquals(FIGURE129, jwe.getJsonFlattenedSerialization());
        verifiyJwe = JweMaker.generateFromJwe(jwe.getJsonFlattenedSerialization());
        assertEquals(FIGURE129, verifiyJwe.getJsonFlattenedSerialization());

        assertEquals(FIGURE72, new String(verifiyJwe.decrypt(keyFigure120)));
    }

    /**
     * 5.6. Direct Encryption using AES-GCM
     */
    @Test
    public void directEncryptionUsingAesGcm()
    {
        JweProtectedHeader protectedHeader = new JweProtectedHeader();
        protectedHeader.setAlgorithm(JwaAlgorithms.DIRECT);
        protectedHeader.setKeyId("77c7e2b8-6e13-45cf-8672-617b5b45243a");
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_GCM);

        JwkKeySet jwks = new JwkKeySet(readKeySet("cookbookSecretSet.txt"));
        List<JwkKey> keys = jwks.getKeys();
        JwkKey keyFigure130 = keys.get(2);

        JweCookbookBypass cookbookBypass = new JweCookbookBypass();
        cookbookBypass.setBypassInitializationVectorForContentEncryption(FIGURE131);

        // Figure 72: Plaintext content
        JweDocument jwe = JweMaker.generateFromMessageForJoseCookBook(cookbookBypass, FIGURE72.getBytes(), null, keyFigure130, protectedHeader, null,
                (JweRecipient[]) null);

        assertEquals(FIGURE136, jwe.getCompactSerialization());
        JweDocument verifiyJwe = JweMaker.generateFromJwe(jwe.getCompactSerialization());
        assertEquals(FIGURE136, verifiyJwe.getCompactSerialization());

        assertEquals(FIGURE137, jwe.getJsonSerialization());
        verifiyJwe = JweMaker.generateFromJwe(jwe.getJsonSerialization());
        assertEquals(FIGURE137, verifiyJwe.getJsonSerialization());

        // Json Serialization and Json Flattened Serialization are equal
        assertEquals(FIGURE137, jwe.getJsonFlattenedSerialization());
        verifiyJwe = JweMaker.generateFromJwe(jwe.getJsonFlattenedSerialization());
        assertEquals(FIGURE137, verifiyJwe.getJsonFlattenedSerialization());

        assertEquals(FIGURE72, new String(verifiyJwe.decrypt(keyFigure130)));
    }

    /**
     * 5.7. Key Wrap using AES-GCM KeyWrap with AES-CBC-HMAC-SHA2
     */
    @Test
    public void keyWrapUsingAesGcmKeyWrapWithAesCbcHmacSha2()
    {
        JweProtectedHeader protectedHeader = new JweProtectedHeader();
        protectedHeader.setAlgorithm(JwaAlgorithms.AES_256_GCMKW);
        protectedHeader.setKeyId("18ec08e1-bfa9-4d95-b205-2b4dd1d4321d");
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_CBC_HMAC_SHA_256);

        JwkKeySet jwks = new JwkKeySet(readKeySet("cookbookSecretSet.txt"));
        List<JwkKey> keys = jwks.getKeys();
        // Figure 138: AES 256-bit Key
        JwkKey keyFigure138 = keys.get(4);

        SecretKey cek = null;
        // Figure 152: Content Encryption Key, base64url-encoded
        cek = new SecretKeySpec(JoseUtils.base64URLTobytes(FIGURE139), "AES");

        JweCookbookBypass cookbookBypass = new JweCookbookBypass();
        cookbookBypass.setBypassContentEncryptionKey(cek);
        cookbookBypass.setBypassInitializationVectorForContentEncryption(FIGURE140);
        cookbookBypass.setBypassInitializationVectorForKeyEncryption(FIGURE141);

        // Figure 72: Plaintext content
        JweDocument jwe = JweMaker.generateFromMessageForJoseCookBook(cookbookBypass, FIGURE72.getBytes(), null, keyFigure138, protectedHeader, null,
                (JweRecipient[]) null);

        assertEquals(FIGURE148, jwe.getCompactSerialization());
        JweDocument verifiyJwe = JweMaker.generateFromJwe(jwe.getCompactSerialization());
        assertEquals(FIGURE148, verifiyJwe.getCompactSerialization());

        assertEquals(FIGURE149, jwe.getJsonSerialization());
        verifiyJwe = JweMaker.generateFromJwe(jwe.getJsonSerialization());
        assertEquals(FIGURE149, verifiyJwe.getJsonSerialization());

        // TODO validation fails because of different protected header order
        // Compact Serialization order: alg, kid, tag, iv, enc
        // Flattened Serialization order: alg, iv, kid, tag, enc
//        assertEquals(FIGURE150, jwe.getJsonFlattenedSerialization());
//        verifiyJwe = JweMaker.generateFromJwe(jwe.getJsonFlattenedSerialization());
//        assertEquals(FIGURE150, verifiyJwe.getJsonFlattenedSerialization());

        assertEquals(FIGURE72, new String(verifiyJwe.decrypt(keyFigure138)));
    }

    /**
     * 5.8. Key Wrap using AES-KeyWrap with AES-GCM
     */
    @Test
    public void keyWrapUsingAesKeyWrapWithAesGcm()
    {
        JweProtectedHeader protectedHeader = new JweProtectedHeader();
        protectedHeader.setAlgorithm(JwaAlgorithms.AES_128_KW);
        protectedHeader.setKeyId("81b20965-8332-43d9-a468-82160ad91ac8");
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_GCM);

        JwkKeySet jwks = new JwkKeySet(readKeySet("cookbookSecretSet.txt"));
        List<JwkKey> keys = jwks.getKeys();
        // Figure 151: AES 128-Bit Key
        JwkKey keyFigure151 = keys.get(3);

        SecretKey cek = null;
        // Figure 152: Content Encryption Key, base64url-encoded
        cek = new SecretKeySpec(JoseUtils.base64URLTobytes(FIGURE152), "AES");

        JweCookbookBypass cookbookBypass = new JweCookbookBypass();
        cookbookBypass.setBypassContentEncryptionKey(cek);
        cookbookBypass.setBypassInitializationVectorForContentEncryption(FIGURE153);

        // Figure 72: Plaintext content
        JweDocument jwe = JweMaker.generateFromMessageForJoseCookBook(cookbookBypass, FIGURE72.getBytes(), null, keyFigure151, protectedHeader, null,
                (JweRecipient[]) null);

        assertEquals(FIGURE159, jwe.getCompactSerialization());
        JweDocument verifiyJwe = JweMaker.generateFromJwe(jwe.getCompactSerialization());
        assertEquals(FIGURE159, verifiyJwe.getCompactSerialization());

        assertEquals(FIGURE160, jwe.getJsonSerialization());
        verifiyJwe = JweMaker.generateFromJwe(jwe.getJsonSerialization());
        assertEquals(FIGURE160, verifiyJwe.getJsonSerialization());

        assertEquals(FIGURE161, jwe.getJsonFlattenedSerialization());
        verifiyJwe = JweMaker.generateFromJwe(jwe.getJsonFlattenedSerialization());
        assertEquals(FIGURE161, verifiyJwe.getJsonFlattenedSerialization());

        assertEquals(new String(verifiyJwe.decrypt(keyFigure151)), FIGURE72);
    }

    /**
     * 5.9. Compressed Content
     */

    @Test
    public void compressedContent()
    {
        JweProtectedHeader protectedHeader = new JweProtectedHeader();
        protectedHeader.setAlgorithm(JwaAlgorithms.AES_128_KW);
        protectedHeader.setKeyId("81b20965-8332-43d9-a468-82160ad91ac8");
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_GCM);
        protectedHeader.setZipAlgorithm("DEF");

        JwkKeySet jwks = new JwkKeySet(readKeySet("cookbookSecretSet.txt"));
        List<JwkKey> keys = jwks.getKeys();
        // Figure 151: AES 128-Bit Key
        JwkKey keyFigure151 = keys.get(3);

        SecretKey cek = null;
        // Figure 163: Content Encryption Key, base64url-encoded
        cek = new SecretKeySpec(JoseUtils.base64URLTobytes(FIGURE163), "AES");

        JweCookbookBypass cookbookBypass = new JweCookbookBypass();
        cookbookBypass.setBypassContentEncryptionKey(cek);
        cookbookBypass.setBypassInitializationVectorForContentEncryption(FIGURE164);

        // Figure 72: Plaintext content
        JweDocument jwe = JweMaker.generateFromMessageForJoseCookBook(cookbookBypass, FIGURE72.getBytes(), null, keyFigure151, protectedHeader, null,
                (JweRecipient[]) null);

        assertEquals(FIGURE170, jwe.getCompactSerialization());
        JweDocument verifiyJwe = JweMaker.generateFromJwe(jwe.getCompactSerialization());
        assertEquals(FIGURE170, verifiyJwe.getCompactSerialization());

        assertEquals(FIGURE171, jwe.getJsonSerialization());
        verifiyJwe = JweMaker.generateFromJwe(jwe.getJsonSerialization());
        assertEquals(FIGURE171, verifiyJwe.getJsonSerialization());

        assertEquals(FIGURE172, jwe.getJsonFlattenedSerialization());
        verifiyJwe = JweMaker.generateFromJwe(jwe.getJsonFlattenedSerialization());
        assertEquals(FIGURE172, verifiyJwe.getJsonFlattenedSerialization());

        assertEquals(new String(verifiyJwe.decrypt(keyFigure151)), FIGURE72);
    }

    /**
     * 5.10. Including Additional Authenticated Data
     */
    @Test
    public void includingAdditionalAuthenticationData()
    {
        JweProtectedHeader protectedHeader = new JweProtectedHeader();
        protectedHeader.setAlgorithm(JwaAlgorithms.AES_128_KW);
        
        JwkKeySet jwks = new JwkKeySet(readKeySet("cookbookSecretSet.txt"));
        List<JwkKey> keys = jwks.getKeys();
        JwkKey keyFigure151 = keys.get(3);

        protectedHeader.setKeyId(keyFigure151.getKid());
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_GCM);
        
        SecretKey cek = null;
        // Figure 174: Content Encryption Key, base64url-encoded
        cek = new SecretKeySpec(JoseUtils.base64URLTobytes(FIGURE174), "AES");

        JweCookbookBypass cookbookBypass = new JweCookbookBypass();
        cookbookBypass.setBypassContentEncryptionKey(cek);
        cookbookBypass.setBypassInitializationVectorForContentEncryption(FIGURE175);

        // Figure 72: Plaintext content
        // Figure 173: Additional Authenticated Data, in JSON format
        JweDocument jwe = JweMaker.generateFromMessageForJoseCookBook(cookbookBypass, FIGURE72.getBytes(), FIGURE173, keyFigure151, protectedHeader,
                null, (JweRecipient[]) null);

        // no Compact Serialization because of additional authentication data
        try
        {
            jwe.getCompactSerialization();
            fail("no Compact Serialization because of additional authentication data");
        } catch (SecurityException expected)
        {
        }      
        
        assertEquals(FIGURE182, jwe.getJsonSerialization());
        JweDocument verifiyJwe = JweMaker.generateFromJwe(jwe.getJsonSerialization());
        assertEquals(FIGURE182, verifiyJwe.getJsonSerialization());

        assertEquals(FIGURE183, jwe.getJsonFlattenedSerialization());
        verifiyJwe = JweMaker.generateFromJwe(jwe.getJsonFlattenedSerialization());
        assertEquals(FIGURE183, verifiyJwe.getJsonFlattenedSerialization());

        assertEquals(FIGURE72, new String(verifiyJwe.decrypt(keyFigure151)));
    }

    /**
     * 5.11. Protecting Specific Header Fields
     */
    @Test
    public void protectingSpecificHeaderFields()
    {
        JweProtectedHeader protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_GCM);
        
        JweSharedUnprotectedHeader unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setAlgorithm(JwaAlgorithms.AES_128_KW);
        
        JwkKeySet jwks = new JwkKeySet(readKeySet("cookbookSecretSet.txt"));
        List<JwkKey> keys = jwks.getKeys();
        JwkKey keyFigure151 = keys.get(3);

        unprotectedHeader.setKeyId(keyFigure151.getKid());

        SecretKey cek = null;
        // Figure 184: Content Encryption Key, base64url-encoded
        cek = new SecretKeySpec(JoseUtils.base64URLTobytes(FIGURE184), "AES");

        JweCookbookBypass cookbookBypass = new JweCookbookBypass();
        cookbookBypass.setBypassContentEncryptionKey(cek);
        cookbookBypass.setBypassInitializationVectorForContentEncryption(FIGURE185);

        // Figure 72: Plaintext content
        JweDocument jwe = JweMaker.generateFromMessageForJoseCookBook(cookbookBypass, FIGURE72.getBytes(), null, keyFigure151, protectedHeader,
                unprotectedHeader, (JweRecipient[]) null);

        // no Compact Serialization because of JWE Shared Unprotected Header
        try
        {
            jwe.getCompactSerialization();
            fail("no Compact Serialization because of JWE Shared Unprotected Header");
        } catch (SecurityException expected)
        {
        }  
        
        assertEquals(FIGURE192, jwe.getJsonSerialization());
        JweDocument verifiyJwe = JweMaker.generateFromJwe(jwe.getJsonSerialization());
        assertEquals(FIGURE192, verifiyJwe.getJsonSerialization());

        assertEquals(FIGURE193, jwe.getJsonFlattenedSerialization());
        verifiyJwe = JweMaker.generateFromJwe(jwe.getJsonFlattenedSerialization());
        assertEquals(FIGURE193, verifiyJwe.getJsonFlattenedSerialization());

        assertEquals(FIGURE72, new String(verifiyJwe.decrypt(keyFigure151)));
    }

    /**
     * 5.12. Protecting_Content_Only
     */
    @Test
    public void protectingContentOnly()
    {
        JweSharedUnprotectedHeader unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setAlgorithm(JwaAlgorithms.AES_128_KW);
        
        JwkKeySet jwks = new JwkKeySet(readKeySet("cookbookSecretSet.txt"));
        List<JwkKey> keys = jwks.getKeys();
        JwkKey keyFigure151 = keys.get(3);

        unprotectedHeader.setKeyId(keyFigure151.getKid());
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_GCM);

        SecretKey cek = null;
        // Figure 194: Content Encryption Key, base64url-encoded
        cek = new SecretKeySpec(JoseUtils.base64URLTobytes(FIGURE194), "AES");

        JweCookbookBypass cookbookBypass = new JweCookbookBypass();
        cookbookBypass.setBypassContentEncryptionKey(cek);
        cookbookBypass.setBypassInitializationVectorForContentEncryption(FIGURE195);

        // Figure 72: Plaintext content
        JweDocument jwe = JweMaker.generateFromMessageForJoseCookBook(cookbookBypass, FIGURE72.getBytes(), null, keyFigure151, null,
                unprotectedHeader, (JweRecipient[]) null);

        // no Compact Serialization because of JWE Shared Unprotected Header
        try
        {
            jwe.getCompactSerialization();
            fail("no Compact Serialization because of JWE Shared Unprotected Header");
        } catch (SecurityException expected)
        {
        }  
        
        assertEquals(FIGURE200, jwe.getJsonSerialization());
        JweDocument verifiyJwe = JweMaker.generateFromJwe(jwe.getJsonSerialization());
        assertEquals(FIGURE200, verifiyJwe.getJsonSerialization());

        assertEquals(FIGURE201, jwe.getJsonFlattenedSerialization());
        verifiyJwe = JweMaker.generateFromJwe(jwe.getJsonFlattenedSerialization());
        assertEquals(FIGURE201, verifiyJwe.getJsonFlattenedSerialization());

        assertEquals(FIGURE72, new String(verifiyJwe.decrypt(keyFigure151)));
    }

    /**
     * 5.13. Encrypting to Multiple Recipients
     */

    @Test
    public void encryptionToMultipleRecipients()
    {
        JweProtectedHeader protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_CBC_HMAC_SHA_256);

        JweSharedUnprotectedHeader unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentType("text/plain");

        JwkKeySet publicJwks = new JwkKeySet(readKeySet("cookbookPublicSet.txt"));
        List<JwkKey> publicKeys = publicJwks.getKeys();
        JwkKeySet privateJwks = new JwkKeySet(readKeySet("cookbookPrivateSet.txt"));
        List<JwkKey> privateKeys = privateJwks.getKeys();
        JwkKeySet secretJwks = new JwkKeySet(readKeySet("cookbookSecretSet.txt"));
        List<JwkKey> secretKeys = secretJwks.getKeys();

        // The RSA public key from Figure 73 for the first recipient.
        // Figure 73: RSA 2048-bit Key JwkKey
        JwkKey keyFigure73 = publicKeys.get(2);

        // The EC public key from Figure 108 for the second recipient.
        // Figure 108: Elliptic Curve P-384 Key, in JWK format JwkKey
        JwkKey keyFigure108 = publicKeys.get(3);

        // Figure 138: AES 256-bit Key JwkKey keyFigure138 = secretKeys.get(4);
        JwkKey keyFigure138 = secretKeys.get(4);

        // recipient One, Frodo
        // --------------------
        JwePerRecipientUnprotectedHeader frodosHeader = new JwePerRecipientUnprotectedHeader();
        frodosHeader.setAlgorithm(JwaAlgorithms.RSA1_5);
        frodosHeader.setKeyId("frodo.baggins@hobbiton.example");

        JweRecipient frodo = new JweRecipient(frodosHeader, keyFigure73);

        // recipient Two, Peregrin
        // -----------------------
        JwePerRecipientUnprotectedHeader peregrinsHeader = new JwePerRecipientUnprotectedHeader();
        peregrinsHeader.setAlgorithm(JwaAlgorithms.ECDH_ES_AES256KW);
        peregrinsHeader.setKeyId("peregrin.took@tuckborough.example");

        JweRecipient peregrin = new JweRecipient(peregrinsHeader, keyFigure108);

        // recipient Three
        // ---------------
        JwePerRecipientUnprotectedHeader recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setAlgorithm(JwaAlgorithms.AES_256_GCMKW);
        recipientHeader.setKeyId("18ec08e1-bfa9-4d95-b205-2b4dd1d4321d");

        JweRecipient recipient = new JweRecipient(recipientHeader, keyFigure138);

        // Figure 202: Content Encryption Key
        SecretKey cek = null;
        cek = new SecretKeySpec(JoseUtils.base64URLTobytes(FIGURE202), "AES");

        JweCookbookBypass cookbookBypass = new JweCookbookBypass();
        cookbookBypass.setBypassContentEncryptionKey(cek);
        cookbookBypass.setBypassInitializationVectorForContentEncryption(FIGURE203);
        cookbookBypass.setBypassInitializationVectorForKeyEncryption(FIGURE211);

        JwkKey keyFigure207 = privateKeys.get(6);
        ECPrivateKey ephemeralPrivateKey = JwkUtils.convertJwkToECPrivateKey(keyFigure207);
        ECPublicKey publicKey = (ECPublicKey) JwkUtils.convertJwkToECPublicKey(keyFigure207);

        cookbookBypass.setBypassEcdhEphemeralPrivateKey(ephemeralPrivateKey);
        cookbookBypass.setBypassEcdhEphemeralPublicKey(publicKey);

        // Figure 72: Plaintext content
        JweDocument jwe = JweMaker.generateFromMessageForJoseCookBook(cookbookBypass, FIGURE72.getBytes(), null, null, protectedHeader,
                unprotectedHeader, frodo, peregrin, recipient);

        // no Compact Serialization because of multiple recipients
        try
        {
            jwe.getCompactSerialization();
            fail("Only for one recipient case");
        } catch (SecurityException expected)
        {
        }

        assertEquals(FIGURE221.length(), jwe.getJsonSerialization().length());
        JweDocument verifiyJwe = JweMaker.generateFromJwe(jwe.getJsonSerialization());
        assertEquals(FIGURE221.length(), verifiyJwe.getJsonSerialization().length());

        // no Json Flattened Serialization because of multiple recipients
        try
        {
            jwe.getJsonFlattenedSerialization();
            fail("Only for one recipient case");
        } catch (SecurityException expected)
        {
        }

        // Frodo decrypts
        assertEquals(FIGURE72, new String(verifiyJwe.decrypt(keyFigure73)));
        // Peregrin decrypts
        assertEquals(FIGURE72, new String(verifiyJwe.decrypt(keyFigure108)));
        // recipient 3 decrypts
        assertEquals(FIGURE72, new String(verifiyJwe.decrypt(keyFigure138)));
    }

    private String readKeySet(String fileName)
    {
        InputStream is = this.getClass().getClassLoader().getResourceAsStream(fileName);
        StringBuilder sb = new StringBuilder(1024);

        try
        {
            for (int i = is.read(); i != -1; i = is.read())
            {
                sb.append((char) i);
            }
        } catch (IOException e)
        {
            e.printStackTrace();
        }
        
        try
        {
            is.close();
        } catch (IOException e)
        {
            e.printStackTrace();
        }
        return sb.toString();
    }
}
