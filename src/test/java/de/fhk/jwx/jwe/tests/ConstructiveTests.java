package de.fhk.jwx.jwe.tests;

import java.io.IOException;
import java.io.InputStream;
import java.security.Security;
import java.util.List;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.Before;
import org.junit.Test;

import de.fhk.jwx.jwa.JwaAlgorithms;
import de.fhk.jwx.jwe.JweDocument;
import de.fhk.jwx.jwe.JweMaker;
import de.fhk.jwx.jwe.JwePerRecipientUnprotectedHeader;
import de.fhk.jwx.jwe.JweProtectedHeader;
import de.fhk.jwx.jwe.JweRecipient;
import de.fhk.jwx.jwe.JweSharedUnprotectedHeader;
import de.fhk.jwx.jwk.JwkKey;
import de.fhk.jwx.jwk.JwkKeySet;
import de.fhk.jwx.util.JoseUtils;

public class ConstructiveTests
{
    private final byte[] PAYLOAD = ("You can trust us to stick with you through thick and thin\\xe2\\x80\\x93to the bitter end. "
            + "And you can trust us to keep any secret of yours\\xe2\\x80\\x93closer than you keep it yourself. "
            + "But you cannot trust us to let you face trouble alone, and go off without a word. We are your friends, Frodo.").getBytes();

    @Before
    public void before() {
        Security.addProvider(new BouncyCastleProvider());
    }
    
    @Test
    public void encAlgCombinationProtectedHeaderTest()
    {    
        final JwkKey SECRET128BIT = new JwkKeySet(readKeySet("constructiveTestsSecretSet.txt")).getKeys().get(0);
        final JwkKey SECRET192BIT = new JwkKeySet(readKeySet("constructiveTestsSecretSet.txt")).getKeys().get(1);
        final JwkKey SECRET256BIT = new JwkKeySet(readKeySet("constructiveTestsSecretSet.txt")).getKeys().get(2);
        final JwkKey SECRET384BIT = new JwkKeySet(readKeySet("constructiveTestsSecretSet.txt")).getKeys().get(3);
        final JwkKey SECRET512BIT = new JwkKeySet(readKeySet("constructiveTestsSecretSet.txt")).getKeys().get(4);
        
        String jwk = "{\"k\":\"" + JoseUtils.bytesToBase64URLString("One today is worth two tomorrows".getBytes()) + "\"" + "}";
        JwkKey pbesSecret = new JwkKey(jwk);
       
        JwkKeySet jwks = new JwkKeySet(readKeySet("cookbookPublicSet.txt"));
        List<JwkKey> keys = jwks.getKeys();
        JwkKey ecdhPublicKey = keys.get(4);
        JwkKey rsaPublicKey = keys.get(2);    
        
        final String AAD = "aad";
        
        JweProtectedHeader protectedHeader = null;
        @SuppressWarnings("unused")
        JweDocument jwe = null;
        
        // AES_128_GCMKW
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_GCM);
        protectedHeader.setAlgorithm(JwaAlgorithms.AES_128_GCMKW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET128BIT, protectedHeader);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET128BIT, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_GCM);
        protectedHeader.setAlgorithm(JwaAlgorithms.AES_128_GCMKW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET128BIT, protectedHeader);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET128BIT, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_GCM);
        protectedHeader.setAlgorithm(JwaAlgorithms.AES_128_GCMKW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET128BIT, protectedHeader);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET128BIT, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_CBC_HMAC_SHA_256);
        protectedHeader.setAlgorithm(JwaAlgorithms.AES_128_GCMKW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET128BIT, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_CBC_HMAC_SHA_384);
        protectedHeader.setAlgorithm(JwaAlgorithms.AES_128_GCMKW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET128BIT, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_CBC_HMAC_SHA_512);
        protectedHeader.setAlgorithm(JwaAlgorithms.AES_128_GCMKW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET128BIT, protectedHeader);
        
        // AES_192_GCMKW
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_GCM);
        protectedHeader.setAlgorithm(JwaAlgorithms.AES_192_GCMKW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET192BIT, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_GCM);
        protectedHeader.setAlgorithm(JwaAlgorithms.AES_192_GCMKW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET192BIT, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_GCM);
        protectedHeader.setAlgorithm(JwaAlgorithms.AES_192_GCMKW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET192BIT, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_CBC_HMAC_SHA_256);
        protectedHeader.setAlgorithm(JwaAlgorithms.AES_192_GCMKW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET192BIT, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_CBC_HMAC_SHA_384);
        protectedHeader.setAlgorithm(JwaAlgorithms.AES_192_GCMKW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET192BIT, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_CBC_HMAC_SHA_512);
        protectedHeader.setAlgorithm(JwaAlgorithms.AES_192_GCMKW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET192BIT, protectedHeader);
        
        // AES_256_GCMKW
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_GCM);
        protectedHeader.setAlgorithm(JwaAlgorithms.AES_256_GCMKW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET256BIT, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_GCM);
        protectedHeader.setAlgorithm(JwaAlgorithms.AES_256_GCMKW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET256BIT, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_GCM);
        protectedHeader.setAlgorithm(JwaAlgorithms.AES_256_GCMKW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET256BIT, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_CBC_HMAC_SHA_256);
        protectedHeader.setAlgorithm(JwaAlgorithms.AES_256_GCMKW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET256BIT, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_CBC_HMAC_SHA_384);
        protectedHeader.setAlgorithm(JwaAlgorithms.AES_256_GCMKW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET256BIT, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_CBC_HMAC_SHA_512);
        protectedHeader.setAlgorithm(JwaAlgorithms.AES_256_GCMKW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET256BIT, protectedHeader);
        
        // AES_128_KW
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_GCM);
        protectedHeader.setAlgorithm(JwaAlgorithms.AES_128_KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET128BIT, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_GCM);
        protectedHeader.setAlgorithm(JwaAlgorithms.AES_128_KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET128BIT, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_GCM);
        protectedHeader.setAlgorithm(JwaAlgorithms.AES_128_KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET128BIT, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_CBC_HMAC_SHA_256);
        protectedHeader.setAlgorithm(JwaAlgorithms.AES_128_KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET128BIT, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_CBC_HMAC_SHA_384);
        protectedHeader.setAlgorithm(JwaAlgorithms.AES_128_KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET128BIT, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_CBC_HMAC_SHA_512);
        protectedHeader.setAlgorithm(JwaAlgorithms.AES_128_KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET128BIT, protectedHeader);
        
        // AES_192_KW
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_GCM);
        protectedHeader.setAlgorithm(JwaAlgorithms.AES_192_KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET192BIT, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_GCM);
        protectedHeader.setAlgorithm(JwaAlgorithms.AES_192_KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET192BIT, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_GCM);
        protectedHeader.setAlgorithm(JwaAlgorithms.AES_192_KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET192BIT, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_CBC_HMAC_SHA_256);
        protectedHeader.setAlgorithm(JwaAlgorithms.AES_192_KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET192BIT, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_CBC_HMAC_SHA_384);
        protectedHeader.setAlgorithm(JwaAlgorithms.AES_192_KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET192BIT, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_CBC_HMAC_SHA_512);
        protectedHeader.setAlgorithm(JwaAlgorithms.AES_192_KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET192BIT, protectedHeader);
        
        // AES_256_KW
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_GCM);
        protectedHeader.setAlgorithm(JwaAlgorithms.AES_256_KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET256BIT, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_GCM);
        protectedHeader.setAlgorithm(JwaAlgorithms.AES_256_KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET256BIT, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_GCM);
        protectedHeader.setAlgorithm(JwaAlgorithms.AES_256_KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET256BIT, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_CBC_HMAC_SHA_256);
        protectedHeader.setAlgorithm(JwaAlgorithms.AES_256_KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET256BIT, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_CBC_HMAC_SHA_384);
        protectedHeader.setAlgorithm(JwaAlgorithms.AES_256_KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET256BIT, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_CBC_HMAC_SHA_512);
        protectedHeader.setAlgorithm(JwaAlgorithms.AES_256_KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET256BIT, protectedHeader);
        
        // DIRECT
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_GCM);
        protectedHeader.setAlgorithm(JwaAlgorithms.DIRECT);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET128BIT, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_GCM);
        protectedHeader.setAlgorithm(JwaAlgorithms.DIRECT);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET192BIT, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_GCM);
        protectedHeader.setAlgorithm(JwaAlgorithms.DIRECT);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET256BIT, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_CBC_HMAC_SHA_256);
        protectedHeader.setAlgorithm(JwaAlgorithms.DIRECT);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET256BIT, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_CBC_HMAC_SHA_384);
        protectedHeader.setAlgorithm(JwaAlgorithms.DIRECT);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET384BIT, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_CBC_HMAC_SHA_512);
        protectedHeader.setAlgorithm(JwaAlgorithms.DIRECT);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET512BIT, protectedHeader);
        
        // ECDH_ES
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_GCM);
        protectedHeader.setAlgorithm(JwaAlgorithms.ECDH_ES);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, ecdhPublicKey, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_GCM);
        protectedHeader.setAlgorithm(JwaAlgorithms.ECDH_ES);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, ecdhPublicKey, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_GCM);
        protectedHeader.setAlgorithm(JwaAlgorithms.ECDH_ES);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, ecdhPublicKey, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_CBC_HMAC_SHA_256);
        protectedHeader.setAlgorithm(JwaAlgorithms.ECDH_ES);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, ecdhPublicKey, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_CBC_HMAC_SHA_384);
        protectedHeader.setAlgorithm(JwaAlgorithms.ECDH_ES);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, ecdhPublicKey, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_CBC_HMAC_SHA_512);
        protectedHeader.setAlgorithm(JwaAlgorithms.ECDH_ES);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, ecdhPublicKey, protectedHeader);
        
        // ECDH_ES_AES128KW
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_GCM);
        protectedHeader.setAlgorithm(JwaAlgorithms.ECDH_ES_AES128KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, ecdhPublicKey, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_GCM);
        protectedHeader.setAlgorithm(JwaAlgorithms.ECDH_ES_AES128KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, ecdhPublicKey, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_GCM);
        protectedHeader.setAlgorithm(JwaAlgorithms.ECDH_ES_AES128KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, ecdhPublicKey, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_CBC_HMAC_SHA_256);
        protectedHeader.setAlgorithm(JwaAlgorithms.ECDH_ES_AES128KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, ecdhPublicKey, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_CBC_HMAC_SHA_384);
        protectedHeader.setAlgorithm(JwaAlgorithms.ECDH_ES_AES128KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, ecdhPublicKey, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_CBC_HMAC_SHA_512);
        protectedHeader.setAlgorithm(JwaAlgorithms.ECDH_ES_AES128KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, ecdhPublicKey, protectedHeader);
     
        // ECDH_ES_AES192KW
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_GCM);
        protectedHeader.setAlgorithm(JwaAlgorithms.ECDH_ES_AES192KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, ecdhPublicKey, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_GCM);
        protectedHeader.setAlgorithm(JwaAlgorithms.ECDH_ES_AES192KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, ecdhPublicKey, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_GCM);
        protectedHeader.setAlgorithm(JwaAlgorithms.ECDH_ES_AES192KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, ecdhPublicKey, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_CBC_HMAC_SHA_256);
        protectedHeader.setAlgorithm(JwaAlgorithms.ECDH_ES_AES192KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, ecdhPublicKey, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_CBC_HMAC_SHA_384);
        protectedHeader.setAlgorithm(JwaAlgorithms.ECDH_ES_AES192KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, ecdhPublicKey, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_CBC_HMAC_SHA_512);
        protectedHeader.setAlgorithm(JwaAlgorithms.ECDH_ES_AES192KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, ecdhPublicKey, protectedHeader);
        
        // ECDH_ES_AES256KW
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_GCM);
        protectedHeader.setAlgorithm(JwaAlgorithms.ECDH_ES_AES256KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, ecdhPublicKey, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_GCM);
        protectedHeader.setAlgorithm(JwaAlgorithms.ECDH_ES_AES256KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, ecdhPublicKey, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_GCM);
        protectedHeader.setAlgorithm(JwaAlgorithms.ECDH_ES_AES256KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, ecdhPublicKey, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_CBC_HMAC_SHA_256);
        protectedHeader.setAlgorithm(JwaAlgorithms.ECDH_ES_AES256KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, ecdhPublicKey, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_CBC_HMAC_SHA_384);
        protectedHeader.setAlgorithm(JwaAlgorithms.ECDH_ES_AES256KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, ecdhPublicKey, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_CBC_HMAC_SHA_512);
        protectedHeader.setAlgorithm(JwaAlgorithms.ECDH_ES_AES256KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, ecdhPublicKey, protectedHeader);
        
        // PBES2_HS256_A128KW
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_GCM);
        protectedHeader.setAlgorithm(JwaAlgorithms.PBES2_HS256_A128KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, pbesSecret, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_GCM);
        protectedHeader.setAlgorithm(JwaAlgorithms.PBES2_HS256_A128KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, pbesSecret, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_GCM);
        protectedHeader.setAlgorithm(JwaAlgorithms.PBES2_HS256_A128KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, pbesSecret, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_CBC_HMAC_SHA_256);
        protectedHeader.setAlgorithm(JwaAlgorithms.PBES2_HS256_A128KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, pbesSecret, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_CBC_HMAC_SHA_384);
        protectedHeader.setAlgorithm(JwaAlgorithms.PBES2_HS256_A128KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, pbesSecret, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_CBC_HMAC_SHA_512);
        protectedHeader.setAlgorithm(JwaAlgorithms.PBES2_HS256_A128KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, pbesSecret, protectedHeader);
        
        // PBES2_HS384_A192KW
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_GCM);
        protectedHeader.setAlgorithm(JwaAlgorithms.PBES2_HS384_A192KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, pbesSecret, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_GCM);
        protectedHeader.setAlgorithm(JwaAlgorithms.PBES2_HS384_A192KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, pbesSecret, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_GCM);
        protectedHeader.setAlgorithm(JwaAlgorithms.PBES2_HS384_A192KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, pbesSecret, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_CBC_HMAC_SHA_256);
        protectedHeader.setAlgorithm(JwaAlgorithms.PBES2_HS384_A192KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, pbesSecret, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_CBC_HMAC_SHA_384);
        protectedHeader.setAlgorithm(JwaAlgorithms.PBES2_HS384_A192KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, pbesSecret, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_CBC_HMAC_SHA_512);
        protectedHeader.setAlgorithm(JwaAlgorithms.PBES2_HS384_A192KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, pbesSecret, protectedHeader);
        
        // PBES2_HS512_A256KW
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_GCM);
        protectedHeader.setAlgorithm(JwaAlgorithms.PBES2_HS512_A256KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, pbesSecret, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_GCM);
        protectedHeader.setAlgorithm(JwaAlgorithms.PBES2_HS512_A256KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, pbesSecret, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_GCM);
        protectedHeader.setAlgorithm(JwaAlgorithms.PBES2_HS512_A256KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, pbesSecret, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_CBC_HMAC_SHA_256);
        protectedHeader.setAlgorithm(JwaAlgorithms.PBES2_HS512_A256KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, pbesSecret, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_CBC_HMAC_SHA_384);
        protectedHeader.setAlgorithm(JwaAlgorithms.PBES2_HS512_A256KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, pbesSecret, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_CBC_HMAC_SHA_512);
        protectedHeader.setAlgorithm(JwaAlgorithms.PBES2_HS512_A256KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, pbesSecret, protectedHeader);
        
        // RSA1_5
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_GCM);
        protectedHeader.setAlgorithm(JwaAlgorithms.RSA1_5);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, rsaPublicKey, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_GCM);
        protectedHeader.setAlgorithm(JwaAlgorithms.RSA1_5);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, rsaPublicKey, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_GCM);
        protectedHeader.setAlgorithm(JwaAlgorithms.RSA1_5);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, rsaPublicKey, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_CBC_HMAC_SHA_256);
        protectedHeader.setAlgorithm(JwaAlgorithms.RSA1_5);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, rsaPublicKey, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_CBC_HMAC_SHA_384);
        protectedHeader.setAlgorithm(JwaAlgorithms.RSA1_5);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, rsaPublicKey, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_CBC_HMAC_SHA_512);
        protectedHeader.setAlgorithm(JwaAlgorithms.RSA1_5);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, rsaPublicKey, protectedHeader);
        
        // RSA_OAEP
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_GCM);
        protectedHeader.setAlgorithm(JwaAlgorithms.RSA_OAEP);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, rsaPublicKey, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_GCM);
        protectedHeader.setAlgorithm(JwaAlgorithms.RSA_OAEP);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, rsaPublicKey, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_GCM);
        protectedHeader.setAlgorithm(JwaAlgorithms.RSA_OAEP);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, rsaPublicKey, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_CBC_HMAC_SHA_256);
        protectedHeader.setAlgorithm(JwaAlgorithms.RSA_OAEP);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, rsaPublicKey, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_CBC_HMAC_SHA_384);
        protectedHeader.setAlgorithm(JwaAlgorithms.RSA_OAEP);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, rsaPublicKey, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_CBC_HMAC_SHA_512);
        protectedHeader.setAlgorithm(JwaAlgorithms.RSA_OAEP);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, rsaPublicKey, protectedHeader);
        
        // RSA_OAEP_256
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_GCM);
        protectedHeader.setAlgorithm(JwaAlgorithms.RSA_OAEP_256);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, rsaPublicKey, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_GCM);
        protectedHeader.setAlgorithm(JwaAlgorithms.RSA_OAEP_256);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, rsaPublicKey, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_GCM);
        protectedHeader.setAlgorithm(JwaAlgorithms.RSA_OAEP_256);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, rsaPublicKey, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_CBC_HMAC_SHA_256);
        protectedHeader.setAlgorithm(JwaAlgorithms.RSA_OAEP_256);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, rsaPublicKey, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_CBC_HMAC_SHA_384);
        protectedHeader.setAlgorithm(JwaAlgorithms.RSA_OAEP_256);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, rsaPublicKey, protectedHeader);
        
        protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_CBC_HMAC_SHA_512);
        protectedHeader.setAlgorithm(JwaAlgorithms.RSA_OAEP_256);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, rsaPublicKey, protectedHeader);
    }
    
    @Test
    public void encAlgCombinationUnprotectedHeaderTest()
    {    
        final JwkKey SECRET128BIT = new JwkKeySet(readKeySet("constructiveTestsSecretSet.txt")).getKeys().get(0);
        final JwkKey SECRET192BIT = new JwkKeySet(readKeySet("constructiveTestsSecretSet.txt")).getKeys().get(1);
        final JwkKey SECRET256BIT = new JwkKeySet(readKeySet("constructiveTestsSecretSet.txt")).getKeys().get(2);
        final JwkKey SECRET384BIT = new JwkKeySet(readKeySet("constructiveTestsSecretSet.txt")).getKeys().get(3);
        final JwkKey SECRET512BIT = new JwkKeySet(readKeySet("constructiveTestsSecretSet.txt")).getKeys().get(4);
        
        String jwk = "{\"k\":\"" + JoseUtils.bytesToBase64URLString("One today is worth two tomorrows".getBytes()) + "\"" + "}";
        JwkKey pbesSecret = new JwkKey(jwk);
       
        JwkKeySet jwks = new JwkKeySet(readKeySet("cookbookPublicSet.txt"));
        List<JwkKey> keys = jwks.getKeys();
        JwkKey ecdhPublicKey = keys.get(4);
        JwkKey rsaPublicKey = keys.get(2);    
        
        final String AAD = "aad";
        
        JweSharedUnprotectedHeader unprotectedHeader = null;
        @SuppressWarnings("unused")
        JweDocument jwe = null;
        
        // AES_128_GCMKW
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_GCM);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.AES_128_GCMKW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET128BIT, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_GCM);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.AES_128_GCMKW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET128BIT, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_GCM);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.AES_128_GCMKW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET128BIT, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_CBC_HMAC_SHA_256);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.AES_128_GCMKW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET128BIT, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_CBC_HMAC_SHA_384);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.AES_128_GCMKW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET128BIT, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_CBC_HMAC_SHA_512);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.AES_128_GCMKW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET128BIT, unprotectedHeader);
        
        // AES_192_GCMKW
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_GCM);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.AES_192_GCMKW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET192BIT, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_GCM);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.AES_192_GCMKW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET192BIT, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_GCM);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.AES_192_GCMKW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET192BIT, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_CBC_HMAC_SHA_256);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.AES_192_GCMKW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET192BIT, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_CBC_HMAC_SHA_384);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.AES_192_GCMKW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET192BIT, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_CBC_HMAC_SHA_512);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.AES_192_GCMKW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET192BIT, unprotectedHeader);
        
        // AES_256_GCMKW
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_GCM);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.AES_256_GCMKW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET256BIT, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_GCM);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.AES_256_GCMKW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET256BIT, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_GCM);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.AES_256_GCMKW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET256BIT, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_CBC_HMAC_SHA_256);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.AES_256_GCMKW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET256BIT, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_CBC_HMAC_SHA_384);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.AES_256_GCMKW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET256BIT, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_CBC_HMAC_SHA_512);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.AES_256_GCMKW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET256BIT, unprotectedHeader);
        
        // AES_128_KW
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_GCM);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.AES_128_KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET128BIT, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_GCM);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.AES_128_KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET128BIT, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_GCM);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.AES_128_KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET128BIT, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_CBC_HMAC_SHA_256);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.AES_128_KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET128BIT, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_CBC_HMAC_SHA_384);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.AES_128_KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET128BIT, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_CBC_HMAC_SHA_512);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.AES_128_KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET128BIT, unprotectedHeader);
        
        // AES_192_KW
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_GCM);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.AES_192_KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET192BIT, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_GCM);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.AES_192_KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET192BIT, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_GCM);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.AES_192_KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET192BIT, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_CBC_HMAC_SHA_256);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.AES_192_KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET192BIT, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_CBC_HMAC_SHA_384);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.AES_192_KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET192BIT, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_CBC_HMAC_SHA_512);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.AES_192_KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET192BIT, unprotectedHeader);
        
        // AES_256_KW
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_GCM);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.AES_256_KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET256BIT, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_GCM);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.AES_256_KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET256BIT, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_GCM);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.AES_256_KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET256BIT, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_CBC_HMAC_SHA_256);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.AES_256_KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET256BIT, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_CBC_HMAC_SHA_384);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.AES_256_KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET256BIT, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_CBC_HMAC_SHA_512);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.AES_256_KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET256BIT, unprotectedHeader);
        
        // DIRECT
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_GCM);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.DIRECT);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET128BIT, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_GCM);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.DIRECT);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET192BIT, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_GCM);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.DIRECT);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET256BIT, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_CBC_HMAC_SHA_256);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.DIRECT);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET256BIT, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_CBC_HMAC_SHA_384);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.DIRECT);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET384BIT, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_CBC_HMAC_SHA_512);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.DIRECT);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, SECRET512BIT, unprotectedHeader);
        
        // ECDH_ES
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_GCM);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.ECDH_ES);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, ecdhPublicKey, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_GCM);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.ECDH_ES);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, ecdhPublicKey, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_GCM);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.ECDH_ES);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, ecdhPublicKey, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_CBC_HMAC_SHA_256);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.ECDH_ES);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, ecdhPublicKey, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_CBC_HMAC_SHA_384);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.ECDH_ES);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, ecdhPublicKey, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_CBC_HMAC_SHA_512);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.ECDH_ES);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, ecdhPublicKey, unprotectedHeader);
        
        // ECDH_ES_AES128KW
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_GCM);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.ECDH_ES_AES128KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, ecdhPublicKey, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_GCM);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.ECDH_ES_AES128KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, ecdhPublicKey, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_GCM);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.ECDH_ES_AES128KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, ecdhPublicKey, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_CBC_HMAC_SHA_256);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.ECDH_ES_AES128KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, ecdhPublicKey, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_CBC_HMAC_SHA_384);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.ECDH_ES_AES128KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, ecdhPublicKey, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_CBC_HMAC_SHA_512);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.ECDH_ES_AES128KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, ecdhPublicKey, unprotectedHeader);
     
        // ECDH_ES_AES192KW
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_GCM);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.ECDH_ES_AES192KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, ecdhPublicKey, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_GCM);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.ECDH_ES_AES192KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, ecdhPublicKey, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_GCM);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.ECDH_ES_AES192KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, ecdhPublicKey, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_CBC_HMAC_SHA_256);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.ECDH_ES_AES192KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, ecdhPublicKey, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_CBC_HMAC_SHA_384);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.ECDH_ES_AES192KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, ecdhPublicKey, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_CBC_HMAC_SHA_512);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.ECDH_ES_AES192KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, ecdhPublicKey, unprotectedHeader);
        
        // ECDH_ES_AES256KW
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_GCM);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.ECDH_ES_AES256KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, ecdhPublicKey, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_GCM);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.ECDH_ES_AES256KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, ecdhPublicKey, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_GCM);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.ECDH_ES_AES256KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, ecdhPublicKey, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_CBC_HMAC_SHA_256);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.ECDH_ES_AES256KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, ecdhPublicKey, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_CBC_HMAC_SHA_384);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.ECDH_ES_AES256KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, ecdhPublicKey, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_CBC_HMAC_SHA_512);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.ECDH_ES_AES256KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, ecdhPublicKey, unprotectedHeader);
        
        // PBES2_HS256_A128KW
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_GCM);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.PBES2_HS256_A128KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, pbesSecret, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_GCM);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.PBES2_HS256_A128KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, pbesSecret, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_GCM);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.PBES2_HS256_A128KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, pbesSecret, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_CBC_HMAC_SHA_256);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.PBES2_HS256_A128KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, pbesSecret, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_CBC_HMAC_SHA_384);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.PBES2_HS256_A128KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, pbesSecret, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_CBC_HMAC_SHA_512);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.PBES2_HS256_A128KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, pbesSecret, unprotectedHeader);
        
        // PBES2_HS384_A192KW
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_GCM);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.PBES2_HS384_A192KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, pbesSecret, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_GCM);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.PBES2_HS384_A192KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, pbesSecret, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_GCM);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.PBES2_HS384_A192KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, pbesSecret, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_CBC_HMAC_SHA_256);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.PBES2_HS384_A192KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, pbesSecret, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_CBC_HMAC_SHA_384);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.PBES2_HS384_A192KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, pbesSecret, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_CBC_HMAC_SHA_512);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.PBES2_HS384_A192KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, pbesSecret, unprotectedHeader);
        
        // PBES2_HS512_A256KW
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_GCM);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.PBES2_HS512_A256KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, pbesSecret, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_GCM);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.PBES2_HS512_A256KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, pbesSecret, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_GCM);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.PBES2_HS512_A256KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, pbesSecret, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_CBC_HMAC_SHA_256);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.PBES2_HS512_A256KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, pbesSecret, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_CBC_HMAC_SHA_384);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.PBES2_HS512_A256KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, pbesSecret, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_CBC_HMAC_SHA_512);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.PBES2_HS512_A256KW);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, pbesSecret, unprotectedHeader);
        
        // RSA1_5
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_GCM);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.RSA1_5);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, rsaPublicKey, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_GCM);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.RSA1_5);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, rsaPublicKey, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_GCM);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.RSA1_5);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, rsaPublicKey, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_CBC_HMAC_SHA_256);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.RSA1_5);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, rsaPublicKey, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_CBC_HMAC_SHA_384);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.RSA1_5);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, rsaPublicKey, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_CBC_HMAC_SHA_512);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.RSA1_5);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, rsaPublicKey, unprotectedHeader);
        
        // RSA_OAEP
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_GCM);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.RSA_OAEP);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, rsaPublicKey, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_GCM);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.RSA_OAEP);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, rsaPublicKey, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_GCM);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.RSA_OAEP);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, rsaPublicKey, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_CBC_HMAC_SHA_256);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.RSA_OAEP);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, rsaPublicKey, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_CBC_HMAC_SHA_384);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.RSA_OAEP);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, rsaPublicKey, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_CBC_HMAC_SHA_512);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.RSA_OAEP);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, rsaPublicKey, unprotectedHeader);
        
        // RSA_OAEP_256
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_GCM);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.RSA_OAEP_256);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, rsaPublicKey, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_GCM);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.RSA_OAEP_256);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, rsaPublicKey, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_GCM);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.RSA_OAEP_256);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, rsaPublicKey, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_CBC_HMAC_SHA_256);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.RSA_OAEP_256);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, rsaPublicKey, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_CBC_HMAC_SHA_384);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.RSA_OAEP_256);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, rsaPublicKey, unprotectedHeader);
        
        unprotectedHeader = new JweSharedUnprotectedHeader();
        unprotectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_CBC_HMAC_SHA_512);
        unprotectedHeader.setAlgorithm(JwaAlgorithms.RSA_OAEP_256);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, rsaPublicKey, unprotectedHeader);
    }
    
    @Test
    public void encAlgCombinationPerRecipientUnprotectedHeaderTest()
    {    
        final JwkKey SECRET128BIT = new JwkKeySet(readKeySet("constructiveTestsSecretSet.txt")).getKeys().get(0);
        final JwkKey SECRET192BIT = new JwkKeySet(readKeySet("constructiveTestsSecretSet.txt")).getKeys().get(1);
        final JwkKey SECRET256BIT = new JwkKeySet(readKeySet("constructiveTestsSecretSet.txt")).getKeys().get(2);
        final JwkKey SECRET384BIT = new JwkKeySet(readKeySet("constructiveTestsSecretSet.txt")).getKeys().get(3);
        final JwkKey SECRET512BIT = new JwkKeySet(readKeySet("constructiveTestsSecretSet.txt")).getKeys().get(4);
        
        String jwk = "{\"k\":\"" + JoseUtils.bytesToBase64URLString("One today is worth two tomorrows".getBytes()) + "\"" + "}";
        JwkKey pbesSecret = new JwkKey(jwk);
       
        JwkKeySet jwks = new JwkKeySet(readKeySet("cookbookPublicSet.txt"));
        List<JwkKey> keys = jwks.getKeys();
        JwkKey ecdhPublicKey = keys.get(4);
        JwkKey rsaPublicKey = keys.get(2);    
        
        final String AAD = "aad";
        
        JwePerRecipientUnprotectedHeader recipientHeader = null;
        JweRecipient recipient = null;
        @SuppressWarnings("unused")
        JweDocument jwe = null;
        
        // AES_128_GCMKW
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_GCM);
        recipientHeader.setAlgorithm(JwaAlgorithms.AES_128_GCMKW);
        recipient = new JweRecipient(recipientHeader, SECRET128BIT);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_GCM);
        recipientHeader.setAlgorithm(JwaAlgorithms.AES_128_GCMKW);
        recipient = new JweRecipient(recipientHeader, SECRET128BIT);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_GCM);
        recipientHeader.setAlgorithm(JwaAlgorithms.AES_128_GCMKW);
        recipient = new JweRecipient(recipientHeader, SECRET128BIT);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_CBC_HMAC_SHA_256);
        recipientHeader.setAlgorithm(JwaAlgorithms.AES_128_GCMKW);
        recipient = new JweRecipient(recipientHeader, SECRET128BIT);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_CBC_HMAC_SHA_384);
        recipientHeader.setAlgorithm(JwaAlgorithms.AES_128_GCMKW);
        recipient = new JweRecipient(recipientHeader, SECRET128BIT);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_CBC_HMAC_SHA_512);
        recipientHeader.setAlgorithm(JwaAlgorithms.AES_128_GCMKW);
        recipient = new JweRecipient(recipientHeader, SECRET128BIT);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        // AES_192_GCMKW
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_GCM);
        recipientHeader.setAlgorithm(JwaAlgorithms.AES_192_GCMKW);
        recipient = new JweRecipient(recipientHeader, SECRET192BIT);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_GCM);
        recipientHeader.setAlgorithm(JwaAlgorithms.AES_192_GCMKW);
        recipient = new JweRecipient(recipientHeader, SECRET192BIT);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_GCM);
        recipientHeader.setAlgorithm(JwaAlgorithms.AES_192_GCMKW);
        recipient = new JweRecipient(recipientHeader, SECRET192BIT);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_CBC_HMAC_SHA_256);
        recipientHeader.setAlgorithm(JwaAlgorithms.AES_192_GCMKW);
        recipient = new JweRecipient(recipientHeader, SECRET192BIT);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_CBC_HMAC_SHA_384);
        recipientHeader.setAlgorithm(JwaAlgorithms.AES_192_GCMKW);
        recipient = new JweRecipient(recipientHeader, SECRET192BIT);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_CBC_HMAC_SHA_512);
        recipientHeader.setAlgorithm(JwaAlgorithms.AES_192_GCMKW);
        recipient = new JweRecipient(recipientHeader, SECRET192BIT);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        // AES_256_GCMKW
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_GCM);
        recipientHeader.setAlgorithm(JwaAlgorithms.AES_256_GCMKW);
        recipient = new JweRecipient(recipientHeader, SECRET256BIT);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_GCM);
        recipientHeader.setAlgorithm(JwaAlgorithms.AES_256_GCMKW);
        recipient = new JweRecipient(recipientHeader, SECRET256BIT);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_GCM);
        recipientHeader.setAlgorithm(JwaAlgorithms.AES_256_GCMKW);
        recipient = new JweRecipient(recipientHeader, SECRET256BIT);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_CBC_HMAC_SHA_256);
        recipientHeader.setAlgorithm(JwaAlgorithms.AES_256_GCMKW);
        recipient = new JweRecipient(recipientHeader, SECRET256BIT);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_CBC_HMAC_SHA_384);
        recipientHeader.setAlgorithm(JwaAlgorithms.AES_256_GCMKW);
        recipient = new JweRecipient(recipientHeader, SECRET256BIT);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_CBC_HMAC_SHA_512);
        recipientHeader.setAlgorithm(JwaAlgorithms.AES_256_GCMKW);
        recipient = new JweRecipient(recipientHeader, SECRET256BIT);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        // AES_128_KW
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_GCM);
        recipientHeader.setAlgorithm(JwaAlgorithms.AES_128_KW);
        recipient = new JweRecipient(recipientHeader, SECRET128BIT);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_GCM);
        recipientHeader.setAlgorithm(JwaAlgorithms.AES_128_KW);
        recipient = new JweRecipient(recipientHeader, SECRET128BIT);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_GCM);
        recipientHeader.setAlgorithm(JwaAlgorithms.AES_128_KW);
        recipient = new JweRecipient(recipientHeader, SECRET128BIT);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_CBC_HMAC_SHA_256);
        recipientHeader.setAlgorithm(JwaAlgorithms.AES_128_KW);
        recipient = new JweRecipient(recipientHeader, SECRET128BIT);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_CBC_HMAC_SHA_384);
        recipientHeader.setAlgorithm(JwaAlgorithms.AES_128_KW);
        recipient = new JweRecipient(recipientHeader, SECRET128BIT);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_CBC_HMAC_SHA_512);
        recipientHeader.setAlgorithm(JwaAlgorithms.AES_128_KW);
        recipient = new JweRecipient(recipientHeader, SECRET128BIT);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        // AES_192_KW
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_GCM);
        recipientHeader.setAlgorithm(JwaAlgorithms.AES_192_KW);
        recipient = new JweRecipient(recipientHeader, SECRET192BIT);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_GCM);
        recipientHeader.setAlgorithm(JwaAlgorithms.AES_192_KW);
        recipient = new JweRecipient(recipientHeader, SECRET192BIT);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_GCM);
        recipientHeader.setAlgorithm(JwaAlgorithms.AES_192_KW);
        recipient = new JweRecipient(recipientHeader, SECRET192BIT);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_CBC_HMAC_SHA_256);
        recipientHeader.setAlgorithm(JwaAlgorithms.AES_192_KW);
        recipient = new JweRecipient(recipientHeader, SECRET192BIT);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_CBC_HMAC_SHA_384);
        recipientHeader.setAlgorithm(JwaAlgorithms.AES_192_KW);
        recipient = new JweRecipient(recipientHeader, SECRET192BIT);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_CBC_HMAC_SHA_512);
        recipientHeader.setAlgorithm(JwaAlgorithms.AES_192_KW);
        recipient = new JweRecipient(recipientHeader, SECRET192BIT);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        // AES_256_KW
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_GCM);
        recipientHeader.setAlgorithm(JwaAlgorithms.AES_256_KW);
        recipient = new JweRecipient(recipientHeader, SECRET256BIT);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_GCM);
        recipientHeader.setAlgorithm(JwaAlgorithms.AES_256_KW);
        recipient = new JweRecipient(recipientHeader, SECRET256BIT);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_GCM);
        recipientHeader.setAlgorithm(JwaAlgorithms.AES_256_KW);
        recipient = new JweRecipient(recipientHeader, SECRET256BIT);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_CBC_HMAC_SHA_256);
        recipientHeader.setAlgorithm(JwaAlgorithms.AES_256_KW);
        recipient = new JweRecipient(recipientHeader, SECRET256BIT);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_CBC_HMAC_SHA_384);
        recipientHeader.setAlgorithm(JwaAlgorithms.AES_256_KW);
        recipient = new JweRecipient(recipientHeader, SECRET256BIT);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_CBC_HMAC_SHA_512);
        recipientHeader.setAlgorithm(JwaAlgorithms.AES_256_KW);
        recipient = new JweRecipient(recipientHeader, SECRET256BIT);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        // DIRECT
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_GCM);
        recipientHeader.setAlgorithm(JwaAlgorithms.DIRECT);
        recipient = new JweRecipient(recipientHeader, SECRET128BIT);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_GCM);
        recipientHeader.setAlgorithm(JwaAlgorithms.DIRECT);
        recipient = new JweRecipient(recipientHeader, SECRET192BIT);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_GCM);
        recipientHeader.setAlgorithm(JwaAlgorithms.DIRECT);
        recipient = new JweRecipient(recipientHeader, SECRET256BIT);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_CBC_HMAC_SHA_256);
        recipientHeader.setAlgorithm(JwaAlgorithms.DIRECT);
        recipient = new JweRecipient(recipientHeader, SECRET256BIT);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_CBC_HMAC_SHA_384);
        recipientHeader.setAlgorithm(JwaAlgorithms.DIRECT);
        recipient = new JweRecipient(recipientHeader, SECRET384BIT);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_CBC_HMAC_SHA_512);
        recipientHeader.setAlgorithm(JwaAlgorithms.DIRECT);
        recipient = new JweRecipient(recipientHeader, SECRET512BIT);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        // ECDH_ES
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_GCM);
        recipientHeader.setAlgorithm(JwaAlgorithms.ECDH_ES);
        recipient = new JweRecipient(recipientHeader, ecdhPublicKey);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_GCM);
        recipientHeader.setAlgorithm(JwaAlgorithms.ECDH_ES);
        recipient = new JweRecipient(recipientHeader, ecdhPublicKey);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_GCM);
        recipientHeader.setAlgorithm(JwaAlgorithms.ECDH_ES);
        recipient = new JweRecipient(recipientHeader, ecdhPublicKey);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_CBC_HMAC_SHA_256);
        recipientHeader.setAlgorithm(JwaAlgorithms.ECDH_ES);
        recipient = new JweRecipient(recipientHeader, ecdhPublicKey);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_CBC_HMAC_SHA_384);
        recipientHeader.setAlgorithm(JwaAlgorithms.ECDH_ES);
        recipient = new JweRecipient(recipientHeader, ecdhPublicKey);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_CBC_HMAC_SHA_512);
        recipientHeader.setAlgorithm(JwaAlgorithms.ECDH_ES);
        recipient = new JweRecipient(recipientHeader, ecdhPublicKey);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        // ECDH_ES_AES128KW
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_GCM);
        recipientHeader.setAlgorithm(JwaAlgorithms.ECDH_ES_AES128KW);
        recipient = new JweRecipient(recipientHeader, ecdhPublicKey);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_GCM);
        recipientHeader.setAlgorithm(JwaAlgorithms.ECDH_ES_AES128KW);
        recipient = new JweRecipient(recipientHeader, ecdhPublicKey);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_GCM);
        recipientHeader.setAlgorithm(JwaAlgorithms.ECDH_ES_AES128KW);
        recipient = new JweRecipient(recipientHeader, ecdhPublicKey);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_CBC_HMAC_SHA_256);
        recipientHeader.setAlgorithm(JwaAlgorithms.ECDH_ES_AES128KW);
        recipient = new JweRecipient(recipientHeader, ecdhPublicKey);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_CBC_HMAC_SHA_384);
        recipientHeader.setAlgorithm(JwaAlgorithms.ECDH_ES_AES128KW);
        recipient = new JweRecipient(recipientHeader, ecdhPublicKey);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_CBC_HMAC_SHA_512);
        recipientHeader.setAlgorithm(JwaAlgorithms.ECDH_ES_AES128KW);
        recipient = new JweRecipient(recipientHeader, ecdhPublicKey);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
     
        // ECDH_ES_AES192KW
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_GCM);
        recipientHeader.setAlgorithm(JwaAlgorithms.ECDH_ES_AES192KW);
        recipient = new JweRecipient(recipientHeader, ecdhPublicKey);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_GCM);
        recipientHeader.setAlgorithm(JwaAlgorithms.ECDH_ES_AES192KW);
        recipient = new JweRecipient(recipientHeader, ecdhPublicKey);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_GCM);
        recipientHeader.setAlgorithm(JwaAlgorithms.ECDH_ES_AES192KW);
        recipient = new JweRecipient(recipientHeader, ecdhPublicKey);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_CBC_HMAC_SHA_256);
        recipientHeader.setAlgorithm(JwaAlgorithms.ECDH_ES_AES192KW);
        recipient = new JweRecipient(recipientHeader, ecdhPublicKey);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_CBC_HMAC_SHA_384);
        recipientHeader.setAlgorithm(JwaAlgorithms.ECDH_ES_AES192KW);
        recipient = new JweRecipient(recipientHeader, ecdhPublicKey);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_CBC_HMAC_SHA_512);
        recipientHeader.setAlgorithm(JwaAlgorithms.ECDH_ES_AES192KW);
        recipient = new JweRecipient(recipientHeader, ecdhPublicKey);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        // ECDH_ES_AES256KW
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_GCM);
        recipientHeader.setAlgorithm(JwaAlgorithms.ECDH_ES_AES256KW);
        recipient = new JweRecipient(recipientHeader, ecdhPublicKey);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_GCM);
        recipientHeader.setAlgorithm(JwaAlgorithms.ECDH_ES_AES256KW);
        recipient = new JweRecipient(recipientHeader, ecdhPublicKey);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_GCM);
        recipientHeader.setAlgorithm(JwaAlgorithms.ECDH_ES_AES256KW);
        recipient = new JweRecipient(recipientHeader, ecdhPublicKey);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_CBC_HMAC_SHA_256);
        recipientHeader.setAlgorithm(JwaAlgorithms.ECDH_ES_AES256KW);
        recipient = new JweRecipient(recipientHeader, ecdhPublicKey);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_CBC_HMAC_SHA_384);
        recipientHeader.setAlgorithm(JwaAlgorithms.ECDH_ES_AES256KW);
        recipient = new JweRecipient(recipientHeader, ecdhPublicKey);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_CBC_HMAC_SHA_512);
        recipientHeader.setAlgorithm(JwaAlgorithms.ECDH_ES_AES256KW);
        recipient = new JweRecipient(recipientHeader, ecdhPublicKey);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        // PBES2_HS256_A128KW
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_GCM);
        recipientHeader.setAlgorithm(JwaAlgorithms.PBES2_HS256_A128KW);
        recipient = new JweRecipient(recipientHeader, pbesSecret);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_GCM);
        recipientHeader.setAlgorithm(JwaAlgorithms.PBES2_HS256_A128KW);
        recipient = new JweRecipient(recipientHeader, pbesSecret);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_GCM);
        recipientHeader.setAlgorithm(JwaAlgorithms.PBES2_HS256_A128KW);
        recipient = new JweRecipient(recipientHeader, pbesSecret);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_CBC_HMAC_SHA_256);
        recipientHeader.setAlgorithm(JwaAlgorithms.PBES2_HS256_A128KW);
        recipient = new JweRecipient(recipientHeader, pbesSecret);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_CBC_HMAC_SHA_384);
        recipientHeader.setAlgorithm(JwaAlgorithms.PBES2_HS256_A128KW);
        recipient = new JweRecipient(recipientHeader, pbesSecret);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_CBC_HMAC_SHA_512);
        recipientHeader.setAlgorithm(JwaAlgorithms.PBES2_HS256_A128KW);
        recipient = new JweRecipient(recipientHeader, pbesSecret);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        // PBES2_HS384_A192KW
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_GCM);
        recipientHeader.setAlgorithm(JwaAlgorithms.PBES2_HS384_A192KW);
        recipient = new JweRecipient(recipientHeader, pbesSecret);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_GCM);
        recipientHeader.setAlgorithm(JwaAlgorithms.PBES2_HS384_A192KW);
        recipient = new JweRecipient(recipientHeader, pbesSecret);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_GCM);
        recipientHeader.setAlgorithm(JwaAlgorithms.PBES2_HS384_A192KW);
        recipient = new JweRecipient(recipientHeader, pbesSecret);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_CBC_HMAC_SHA_256);
        recipientHeader.setAlgorithm(JwaAlgorithms.PBES2_HS384_A192KW);
        recipient = new JweRecipient(recipientHeader, pbesSecret);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_CBC_HMAC_SHA_384);
        recipientHeader.setAlgorithm(JwaAlgorithms.PBES2_HS384_A192KW);
        recipient = new JweRecipient(recipientHeader, pbesSecret);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_CBC_HMAC_SHA_512);
        recipientHeader.setAlgorithm(JwaAlgorithms.PBES2_HS384_A192KW);
        recipient = new JweRecipient(recipientHeader, pbesSecret);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        // PBES2_HS512_A256KW
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_GCM);
        recipientHeader.setAlgorithm(JwaAlgorithms.PBES2_HS512_A256KW);
        recipient = new JweRecipient(recipientHeader, pbesSecret);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_GCM);
        recipientHeader.setAlgorithm(JwaAlgorithms.PBES2_HS512_A256KW);
        recipient = new JweRecipient(recipientHeader, pbesSecret);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_GCM);
        recipientHeader.setAlgorithm(JwaAlgorithms.PBES2_HS512_A256KW);
        recipient = new JweRecipient(recipientHeader, pbesSecret);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_CBC_HMAC_SHA_256);
        recipientHeader.setAlgorithm(JwaAlgorithms.PBES2_HS512_A256KW);
        recipient = new JweRecipient(recipientHeader, pbesSecret);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_CBC_HMAC_SHA_384);
        recipientHeader.setAlgorithm(JwaAlgorithms.PBES2_HS512_A256KW);
        recipient = new JweRecipient(recipientHeader, pbesSecret);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_CBC_HMAC_SHA_512);
        recipientHeader.setAlgorithm(JwaAlgorithms.PBES2_HS512_A256KW);
        recipient = new JweRecipient(recipientHeader, pbesSecret);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        // RSA1_5
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_GCM);
        recipientHeader.setAlgorithm(JwaAlgorithms.RSA1_5);
        recipient = new JweRecipient(recipientHeader, rsaPublicKey);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_GCM);
        recipientHeader.setAlgorithm(JwaAlgorithms.RSA1_5);
        recipient = new JweRecipient(recipientHeader, rsaPublicKey);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_GCM);
        recipientHeader.setAlgorithm(JwaAlgorithms.RSA1_5);
        recipient = new JweRecipient(recipientHeader, rsaPublicKey);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_CBC_HMAC_SHA_256);
        recipientHeader.setAlgorithm(JwaAlgorithms.RSA1_5);
        recipient = new JweRecipient(recipientHeader, rsaPublicKey);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_CBC_HMAC_SHA_384);
        recipientHeader.setAlgorithm(JwaAlgorithms.RSA1_5);
        recipient = new JweRecipient(recipientHeader, rsaPublicKey);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_CBC_HMAC_SHA_512);
        recipientHeader.setAlgorithm(JwaAlgorithms.RSA1_5);
        recipient = new JweRecipient(recipientHeader, rsaPublicKey);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        // RSA_OAEP
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_GCM);
        recipientHeader.setAlgorithm(JwaAlgorithms.RSA_OAEP);
        recipient = new JweRecipient(recipientHeader, rsaPublicKey);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_GCM);
        recipientHeader.setAlgorithm(JwaAlgorithms.RSA_OAEP);
        recipient = new JweRecipient(recipientHeader, rsaPublicKey);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_GCM);
        recipientHeader.setAlgorithm(JwaAlgorithms.RSA_OAEP);
        recipient = new JweRecipient(recipientHeader, rsaPublicKey);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_CBC_HMAC_SHA_256);
        recipientHeader.setAlgorithm(JwaAlgorithms.RSA_OAEP);
        recipient = new JweRecipient(recipientHeader, rsaPublicKey);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_CBC_HMAC_SHA_384);
        recipientHeader.setAlgorithm(JwaAlgorithms.RSA_OAEP);
        recipient = new JweRecipient(recipientHeader, rsaPublicKey);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_CBC_HMAC_SHA_512);
        recipientHeader.setAlgorithm(JwaAlgorithms.RSA_OAEP);
        recipient = new JweRecipient(recipientHeader, rsaPublicKey);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        // RSA_OAEP_256
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_GCM);
        recipientHeader.setAlgorithm(JwaAlgorithms.RSA_OAEP_256);
        recipient = new JweRecipient(recipientHeader, rsaPublicKey);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_GCM);
        recipientHeader.setAlgorithm(JwaAlgorithms.RSA_OAEP_256);
        recipient = new JweRecipient(recipientHeader, rsaPublicKey);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_GCM);
        recipientHeader.setAlgorithm(JwaAlgorithms.RSA_OAEP_256);
        recipient = new JweRecipient(recipientHeader, rsaPublicKey);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_CBC_HMAC_SHA_256);
        recipientHeader.setAlgorithm(JwaAlgorithms.RSA_OAEP_256);
        recipient = new JweRecipient(recipientHeader, rsaPublicKey);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_192_CBC_HMAC_SHA_384);
        recipientHeader.setAlgorithm(JwaAlgorithms.RSA_OAEP_256);
        recipient = new JweRecipient(recipientHeader, rsaPublicKey);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
        
        recipientHeader = new JwePerRecipientUnprotectedHeader();
        recipientHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_CBC_HMAC_SHA_512);
        recipientHeader.setAlgorithm(JwaAlgorithms.RSA_OAEP_256);
        recipient = new JweRecipient(recipientHeader, rsaPublicKey);
        jwe = JweMaker.generateFromMessage(PAYLOAD, AAD, recipient);
    }
    
    private String readKeySet(String fileName)
    {
        InputStream is = this.getClass().getClassLoader().getResourceAsStream(fileName);
        StringBuilder sb = new StringBuilder(1024);

        try
        {
            for (int i = is.read(); i != -1; i = is.read())
            {
                sb.append((char) i);
            }
        } catch (IOException e)
        {
            e.printStackTrace();
        }
        
        try
        {
            is.close();
        } catch (IOException e)
        {
            e.printStackTrace();
        }
        return sb.toString();
    }
}
