/*
 * Copyright 2015 Peter Leo Gorski, Luigi Lo Iacono, Daniel Behnam Torkian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fhk.jwx.jws.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import javax.crypto.spec.SecretKeySpec;

import org.junit.Test;

import de.fhk.jwx.jwa.JwaAlgorithms;
import de.fhk.jwx.jws.JwsDocument;
import de.fhk.jwx.jws.JwsMaker;
import de.fhk.jwx.jws.JwsProtectedHeader;
import de.fhk.jwx.jws.JwsUnprotectedHeader;
import de.fhk.jwx.jws.crypto.HmacJwsSigner;

public class JwsDemonstrativeTests {
    private static final byte[] secretKeyinBytes = { 72, 97, 115, 104, 45, 98,
            97, 115, 101, 100, 32, 77, 101, 115, 115, 97, 103, 101, 32, 65,
            117, 116, 104, 101, 110, 116, 105, 99, 97, 116, 105, 111, 110, 32,
            67, 111, 100, 101, 115, 32, 40, 72, 77, 65, 67, 115, 41, 32, 101,
            110, 97, 98, 108, 101, 32, 111, 110, 101, 32, 116, 111, 32, 117,
            115, 101, 32, 97, 32, 115, 101, 99, 114, 101, 116, 32, 112, 108,
            117, 115, 32, 97, 32, 99, 114, 121, 112, 116, 111, 103, 114, 97,
            112, 104, 105, 99, 32, 104, 97, 115, 104, 32, 102, 117, 110, 99,
            116, 105, 111, 110, 32, 116, 111, 32, 103, 101, 110, 101, 114, 97,
            116, 101, 32, 97, 32, 77, 101, 115, 115, 97, 103, 101, 32, 65, 117,
            116, 104, 101, 110, 116, 105, 99, 97, 116, 105, 111, 110, 32, 67,
            111, 100, 101, 32, 40, 77, 65, 67, 41, 46, 32, 32, 84, 104, 105,
            115, 32, 99, 97, 110, 32, 98, 101, 32, 117, 115, 101, 100, 32, 116,
            111, 32, 100, 101, 109, 111, 110, 115, 116, 114, 97, 116, 101, 32,
            116, 104, 97, 116, 32, 119, 104, 111, 101, 118, 101, 114, 32, 103,
            101, 110, 101, 114, 97, 116, 101, 100, 32, 116, 104, 101, 32, 77,
            65, 67, 32, 119, 97, 115, 32, 105, 110, 32, 112, 111, 115, 115,
            101, 115, 115, 105, 111, 110, 32, 111, 102, 32, 116, 104, 101, 32,
            77, 65, 67, 32, 107, 101, 121, 46, 32, 84, 104, 101, 32, 97, 108,
            103, 111, 114, 105, 116, 104, 109, 32, 102, 111, 114, 32, 105, 109,
            112, 108, 101, 109, 101, 110, 116, 105, 110, 103, 32, 97, 110, 100,
            32, 118, 97, 108, 105, 100, 97, 116, 105, 110, 103, 32, 72, 77, 65,
            67, 115, 32, 105, 115, 32, 112, 114, 111, 118, 105, 100, 101, 100,
            32, 105, 110, 32, 82, 70, 67, 50, 49, 48, 52, 32, 91, 82, 70, 67,
            50, 49, 48, 52, 93, 46, 32, 65, 32, 107, 101, 121, 32, 111, 102,
            32, 116, 104, 101, 32, 115, 97, 109, 101, 32, 115, 105, 122, 101,
            32, 97, 115, 32, 116, 104, 101, 32, 104, 97, 115, 104, 32, 111,
            117, 116, 112, 117, 116, 32, 40, 102, 111, 114, 32, 105, 110, 115,
            116, 97, 110, 99, 101, 44, 32, 50, 53, 54, 32, 98, 105, 116, 115,
            32, 102, 111, 114, 32, 34, 72, 83, 50, 53, 54, 34, 41, 32, 111,
            114, 32, 108, 97, 114, 103, 101, 114, 32, 77, 85, 83, 84, 32, 98,
            101, 32, 117, 115, 101, 100, 32, 119, 105, 116, 104, 32, 116, 104,
            105, 115, 32, 97, 108, 103, 111, 114, 105, 116, 104, 109, 46, 32,
            104, 116, 116, 112, 58, 47, 47, 116, 111, 111, 108, 115, 46, 105,
            101, 116, 102, 46, 111, 114, 103, 47, 104, 116, 109, 108, 47, 100,
            114, 97, 102, 116, 45, 105, 101, 116, 102, 45, 106, 111, 115, 101,
            45, 106, 115, 111, 110, 45, 119, 101, 98, 45, 97, 108, 103, 111,
            114, 105, 116, 104, 109, 115, 45, 51, 48 };

    private static final String JWS_TEST_CASE_1 = "eyJhbGciOiJIUzUxMiJ9.eyJwYX"
            + "lsb2FkIiA6ICJ0aGUgY29udGVudCB0aGF0IHVzZWQgdG8gc2lnbi4ifQ.qXyLBC"
            + "8bcgwxd75lf9ex2FMK9uJGwuoArtkZ6aBpTHWZWDn8W2-wY3wy2Hkdoxp_Migto"
            + "yrpxfvkGDXW5yw8aQ";

    private static final String JWS_TEST_CASE_3 = "eyJhbGciOiJIUzUxMiJ9.dGhlIG"
            + "NvbnRlbnQgdGhhdCB1c2VkIHRvIHNpZ24u.C8agio4_3YEFarjFc_njftmHXpAX"
            + "1tcyZ6XTTxpMotnDf0TBp4cucZIyhUaWr_0esJNm5VZok0HSW0VI5exPUg";

    private static final String JWS_TEST_CASE_5 = "{\"payload\":\"eyJwYXlsb2Fk"
            + "IiA6ICJ0aGUgY29udGVudCB0aGF0IHVzZWQgdG8gc2lnbi4ifQ\",\"signatur"
            + "es\":[{\"protected\":\"eyJhbGciOiJIUzUxMiJ9\",\"signature\":\"q"
            + "XyLBC8bcgwxd75lf9ex2FMK9uJGwuoArtkZ6aBpTHWZWDn8W2-wY3wy2Hkdoxp_"
            + "MigtoyrpxfvkGDXW5yw8aQ\"}]}";

    private static final String JWS_TEST_CASE_7 = "{\"payload\":\"eyJwYXlsb2Fk"
            + "IiA6ICJ0aGUgY29udGVudCB0aGF0IHVzZWQgdG8gc2lnbi4ifQ\",\"signatur"
            + "es\":[{\"header\":{\"alg\":\"HS512\"},\"signature\":\"nb8Gp4DDk"
            + "7TN_WVktKJSj1zwTCq_yYRNfGYaKFtvOvLOpoEN_rE301W7AKZlFjMB1ro-V19L"
            + "pvmQ9mJenIT6xQ\"}]}";

    private static final String JWS_TEST_CASE_9 = "{\"payload\":\"eyJwYXlsb2Fk"
            + "IiA6ICJ0aGUgY29udGVudCB0aGF0IHVzZWQgdG8gc2lnbi4ifQ\",\"signatur"
            + "es\":[{\"protected\":\"eyJjcml0IjpbImV4cCJdLCJleHAiOjE3Mzk3NjAy"
            + "MTgxMzV9\",\"header\":{\"alg\":\"HS512\"},\"signature\":\"v8HN9"
            + "p5pfctjLE9XJnMo1EQ_ulzs2YpIYXP6khCGyb-qR12kBiRY5u6oHFs8FAsI8EkN"
            + "VURvJvvZl7bvOYZfHQ\"}]}";

    private static final String JWS_TEST_CASE_11 = "{\"payload\":\"eyJwYXlsb2F"
            + "kIiA6ICJ0aGUgY29udGVudCB0aGF0IHVzZWQgdG8gc2lnbi4ifQ\",\"signatu"
            + "res\":[{\"protected\":\"eyJjcml0IjpbImV4cCJdLCJleHAiOjE3Mzk3NjA"
            + "yMTgxMzUsImFsZyI6IkhTNTEyIn0\",\"signature\":\"7A61oCma-nvo5plm"
            + "Wx9zEqlamOQ14kue74ZVAW_SQRjXJyZ-MMsyhR1dn9VOqopaF1PAY-Uhgp2QzZ5"
            + "vXmlqmQ\"}]}";

    private static final String JWS_TEST_CASE_13 = "eyJjcml0IjpbImV4cCJdLCJleH"
            + "AiOjE3Mzk3NjAyMTgxMzUsImFsZyI6IkhTNTEyIn0.eyJwYXlsb2FkIiA6ICJ0a"
            + "GUgY29udGVudCB0aGF0IHVzZWQgdG8gc2lnbi4ifQ.7A61oCma-nvo5plmWx9zE"
            + "qlamOQ14kue74ZVAW_SQRjXJyZ-MMsyhR1dn9VOqopaF1PAY-Uhgp2QzZ5vXmlq"
            + "mQ";

    @Test
    public void JWSTestCase1And2() {
        JwsProtectedHeader protectedHeader;
        protectedHeader = new JwsProtectedHeader();

        protectedHeader.setAlgorithm(JwaAlgorithms.HMAC_SHA_512
                .getSpecName());

        SecretKeySpec secretKey = new SecretKeySpec(secretKeyinBytes,
                JwaAlgorithms.HMAC_SHA_512.getJavaName());
        String payload = "{\"payload\" : \"the content that used to sign.\"}";

        HmacJwsSigner signer = new HmacJwsSigner(secretKey);

        JwsDocument jws;
        try {
            jws = JwsMaker.generateFromPayload(payload, protectedHeader,
                    signer);

            assertEquals(jws.getCompactSerialisation(), JWS_TEST_CASE_1);

            assertTrue(jws.verifySignatureWith(secretKey));
        } catch (SecurityException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Test
    public void JWSTestCase3And4(){
        JwsProtectedHeader protectedHeader;
        protectedHeader = new JwsProtectedHeader();

      protectedHeader.setAlgorithm(JwaAlgorithms.HMAC_SHA_512.getSpecName());

      SecretKeySpec secretKey = new SecretKeySpec(secretKeyinBytes,
            JwaAlgorithms.HMAC_SHA_512.getJavaName());
      String payload = "the content that used to sign.";

      HmacJwsSigner signer = new HmacJwsSigner(secretKey);

      JwsDocument jws;
      try {
        jws = JwsMaker.generateFromPayload(payload,
                protectedHeader, signer);

      assertEquals(jws.getCompactSerialisation(), JWS_TEST_CASE_3);
      jws = JwsMaker.generateFromJws(jws.getCompactSerialisation());
      assertTrue(jws.verifySignatureWith(secretKey));
      } catch (SecurityException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }

    @Test
    public void JWSTestCase5And6(){
        JwsProtectedHeader protectedHeader;
        protectedHeader = new JwsProtectedHeader();

      protectedHeader.setAlgorithm(JwaAlgorithms.HMAC_SHA_512.getSpecName());

      JwsUnprotectedHeader unprotectedHeader = new JwsUnprotectedHeader();

      SecretKeySpec secretKey = new SecretKeySpec(secretKeyinBytes,
            JwaAlgorithms.HMAC_SHA_512.getJavaName());
      String payload = "{\"payload\" : \"the content that used to sign.\"}";

      HmacJwsSigner signer = new HmacJwsSigner(secretKey);
      JwsDocument jws;
      try {
        jws = JwsMaker.generateFromPayload(payload,
                protectedHeader, unprotectedHeader, signer);

      assertEquals(jws.getJsonSerialisation(), JWS_TEST_CASE_5);
      jws = JwsMaker.generateFromJws(jws.getJsonSerialisation());
      assertTrue(jws.verifySignatureWith(secretKey));
      } catch (SecurityException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }

    @Test
    public void JWSTestCase7And8(){
        JwsProtectedHeader protectedHeader;
        protectedHeader = new JwsProtectedHeader();


      JwsUnprotectedHeader unprotectedHeader = new JwsUnprotectedHeader();
      unprotectedHeader
            .setAlgorithm(JwaAlgorithms.HMAC_SHA_512.getSpecName());

      SecretKeySpec secretKey = new SecretKeySpec(secretKeyinBytes,
            JwaAlgorithms.HMAC_SHA_512.getJavaName());
      String payload = "{\"payload\" : \"the content that used to sign.\"}";

      HmacJwsSigner signer = new HmacJwsSigner(secretKey);
      JwsDocument jws;
      try {
        jws = JwsMaker.generateFromPayload(payload,
                protectedHeader, unprotectedHeader, signer);

      assertEquals(jws.getJsonSerialisation(), JWS_TEST_CASE_7);

      jws = JwsMaker.generateFromJws(jws.getJsonSerialisation());
      assertTrue(jws.verifySignatureWith(secretKey));
      } catch (SecurityException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }

    @Test
    public void JWSTestCase9And10(){
        List<String> crit = new ArrayList<String>();
        crit.add("exp");

        JwsProtectedHeader protectedHeader;
        protectedHeader = new JwsProtectedHeader();

      protectedHeader.setCritical(crit);
      protectedHeader.setHeader("exp", 1739760218135l);

      JwsUnprotectedHeader unprotectedHeader = new JwsUnprotectedHeader();
      unprotectedHeader
            .setAlgorithm(JwaAlgorithms.HMAC_SHA_512.getSpecName());

      SecretKeySpec secretKey = new SecretKeySpec(secretKeyinBytes,
            JwaAlgorithms.HMAC_SHA_512.getJavaName());
      String payload = "{\"payload\" : \"the content that used to sign.\"}";

      HmacJwsSigner signer = new HmacJwsSigner(secretKey);
      JwsDocument jws;
      try {
        jws = JwsMaker.generateFromPayload(payload,
                protectedHeader, unprotectedHeader, signer);

      assertEquals(jws.getJsonSerialisation(), JWS_TEST_CASE_9);

      jws = JwsMaker.generateFromJws(jws.getJsonSerialisation());
      assertTrue(jws.verifySignatureWith(secretKey));
      } catch (SecurityException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }

    @Test
    public void JWSTestCase11And12(){
        List<String> crit = new ArrayList<String>();
        crit.add("exp");

        JwsProtectedHeader protectedHeader;
        protectedHeader = new JwsProtectedHeader();

      protectedHeader.setCritical(crit);
      protectedHeader.setHeader("exp", 1739760218135l);
      protectedHeader.setAlgorithm(JwaAlgorithms.HMAC_SHA_512.getSpecName());

      JwsUnprotectedHeader unprotectedHeader = new JwsUnprotectedHeader();

      SecretKeySpec secretKey = new SecretKeySpec(secretKeyinBytes,
            JwaAlgorithms.HMAC_SHA_512.getJavaName());
      String payload = "{\"payload\" : \"the content that used to sign.\"}";

      HmacJwsSigner signer = new HmacJwsSigner(secretKey);
      JwsDocument jws;
      try {
        jws = JwsMaker.generateFromPayload(payload,
                protectedHeader, unprotectedHeader, signer);

      assertEquals(jws.getJsonSerialisation(), JWS_TEST_CASE_11);

      jws = JwsMaker.generateFromJws(jws.getJsonSerialisation());
      assertTrue(jws.verifySignatureWith(secretKey));
      } catch (SecurityException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }

    @Test
    public void JWSTestCase13And14(){
        List<String> crit = new ArrayList<String>();
        crit.add("exp");

        JwsProtectedHeader protectedHeader;
        protectedHeader = new JwsProtectedHeader();

      protectedHeader.setCritical(crit);
      protectedHeader.setHeader("exp", 1739760218135l);
      protectedHeader.setAlgorithm(JwaAlgorithms.HMAC_SHA_512.getSpecName());

      JwsUnprotectedHeader unprotectedHeader = new JwsUnprotectedHeader();

      SecretKeySpec secretKey = new SecretKeySpec(secretKeyinBytes,
            JwaAlgorithms.HMAC_SHA_512.getJavaName());
      String payload = "{\"payload\" : \"the content that used to sign.\"}";

      HmacJwsSigner signer = new HmacJwsSigner(secretKey);
      JwsDocument jws;
      try {
        jws = JwsMaker.generateFromPayload(payload,
                protectedHeader, unprotectedHeader, signer);

      assertEquals(jws.getCompactSerialisation(), JWS_TEST_CASE_13);

      jws = JwsMaker.generateFromJws(jws.getJsonSerialisation());
      assertTrue(jws.verifySignatureWith(secretKey));
      } catch (SecurityException
             e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }

}
