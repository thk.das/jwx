/*
 * Copyright 2015 Peter Leo Gorski, Luigi Lo Iacono, Daniel Behnam Torkian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fhk.jwx.jws.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import de.fhk.jwx.jwa.JwaAlgorithms;
import de.fhk.jwx.jws.JwsDocument;
import de.fhk.jwx.jws.JwsMaker;
import de.fhk.jwx.jws.JwsProtectedHeader;
import de.fhk.jwx.jws.crypto.HmacJwsSigner;
import de.fhk.jwx.jws.crypto.HmacJwsVerifier;
import de.fhk.jwx.jws.crypto.NoneJwsSigner;
import de.fhk.jwx.jws.crypto.NoneJwsVerifier;

public class JWSTests {
	
	private static final String PAYLOAD = "It’s a dangerous business, Frodo, going out your door. You step onto the road, and if you don't keep your feet, there’s no knowing where you might be swept off to.";
	private static final String ENCODED_PAYLOAD = "SXTigJlzIGEgZGFuZ2Vyb3VzIGJ1c2luZXNzLCBGcm9kbywgZ29pbmcgb3V0IH"
        + "lvdXIgZG9vci4gWW91IHN0ZXAgb250byB0aGUgcm9hZCwgYW5kIGlmIHlvdSBk"
        + "b24ndCBrZWVwIHlvdXIgZmVldCwgdGhlcmXigJlzIG5vIGtub3dpbmcgd2hlcm"
        + "UgeW91IG1pZ2h0IGJlIHN3ZXB0IG9mZiB0by4";
	private static final String keyId = "018c0ae5-4d9b-471b-bfd6-eef314bc7037";
	private static final String SIGNATURE_INPUT = "eyJhbGciOiJIUzI1NiIsImtpZCI6IjAxOGMwYWU1LTRkOWItNDcxYi1iZmQ2LW"
		+ "VlZjMxNGJjNzAzNyJ9"
	    + "."
	    + "SXTigJlzIGEgZGFuZ2Vyb3VzIGJ1c2luZXNzLCBGcm9kbywgZ29pbmcgb3V0IH"
	    + "lvdXIgZG9vci4gWW91IHN0ZXAgb250byB0aGUgcm9hZCwgYW5kIGlmIHlvdSBk"
	    + "b24ndCBrZWVwIHlvdXIgZmVldCwgdGhlcmXigJlzIG5vIGtub3dpbmcgd2hlcm"
	    + "UgeW91IG1pZ2h0IGJlIHN3ZXB0IG9mZiB0by4";
	private static final String SIGNATURE = "s0h6KThzkfBBBkLspW1h84VsJZFTsPPqMDA7g1Md7p0";

	@Test
	public void JWSTest() {
		JwsProtectedHeader protectedHeader = new JwsProtectedHeader();
		protectedHeader.setAlgorithm(JwaAlgorithms.HMAC_SHA_256.getSpecName());
		protectedHeader.setKeyId(keyId);
		JwsDocument jws = JwsMaker.generateFromPayload(PAYLOAD, protectedHeader, new HmacJwsSigner("hJtXIZ2uSN5kbQfbtTNWbpdmhkV8FJG-Onbc6mxCcYg"));
		
		assertEquals(jws.getSignatureInput(), SIGNATURE_INPUT);
		assertEquals(
				jws.getCompactSerialisation()
				, SIGNATURE_INPUT + "." + SIGNATURE);
		assertTrue(jws.verifySignatureWith(new HmacJwsVerifier("hJtXIZ2uSN5kbQfbtTNWbpdmhkV8FJG-Onbc6mxCcYg")));
		JwsDocument consumerCompactSerialisation = JwsMaker.generateFromJws(jws.getCompactSerialisation());
		System.out.println(consumerCompactSerialisation.getCompactSerialisation());

		System.out.println("Compact Serialisation DETACHED");
		System.out.println(jws.getCompactDetachedSerialisation());
		JwsDocument consumerCompactSerialisationDETACHED = JwsMaker.generateFromJws(jws.getCompactDetachedSerialisation(), ENCODED_PAYLOAD);
		System.out.println(consumerCompactSerialisationDETACHED.getCompactDetachedSerialisation());
		
		System.out.println("JSON Serialisation");
		System.out.println(jws.getJsonSerialisation());
		JwsDocument consumerJsonSerialisation = JwsMaker.generateFromJws(jws.getJsonSerialisation());
		System.out.println(consumerJsonSerialisation.getJsonSerialisation());
		
		System.out.println("JSON Serialisation PRETTYPRINTED");
		System.out.println(jws.getJsonSerialisation(true));
		JwsDocument consumerJsonSerialisationPrettyPrinted = JwsMaker.generateFromJws(jws.getJsonSerialisation(true));
		System.out.println(consumerJsonSerialisationPrettyPrinted.getJsonSerialisation(true));
		
		
		System.out.println("JSON Serialisation DETACHED");
		System.out.println(jws.getJsonDetachedSerialisation());
		JwsDocument consumerJsonSerialisationDetached = JwsMaker.generateFromJws(jws.getJsonDetachedSerialisation(), ENCODED_PAYLOAD);
		System.out.println(consumerJsonSerialisationDetached.getJsonDetachedSerialisation());
		
		System.out.println("JSON Serialisation DETACHED & PRITTYPRINTED");
		System.out.println(jws.getJsonDetachedSerialisation(true));
		JwsDocument consumerJsonSerialisationDetachedPrittyPrinted = JwsMaker.generateFromJws(jws.getJsonDetachedSerialisation(true), ENCODED_PAYLOAD);
		System.out.println(consumerJsonSerialisationDetachedPrittyPrinted.getJsonDetachedSerialisation(true));
		
		System.out.println("JSON Flattened Serialisation");
		System.out.println(jws.getJsonFlattenedSerialisation());
		JwsDocument consumerJsonFlattenedSerialisation = JwsMaker.generateFromJws(jws.getJsonFlattenedSerialisation(), null);
		System.out.println(consumerJsonFlattenedSerialisation.getJsonFlattenedSerialisation());
		
		System.out.println("JSON Flattened Serialisation PRITTYPRINTED");
		System.out.println(jws.getJsonFlattenedSerialisation(true));
		JwsDocument consumerJsonFlattenedSerialisationPrettyPrinted = JwsMaker.generateFromJws(jws.getJsonFlattenedSerialisation(true), null);
		System.out.println(consumerJsonFlattenedSerialisationPrettyPrinted.getJsonFlattenedSerialisation(true));
		
		
		System.out.println("JSON Flattened Serialisation DETACHED");
		System.out.println(jws.getJsonFlattenedDetachedSerialisation());
		JwsDocument consumerJsonFlattenedSerialisationDetached = JwsMaker.generateFromJws(jws.getJsonFlattenedDetachedSerialisation(), ENCODED_PAYLOAD);
		System.out.println(consumerJsonFlattenedSerialisationDetached.getJsonFlattenedDetachedSerialisation());
		
		System.out.println("JSON Flattened Serialisation DETACHED & PRITTYPRINTED");
		System.out.println(jws.getJsonFlattenedDetachedSerialisation(true));
		JwsDocument consumerJsonFlattenedSerialisationDetachedPrettyPrinted = JwsMaker.generateFromJws(jws.getJsonFlattenedDetachedSerialisation(true), ENCODED_PAYLOAD);
		System.out.println(consumerJsonFlattenedSerialisationDetachedPrettyPrinted.getJsonFlattenedDetachedSerialisation(true));
		
		protectedHeader = new JwsProtectedHeader();
        protectedHeader.setAlgorithm(JwaAlgorithms.PLAIN_TEXT.getSpecName());
        jws = JwsMaker.generateFromPayload(PAYLOAD, protectedHeader, new NoneJwsSigner());
		
        System.out.println("JSON Serialisation PRETTYPRINTED");
        System.out.println(jws.getJsonSerialisation(true));
        consumerJsonSerialisationPrettyPrinted = JwsMaker.generateFromJws(jws.getJsonSerialisation(true));
        System.out.println(consumerJsonSerialisationPrettyPrinted.getJsonSerialisation(true));
        
        System.out.println("Compact Serialisation");
        System.out.println(jws.getCompactSerialisation());
        assertTrue(jws.verifySignatureWith(new NoneJwsVerifier()));
	}
}
