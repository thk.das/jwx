/*
 * Copyright 2015 Peter Leo Gorski, Luigi Lo Iacono, Daniel Behnam Torkian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fhk.jwx.jws.crypto;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;
import java.util.Arrays;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import de.fhk.jwx.jwa.JwaAlgorithms;
import de.fhk.jwx.jws.JwsHeaders;
import de.fhk.jwx.util.JoseUtils;

/**
 * this class provides the verifier for HS-* algorithms.
 *
 */
public class HmacJwsVerifier implements JwsVerifier {
    private SecretKeySpec key;
    private AlgorithmParameterSpec spec;

    public HmacJwsVerifier(String encodedKey) {
        this(new SecretKeySpec(JoseUtils.base64URLTobytes(encodedKey), "AES"));
    }

    public HmacJwsVerifier(SecretKeySpec key) {
        this(key, null);
    }

    public HmacJwsVerifier(SecretKeySpec key, AlgorithmParameterSpec spec) {
        this.key = key;
        this.spec = spec;
    }

    public boolean verify(JwsHeaders headers, byte[] content,
            byte[] signature) {
        if (!isProvidedAlg(headers.getAlgorithm())) return false;;
        Mac mac;
        byte[] expected = new byte[0];
        try {
            mac = Mac.getInstance(JwaAlgorithms.getAlgorithm(
                    headers.getAlgorithm()).getJavaName());

            if (spec == null) {
                mac.init(key);
            } else {
                mac.init(key, spec);
            }
            mac.update(content);

            expected = mac.doFinal();
        } catch (NoSuchAlgorithmException | InvalidKeyException
                | InvalidAlgorithmParameterException  e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return Arrays.equals(expected, signature);
    }

    private boolean isProvidedAlg(String alg){
        return JwaAlgorithms.isHmacSign(alg);
    }



}
