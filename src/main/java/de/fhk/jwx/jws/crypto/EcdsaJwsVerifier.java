/*
 * Copyright 2015 Peter Leo Gorski, Luigi Lo Iacono, Daniel Behnam Torkian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fhk.jwx.jws.crypto;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.PublicKey;
import java.security.spec.AlgorithmParameterSpec;

import de.fhk.jwx.jwa.JwaAlgorithms;
import de.fhk.jwx.jwa.JwaLengthOfSignature;
import de.fhk.jwx.jws.JwsHeaders;

/**
 * this class provides the verifier for ES-* algorithms.
 *
 */
public class EcdsaJwsVerifier extends AbstractPublicKeyJwsVerifier {
    public EcdsaJwsVerifier(PublicKey key) {
        this(key, null);
    }

    public EcdsaJwsVerifier(PublicKey key, AlgorithmParameterSpec spec) {
        super(key, spec);
    }

    @Override
    public boolean verify(JwsHeaders headers, byte[] content,
            byte[] signature) {
        int length = JwaLengthOfSignature.lengthOf(JwaAlgorithms.getAlgorithm(headers.getAlgorithm()));
        if (signature.length!=length) return false;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] rByte = new byte[signature.length / 2];
        byte[] sByte = new byte[signature.length / 2];
        System.arraycopy(signature, 0, rByte, 0, rByte.length);
        System.arraycopy(signature, rByte.length, sByte, 0, sByte.length);
        try {
            baos.write(48);
            if (signature.length > 127) {
                baos.write(-127);
            }
            baos.write(signature.length+4);
            baos.write(2);
            baos.write(rByte.length);
            baos.write(rByte, 0, rByte.length);
            baos.write(2);
            baos.write(sByte.length);
            baos.write(sByte, 0, sByte.length);
            baos.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        return super.verify(headers, content, baos.toByteArray());
    }

    @Override
    boolean isProvidedAlg(String alg) {
        
        return JwaAlgorithms.isEcdsaSign(alg);
    }



}
