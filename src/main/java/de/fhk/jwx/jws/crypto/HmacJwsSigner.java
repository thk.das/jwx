/*
 * Copyright 2015 Peter Leo Gorski, Luigi Lo Iacono, Daniel Behnam Torkian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fhk.jwx.jws.crypto;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import de.fhk.jwx.jwa.JwaAlgorithms;
import de.fhk.jwx.jws.JwsHeaders;
import de.fhk.jwx.util.JoseUtils;

/**
 * this class provides the signer for HS-* algorithms.
 *
 */
public class HmacJwsSigner implements JwsSigner {
    private SecretKeySpec key;
    private AlgorithmParameterSpec spec;

    public HmacJwsSigner(SecretKeySpec key) {
        this(key, null);
    }

    public HmacJwsSigner(SecretKeySpec key, AlgorithmParameterSpec spec) {
        super();
        this.key = key;
        this.spec = spec;
    }

    public HmacJwsSigner(String encodedKey) {
        super();
        SecretKeySpec k = new SecretKeySpec(
                JoseUtils.base64URLTobytes(encodedKey), "AES");
        this.key = k;
    }


    public boolean isProvidedAlg(String alg) {
        return (JwaAlgorithms.isHmacSign(alg));
    }

    public byte[] sign(JwsHeaders headers, byte[] content) {
        if (!isProvidedAlg(headers.getAlgorithm())) throw new SecurityException("alg not provided!");
        Mac mac;
        try {
            mac = Mac.getInstance(JwaAlgorithms.getAlgorithm(
                    headers.getAlgorithm()).getJavaName());

            if (spec == null) {
                mac.init(key);
            } else {
                mac.init(key, spec);
            }
            mac.update(content);
            return (mac.doFinal());

        } catch (NoSuchAlgorithmException | InvalidKeyException
                | InvalidAlgorithmParameterException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;

        }
    }
}
