/*
 * Copyright 2015 Peter Leo Gorski, Luigi Lo Iacono, Daniel Behnam Torkian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fhk.jwx.jws;

import java.net.URL;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import de.fhk.jwx.JoseHeaders;
import de.fhk.jwx.jwk.JwkKey;
import de.fhk.jwx.util.JoseUtils;

/**
 * this class represents JWS headers and implements JoseHeaders.
 *
 */
public class JwsHeaders implements JoseHeaders {
    private ObjectMapper mapper = new ObjectMapper();
    private Map<String, Object> headers = new LinkedHashMap<String, Object>();

    public JwsHeaders() {
    }

    public void setHeader(String key, Object value) {
        this.headers.put(key, value);
    }

    public Object getHeader(String key) {
        return headers.get(key);
    }

    public void setType(String type) {
        setHeader(JoseHeaders.HEADER_TYPE, type);
    }

    public String getType() {
        return (String) getHeader(JoseHeaders.HEADER_TYPE);
    }

    public void setContentType(String type) {
        setHeader(JoseHeaders.HEADER_CONTENT_TYPE, type);
    }

    public String getContentType() {
        return (String) getHeader(JoseHeaders.HEADER_CONTENT_TYPE);
    }

    public void setAlgorithm(String algo) {
        setHeader(JoseHeaders.HEADER_ALGORITHM, algo);
    }

    public String getAlgorithm() {
        return (String) getHeader(JoseHeaders.HEADER_ALGORITHM);
    }

    public void setKeyId(String kid) {
        setHeader(JoseHeaders.HEADER_KEY_ID, kid);
    }

    public String getKeyId() {
        return (String) getHeader(JoseHeaders.HEADER_KEY_ID);
    }

    public void setX509Url(String x509Url) {
        setHeader(JoseHeaders.HEADER_X509_URL, x509Url);
    }

    public URL getX509Url() {
        return (URL) getHeader(JoseHeaders.HEADER_X509_URL);
    }

    public void setX509Chain(List<String> x509Chain) {
        setHeader(JoseHeaders.HEADER_X509_CHAIN, x509Chain);
    }

    @SuppressWarnings("unchecked")
    public List<String> getX509Chain() {
        return (List<String>) getHeader(JoseHeaders.HEADER_X509_CHAIN);
    }

    public void setX509Thumbprint(String x509Thumbprint) {
        setHeader(JoseHeaders.HEADER_X509_THUMBPRINT, x509Thumbprint);
    }

    public String getX509Thumbprint() {
        return (String) getHeader(JoseHeaders.HEADER_X509_THUMBPRINT);
    }

    public void setX509ThumbprintSHA256(String x509Thumbprint) {
        setHeader(JoseHeaders.HEADER_X509_THUMBPRINT_SHA256, x509Thumbprint);
    }

    public String getX509ThumbprintSHA256() {
        return (String) getHeader(JoseHeaders.HEADER_X509_THUMBPRINT_SHA256);
    }

    public String getHeaders(boolean prettyPrint) {
        try {
            if (prettyPrint)

                return mapper.writerWithDefaultPrettyPrinter()
                        .writeValueAsString(headers);

            else
                return mapper.writeValueAsString(headers);
        } catch (JsonProcessingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
    }

    public String getHeaders() {
        return getHeaders(false);
    }

    public String getBase64URLEncodedHeaders() {
        try {
            return JoseUtils.stringToBase64URL(mapper
                    .writeValueAsString(headers));
        } catch (JsonProcessingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
    }

    public void setHeaders(Map<String, Object> headers) {
        this.headers.putAll(headers);
    }

    public Map<String, Object> getAllHeaders() {
        return this.headers;
    }

    public void setJSONWebKeySet(URL url) {
        setHeader(JoseHeaders.HEADER_JSON_WEB_KEY_SET_URL, url);
    }

    public URL getJSONWebKeySet() {
        return (URL) getHeader(JoseHeaders.HEADER_JSON_WEB_KEY_SET_URL);
    }

    public void setJSONWebKey(JwkKey key) {
        setHeader(JoseHeaders.HEADER_JSON_WEB_KEY, key);

    }

    public JwkKey getJSONWebKey() {
        return (JwkKey) getHeader(JoseHeaders.HEADER_JSON_WEB_KEY);
    }

    public void setX509Url(URL x509Url) {
        setHeader(JoseHeaders.HEADER_X509_URL, x509Url);
    }

    public int getNumberOfEntries() {
        return this.headers.size();
    }

    @SuppressWarnings("unchecked")
    public List<String> getCriticalHeaders() {
        return (List<String>) this.headers.get(JoseHeaders.HEADER_CRITICAL);
    }

    public boolean contains(Object key) {
        return this.headers.containsKey(key);
    }

    public Set<String> getKeys() {
        // TODO Auto-generated method stub
        return this.headers.keySet();
    }

    @Override
    public void removeHeaderEntry(String key) {
        this.headers.remove(key);

    }
}
