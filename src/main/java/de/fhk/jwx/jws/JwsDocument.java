/*
 * Copyright 2015 Peter Leo Gorski, Luigi Lo Iacono, Daniel Behnam Torkian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fhk.jwx.jws;

import java.io.IOException;
import java.nio.charset.Charset;
import java.security.Key;
import java.security.interfaces.ECPrivateKey;
import java.security.interfaces.ECPublicKey;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.crypto.spec.SecretKeySpec;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import de.fhk.jwx.JoseHeaders;
import de.fhk.jwx.jwa.JwaAlgorithms;
import de.fhk.jwx.jwk.JwkKey;
import de.fhk.jwx.jws.crypto.EcdsaJwsSigner;
import de.fhk.jwx.jws.crypto.EcdsaJwsVerifier;
import de.fhk.jwx.jws.crypto.HmacJwsSigner;
import de.fhk.jwx.jws.crypto.HmacJwsVerifier;
import de.fhk.jwx.jws.crypto.JwsSigner;
import de.fhk.jwx.jws.crypto.JwsVerifier;
import de.fhk.jwx.jws.crypto.NoneJwsSigner;
import de.fhk.jwx.jws.crypto.NoneJwsVerifier;
import de.fhk.jwx.jws.crypto.RsaJwsSigner;
import de.fhk.jwx.jws.crypto.RsaJwsVerifier;
import de.fhk.jwx.jws.crypto.RsassapssJwsSigner;
import de.fhk.jwx.jws.crypto.RsassapssJwsVerifier;
import de.fhk.jwx.util.JoseUtils;
import de.fhk.jwx.util.JwsSignerAssigning;

/**
 * JwsDocument represents a JWS object. Following serialisations are supported:
 * 1. Compact Serialisation 2. Compact Detached Serialisation 3. Json
 * Serialisation 4. Json Detached Serialisation 5. Json Flattended Serialisation
 * 6. Json Detached and Flattended Serialisation
 * 
 * this class has no public constructor. see JwsMaker.
 *
 */
public final class JwsDocument {
    private boolean compactSerialisation = true;
    private String encodedPayload;
    private String payload;
    private boolean producer;
    private List<JwsSignatureEntry> signatureEntries = new ArrayList<JwsSignatureEntry>();

    /**
     * protected constructor (please see JwsMaker)
     * 
     * @param input
     * @param detachedPayload
     * @param type
     *            of Jws (consumer or producer)
     */
    protected JwsDocument(String input, String detachedPayload, JwsType type) {
        switch (type) {
        case PRODUCER:
            this.setProducer(true);
            this.payload = input;
            this.encodedPayload = JoseUtils.stringToBase64URL(this.payload);
            break;
        case CONSUMER:
            try {
                setProducer(false);
                ObjectMapper mapper = new ObjectMapper();
                if (input.startsWith("{") && input.endsWith("}")) {

                    LinkedHashMap<String, Object> jwsAsJson;

                    jwsAsJson = mapper.readValue(input,
                            new TypeReference<LinkedHashMap<String, Object>>() {
                            });

                    this.encodedPayload = (String) jwsAsJson.get("payload");
                    if (this.encodedPayload == null & detachedPayload != null)
                        this.encodedPayload = detachedPayload;
                    if (this.encodedPayload != null)
						this.payload = JoseUtils
						        .base64URLToString(this.encodedPayload);
                    @SuppressWarnings("unchecked")
                    List<LinkedHashMap<String, Object>> signatures = (List<LinkedHashMap<String, Object>>) jwsAsJson
                            .get("signatures");
                    if (signatures != null) {
                        for (LinkedHashMap<String, Object> signEntry : signatures) {
                            JwsProtectedHeader protectedHeader = null;
                            if (signEntry.get("protected") != null) {
                                protectedHeader = new JwsProtectedHeader();
                                LinkedHashMap<String, Object> protectedHeaderAsJson = null;
								protectedHeaderAsJson = mapper
								        .readValue(
								                JoseUtils
								                        .base64URLToString((String) signEntry
								                                .get("protected")),
								                new TypeReference<LinkedHashMap<String, Object>>() {
								                });
                                protectedHeader
                                        .setHeaders(protectedHeaderAsJson);
                            }
                            JwsUnprotectedHeader unprotectedHeader = null;
                            if (signEntry.get("header") != null) {
                                unprotectedHeader = new JwsUnprotectedHeader();
                                @SuppressWarnings("unchecked")
                                LinkedHashMap<String, Object> unprotectedHeaderAsJson = (LinkedHashMap<String, Object>) signEntry
                                        .get("header");
                                unprotectedHeader
                                        .setHeaders(unprotectedHeaderAsJson);
                            }

                            JwsSignatureEntry signature = ((protectedHeader != null) && (unprotectedHeader != null)) ? new JwsSignatureEntry(
                                    this.encodedPayload, protectedHeader,
                                    unprotectedHeader)
                                    : (((protectedHeader != null) && (unprotectedHeader == null)) ? new JwsSignatureEntry(
                                            this.encodedPayload,
                                            protectedHeader)
                                            : ((protectedHeader == null) && (unprotectedHeader != null)) ? new JwsSignatureEntry(
                                                    this.encodedPayload,
                                                    unprotectedHeader) : null);
                            if (signature == null)
                                throw new SecurityException("JWS Erorr");
                            signature.setSignature(((String) signEntry
                                    .get("signature")).getBytes(), true);
                            this.signatureEntries.add(signature);
                        }
                    } else {
                        JwsProtectedHeader protectedHeader = null;
                        if (jwsAsJson.get("protected") != null) {
                            protectedHeader = new JwsProtectedHeader();
                            Map<String, Object> protectedHeaderAsJson = null;
							protectedHeaderAsJson = mapper
							        .readValue(
							                JoseUtils
							                        .base64URLToString((String) jwsAsJson
							                                .get("protected")),
							                new TypeReference<Map<String, Object>>() {
							                });
                            protectedHeader.setHeaders(protectedHeaderAsJson);
                        }
                        JwsUnprotectedHeader unprotectedHeader = null;
                        if (jwsAsJson.get("header") != null) {
                            unprotectedHeader = new JwsUnprotectedHeader();
                            @SuppressWarnings("unchecked")
                            LinkedHashMap<String, Object> unprotectedHeaderAsJson = (LinkedHashMap<String, Object>) jwsAsJson
                                    .get("header");
                            unprotectedHeader
                                    .setHeaders(unprotectedHeaderAsJson);
                        }
                        JwsSignatureEntry signature = ((protectedHeader != null) && (unprotectedHeader != null)) ? new JwsSignatureEntry(
                                this.encodedPayload, protectedHeader,
                                unprotectedHeader)
                                : (((protectedHeader != null) && (unprotectedHeader == null)) ? new JwsSignatureEntry(
                                        this.encodedPayload, protectedHeader)
                                        : ((protectedHeader == null) && (unprotectedHeader != null)) ? new JwsSignatureEntry(
                                                this.encodedPayload,
                                                unprotectedHeader) : null);
                        if (signature == null)
                            throw new SecurityException("JWS Erorr");
                        signature.setSignature(((String) jwsAsJson
                                .get("signature")).getBytes(), true);
                        this.signatureEntries.add(signature);

                    }
                } else {
                    String[] jwsParts = input.split("\\.");
                    if (jwsParts.length > 3)
                        throw new SecurityException(
                                "jws has more than three parts");
                    if (jwsParts.length < 3)
                        throw new SecurityException(
                                "jws has less than three parts");
                    else if (jwsParts[1].equals("") && detachedPayload != null)
                        jwsParts[1] = detachedPayload;
                    this.encodedPayload = jwsParts[1];
                    this.payload = JoseUtils.base64URLToString(encodedPayload);
                    JwsProtectedHeader protectedHeader = new JwsProtectedHeader();
                    LinkedHashMap<String, Object> protectedHeaderAsJson = null;
					protectedHeaderAsJson = mapper
					        .readValue(
					                JoseUtils.base64URLToString(jwsParts[0]),
					                new TypeReference<LinkedHashMap<String, Object>>() {
					                });
                    protectedHeader.setHeaders(protectedHeaderAsJson);
                    if (!JoseUtils.stringToBase64URL(
                            protectedHeader.getHeaders()).equals(jwsParts[0]))
                        throw new SecurityException(
                                "not able to reconstruct protected header (double header parametres)");
                    JwsSignatureEntry signEntry = new JwsSignatureEntry(
                            this.encodedPayload, protectedHeader);
                    signEntry.setSignature(jwsParts[2].getBytes(), true);
                    signatureEntries.add(signEntry);
                }
                break;
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    /**
     * this method allows to add additionally signature to a JwsDocument
     * 
     * @param signer
     * @param protectedHeader
     * @param unprotectedHeader
     */
    public void additionallySign(JwsSigner signer,
            JwsProtectedHeader protectedHeader,
            JwsUnprotectedHeader unprotectedHeader) {
        JwsSignatureEntry sigEntry;
        sigEntry = new JwsSignatureEntry(this.payload, protectedHeader,
                unprotectedHeader);

        JwsHeaders headers = new JwsHeaders();
        int size = 0;
        if (protectedHeader != null && sigEntry.sizeOfProtectedHeader() > 0) {
            headers.setHeaders(protectedHeader.getAllHeaders());
            size += protectedHeader.getAllHeaders().size();
        }

        if (unprotectedHeader != null && sigEntry.sizeOfUnprotectedHeader() > 0) {
            if (unprotectedHeader.getHeader(JoseHeaders.HEADER_CRITICAL) != null)
                throw new SecurityException(
                        "critical header parameter must be integrity protected!");
            headers.setHeaders(unprotectedHeader.getAllHeaders());
            size += unprotectedHeader.getAllHeaders().size();
        }

        if (headers.getKeys().size() < size) {
            throw new SecurityException("double entries detected!");
        }
        sigEntry.setSignature(signer
                .sign(headers,
                        (((protectedHeader != null && sigEntry
                                .sizeOfProtectedHeader() > 0) ? protectedHeader
                                .getBase64URLEncodedHeaders() : "")
                                + "." + this.encodedPayload).getBytes()));
        this.signatureEntries.add(sigEntry);
    }

    /**
     * this method allows to add additionally signature to a JwsDocument
     * 
     * @param jwk
     * @param protectedHeader
     * @param unprotectedHeader
     */
    public void additionallySignWith(JwkKey jwk,
            JwsProtectedHeader protectedHeader,
            JwsUnprotectedHeader unprotectedHeader) {
        additionallySignWith(
                JwsSignerAssigning.getJwsSigner(jwk,
                        unionHeaders(protectedHeader, unprotectedHeader)
                                .getAlgorithm()), protectedHeader,
                unprotectedHeader);
    }

    /**
     * this method allows to add additionally signature to a JwsDocument
     * 
     * @param signer
     * @param protectedHeader
     * @param unprotectedHeader
     */
    public void additionallySignWith(JwsSigner signer,
            JwsProtectedHeader protectedHeader,
            JwsUnprotectedHeader unprotectedHeader) {
        JwsSignatureEntry signatureEntry;
        signatureEntry = new JwsSignatureEntry(encodedPayload, protectedHeader,
                unprotectedHeader);

        byte[] bytes;
        if (signatureEntry.getProtectedHeader() == null
                && signatureEntry.getUnprotectedHeader() != null)
            bytes = ("." + signatureEntry.getEncodedJwsPayload())
                    .getBytes(Charset.forName("UTF-8"));
        else
            bytes = (signatureEntry.getProtectedHeader() + "." + signatureEntry
                    .getEncodedJwsPayload()).getBytes(Charset.forName("UTF-8"));
        byte[] sig = signer.sign(
                unionHeaders(protectedHeader, unprotectedHeader), bytes);
        signatureEntry.setSignatureBytes(sig);
        this.signatureEntries.add(signatureEntry);
    }

    /**
     * this method returns the compact and detached serialisation of JwsDocument
     * if it is compactable
     * 
     * @return Compact Detached Serialisation of JWS
     */
    public String getCompactDetachedSerialisation() {
        if (this.isCompactable() && signatureEntries.size() == 1)
            return (signatureEntries.get(0).getProtectedHeader() + "." + "." + signatureEntries
                    .get(0).getSignature());
        else
            throw new SecurityException("there are more than one signature!");
    }

    /**
     * this method returns the compact serialisation of JwsDocument if it is
     * compactable
     * 
     * @return Compact Serialisation of JWS
     */
    public String getCompactSerialisation() {


            if (this.isCompactable() && signatureEntries.size() == 1)
                return (this.getSignatureInput() + "." + signatureEntries
                        .get(0).getSignature());
            else
                throw new SecurityException(
                        "there are more than one signature or JWS Document is not compactable!");


    }

    /**
     * @return decoded payload
     */
    public String getDecodedPayload() {
        return new String(this.payload);
    }

    /**
     * @return encoded payload
     */
    public String getEncodedPayload() {
        return new String(this.encodedPayload);
    }

    /**
     * @param signatureEntry
     * @return all of headers of a signature entry
     */
    private JwsHeaders getHeaders(int signatureEntry) {
        int sizeOfHeaders = 0;
        ObjectMapper mapper = new ObjectMapper();
        JwsHeaders headers = new JwsHeaders();
        try {
            if (this.signatureEntries.get(signatureEntry).getProtectedHeader() != null) {
                Map<String, Object> protectedHeaderAsJson = null;
				protectedHeaderAsJson = mapper.readValue(
				        JoseUtils.base64URLToString(this.signatureEntries.get(
				                signatureEntry).getProtectedHeader()),
				        new TypeReference<Map<String, Object>>() {
				        });
                headers.setHeaders(protectedHeaderAsJson);
                sizeOfHeaders += protectedHeaderAsJson.size();
            }

            if (this.signatureEntries.get(signatureEntry)
                    .getUnprotectedHeader() != null) {
                Map<String, Object> unprotectedHeaderAsJson = mapper.readValue(
                        this.signatureEntries.get(signatureEntry)
                                .getUnprotectedHeader(),
                        new TypeReference<Map<String, Object>>() {
                        });

                headers.setHeaders(unprotectedHeaderAsJson);
                sizeOfHeaders += unprotectedHeaderAsJson.size();
            }

            Set<String> areThereDoubleHeaders = headers.getAllHeaders()
                    .keySet();
            if (areThereDoubleHeaders.size() < sizeOfHeaders)
                throw new SecurityException("headers contain double entries!");
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return headers;

    }

    /**
     * @return JSON Detached Serialisation
     */
    public String getJsonDetachedSerialisation() {
        return this.getJsonDetachedSerialisation(false);
    }

    /**
     * @param prettyPrint
     * @return JSON Detached Serialisation
     */
    public String getJsonDetachedSerialisation(boolean prettyPrint) {
        try {
            Map<String, Object> json = new LinkedHashMap<String, Object>();

            List<Map<String, Object>> signatures = new ArrayList<Map<String, Object>>();
            for (JwsSignatureEntry signEntry : signatureEntries) {
                Map<String, Object> entry = new LinkedHashMap<String, Object>();

                if (signEntry.getProtectedHeader() != null)
                    entry.put("protected", signEntry.getProtectedHeader());

                if (signEntry.getUnprotectedHeader() != null)
                    entry.put("header",
                            signEntry.getUnprotectedHeaderAsUnprotectedHeader());
                entry.put("signature", signEntry.getSignature());
                signatures.add(entry);
            }
            json.put("signatures", signatures);
            ObjectMapper mapper = new ObjectMapper();
            if (prettyPrint)
                return mapper.writerWithDefaultPrettyPrinter()
                        .writeValueAsString(json);
            else
                return mapper.writeValueAsString(json);
        } catch (JsonProcessingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
    }

    /**
     * @return JSON Detached Serialisation as JsonNode
     */
    public JsonNode getJsonDetachedSerialisationAsJsonNode() {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readTree(this.getJsonDetachedSerialisation());
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
    }

    public String getJsonFlattenedDetachedSerialisation() {
        return this.getJsonFlattenedDetachedSerialisation(false);
    }

    public String getJsonFlattenedDetachedSerialisation(boolean prettyPrint) {
        try {
            if (signatureEntries.size() == 1) {
                Map<String, Object> json = new LinkedHashMap<String, Object>();

                if (signatureEntries.get(0).getProtectedHeader() != null)
                    json.put("protected", signatureEntries.get(0)
                            .getProtectedHeader());

                if (signatureEntries.get(0).getUnprotectedHeader() != null)
                    json.put("header", signatureEntries.get(0)
                            .getUnprotectedHeaderAsUnprotectedHeader());
                json.put("signature", signatureEntries.get(0).getSignature());
                ObjectMapper mapper = new ObjectMapper();
                if (prettyPrint)
                    return mapper.writerWithDefaultPrettyPrinter()
                            .writeValueAsString(json);
                else
                    return mapper.writeValueAsString(json);
            } else
                throw new SecurityException(
                        "there are more than one signature or no signature");
        } catch (JsonProcessingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
    }

    /**
     * @return JSON Flattended Serialisation as JsonNode
     */
    public JsonNode getJsonFlattenedDetachedSerialisationAsJsonNode() {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper
                    .readTree(this.getJsonFlattenedDetachedSerialisation());
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
    }

    /**
     * @return JSON Flattended Serialisation
     */
    public String getJsonFlattenedSerialisation() {
        return this.getJsonFlattenedSerialisation(false);
    }

    /**
     * @param prettyPrint
     */
    public String getJsonFlattenedSerialisation(boolean prettyPrint) {
        if (signatureEntries.size() == 1) {
            Map<String, Object> json = new LinkedHashMap<String, Object>();

            json.put("payload", this.encodedPayload);
            try {
                if (signatureEntries.get(0).getProtectedHeader() != null)
                    json.put("protected", signatureEntries.get(0)
                            .getProtectedHeader());

                if (signatureEntries.get(0).getUnprotectedHeader() != null)
                    json.put("header", signatureEntries.get(0)
                            .getUnprotectedHeaderAsUnprotectedHeader());
                json.put("signature", signatureEntries.get(0).getSignature());
                ObjectMapper mapper = new ObjectMapper();
                if (prettyPrint)
                    return mapper.writerWithDefaultPrettyPrinter()
                            .writeValueAsString(json);
                else
                    return mapper.writeValueAsString(json);
            } catch (JsonProcessingException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return null;
            }
        } else
            throw new SecurityException(
                    "there are more than one signature or no signature");
    }

    /**
     * @return JSON Flattended Serialisation as JsonNode
     */
    public JsonNode getJsonFlattenedSerialisationAsJsonNode() {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readTree(this.getJsonFlattenedSerialisation());
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
    }

    /**
     * @return JSON Serialisation
     */
    public String getJsonSerialisation() {
        return this.getJsonSerialisation(false);
    }

    /**
     * @param prettyPrint
     * @return JSON Serialisation
     */
    public String getJsonSerialisation(boolean prettyPrint) {
        try {
            if (this.payload == null)
                throw new SecurityException("there is no payload!");
            Map<String, Object> json = new LinkedHashMap<String, Object>();

            json.put("payload", this.encodedPayload);
            List<Map<String, Object>> signatures = new ArrayList<Map<String, Object>>();
            for (JwsSignatureEntry signEntry : signatureEntries) {
                Map<String, Object> entry = new LinkedHashMap<String, Object>();

                if (signEntry.getProtectedHeader() != null
                        && signEntry.sizeOfProtectedHeader() > 0)
                    entry.put("protected", signEntry.getProtectedHeader());

                if (signEntry.getUnprotectedHeader() != null
                        && signEntry.sizeOfUnprotectedHeader() > 0)
                    entry.put("header",
                            signEntry.getUnprotectedHeaderAsUnprotectedHeader());
                entry.put("signature", signEntry.getSignature());
                signatures.add(entry);

            }
            json.put("signatures", signatures);
            ObjectMapper mapper = new ObjectMapper();
            if (prettyPrint)
                return mapper.writerWithDefaultPrettyPrinter()
                        .writeValueAsString(json);
            else
                return mapper.writeValueAsString(json);
        } catch (JsonProcessingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
    }

    /**
     * @return JSON Serialisation as JsonNode
     */
    public JsonNode getJsonSerialisationAsJsonNode() {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readTree(this.getJsonSerialisation());
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
    }

    /**
     * @param index
     * @return a JwsDocument with just one signature
     */
    public JwsDocument getJwsDocument(int index) {
        JwsDocument jws = new JwsDocument(this.payload, null, JwsType.PRODUCER);
        jws.signatureEntries.add(this.signatureEntries.get(index));
        jws.setProducer(this.isProducer());
        return jws;
    }

    /**
     * @return JwsSignatureEntry if there is just one signature
     */
    public JwsSignatureEntry getSignatureEntry() {
        if (signatureEntries.size() != 1)
            throw new SecurityException(
                    "there are more than one or no signature entries!");
        else
            return getSignatureEntry(0);
    }

    /**
     * @param index
     * @return JwsSignatureEntry
     */
    public JwsSignatureEntry getSignatureEntry(int index) {
        return this.signatureEntries.get(index);
    }

    /**
     * @return signature input, PROTECTED_HEADER.ENCODED_PAYLOAD as a string
     */
    public String getSignatureInput() {
        if (signatureEntries.size() != 1)
            throw new SecurityException(
                    "there are more than one or no signature entries!");
        else
            return getSignatureInput(0);
    }

    /**
     * @param signatureIndex
     * @return signature input, PROTECTED_HEADER.ENCODED_PAYLOAD as a string
     */
    public String getSignatureInput(int signatureIndex) {
        if (signatureEntries.get(signatureIndex).getProtectedHeader() != null
                && signatureEntries.get(signatureIndex).sizeOfProtectedHeader() > 0)
            return new String(signatureEntries.get(signatureIndex)
                    .getProtectedHeader() + "." + this.encodedPayload);
        else
            return new String("." + this.encodedPayload);
    }

    /**
     * this method checks whether it is possible to return a compact
     * serialisation of JwsDocument a JwsDocument is not compactable if there
     * are more than one signatures present (to convert such JwsDocuments to a
     * compactable JwsDocuments see getJwsDocument), or there is no protected
     * header.
     * 
     * @return boolean value
     */
    public boolean isCompactable() {
        if (signatureEntries.size() > 1
                || signatureEntries.get(0).getProtectedHeader() == null
                || signatureEntries.get(0).sizeOfProtectedHeader() < 1
                || signatureEntries.get(0).getProtectedHeaderAlgorithm() == null)
            this.compactSerialisation = false;
        return this.compactSerialisation;
    }

    /**
     * this method returns the role of JwsDocument
     * 
     * @return boolean value
     */
    public boolean isProducer() {
        return producer;
    }

    /**
     * @return number of signatures
     */
    public int numberOfSignatures() {
        return signatureEntries.size();
    }

    /**
     * this method allows to switch between JwsDocument roles.
     * 
     * @param value
     */
    private void setProducer(boolean value) {
        this.producer = value;
    }

    protected void signWith(RSAPrivateKey key,
            JwsProtectedHeader protectedHeader,
            JwsUnprotectedHeader unprotectedHeader) {

        additionallySignWith(
                new RsaJwsSigner(key), protectedHeader,
                unprotectedHeader);
    }

    protected void signWith(SecretKeySpec key,
            JwsProtectedHeader protectedHeader,
            JwsUnprotectedHeader unprotectedHeader) {
        additionallySignWith(
                new HmacJwsSigner(key), protectedHeader,
                unprotectedHeader);
    }

    public boolean tryToVerifySignatureWith(JwkKey key, String alg) {
        return tryToVerifySignatureWith(JwsSignerAssigning.getJwsVerifier(key,
                alg));
    }

    public boolean tryToVerifySignatureWith(JwsVerifier validator) {
        if (signatureEntries != null) {
            for (int i = 0; i < signatureEntries.size(); i++) {
                if (this.getJwsDocument(i).verifySignatureWith(validator))
                    return true;
            }
        }
        return false;

    }

    private JwsHeaders unionHeaders(JwsHeaders jwsHeaders,
            JwsHeaders jwsHeaders2) {
        JwsHeaders returnHeaders = new JwsHeaders();
        if (jwsHeaders != null)
            returnHeaders.setHeaders(jwsHeaders.getAllHeaders());
        if (jwsHeaders2 != null)
            returnHeaders.setHeaders(jwsHeaders2.getAllHeaders());
        return returnHeaders;
    }

    public boolean verifySignatureWith(JwkKey key) {
        if (signatureEntries.size() != 1)
            throw new SecurityException(
                    "there are more than one or no signature entries!");
        else
            return verifySignatureWith(key, 0);
    }

    public boolean verifySignatureWith(JwkKey key, int signatureIndex) {
        if (key.getAlgorithm() == null) {
            Map<String, Object> parameters = key.getAllParameters();

            parameters
                    .put(JwkKey.KEY_ALGO, this
                            .getSignatureEntry(signatureIndex)
                            .getHeaderAlgorithm());

            key = new JwkKey(parameters);
        }
        return verifySignatureWith(JwsSignerAssigning.getJwsVerifier(key, this
                .getSignatureEntry(signatureIndex)
                .getHeaderAlgorithm()),
                signatureIndex);
    }

    public boolean verifySignatureWith(JwkKey key, String algo) {
        try {
            if (signatureEntries.size() != 1)

                throw new Exception(
                        "there are more than one or no signature entries!");

            else
                return verifySignatureWith(key, algo, 0);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return false;
        }

    }

    public boolean verifySignatureWith(JwkKey key, String algo,
            int signatureIndex) {
        return verifySignatureWith(
                JwsSignerAssigning.getJwsVerifier(key, algo), signatureIndex);
    }

    public boolean verifySignatureWith(JwsVerifier validator) {
        if (this.signatureEntries.size() == 1) {

            return verifySignatureWith(validator, 0);
        } else {
            throw new SecurityException(
                    "there are more than one signature or no signature!");
        }
    }

    public boolean verifySignatureWith(Key key) {

        if (this.signatureEntries.size() == 1) {

            return verifySignatureWith(key, 0);
        } else {
            throw new SecurityException(
                    "there are more than one signature or no signature!");
        }
    }

    public boolean verifySignatureWith(JwsVerifier validator, int signatureIndex) {
        boolean critHeader = true;

        if (this.signatureEntries.get(signatureIndex)
                .getUnprotectedHeaderAsUnprotectedHeader() != null)
            if (this.signatureEntries.get(signatureIndex)
                    .getUnprotectedHeaderAsUnprotectedHeader()
                    .get(JoseHeaders.HEADER_CRITICAL) != null)
                throw new SecurityException(
                        "critical header parameter must be integrity protected.");
        if (this.signatureEntries.get(signatureIndex).getProtectedHeader() != null)
            if (!JoseUtils.validateCriticalHeaders(getHeaders(signatureIndex)))
                critHeader = false;

        if (validator.verify(getHeaders(signatureIndex), this
                .getSignatureInput(signatureIndex).getBytes(), JoseUtils
                .base64URLTobytes(this.signatureEntries.get(signatureIndex)
                        .getSignature()))
                && critHeader) {
            return true;

        }

        return false;
    }

    public boolean verifySignatureWith(RSAPublicKey key) {
        if (signatureEntries.size() != 1)

                throw new SecurityException(
                        "there are more than one or no signature entries!");

        else
            return verifySignatureWith(key, 0);
    }


    public boolean verifySignatureWith(SecretKeySpec key) {
        if (signatureEntries.size() != 1)
            throw new SecurityException(
                    "there are more than one or no signature entries!");
        else
            return verifySignatureWith(key, 0);
    }



    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
    }

    public void additionallySign(Key inputKey,
            JwsProtectedHeader protectedHeader,
            JwsUnprotectedHeader unprotectedHeader) {
        // TODO Auto-generated method stub
        String alg = unionHeaders(protectedHeader, unprotectedHeader)
                .getAlgorithm();
        JwsSigner signer = null;
        if (alg.startsWith("HS")) {
            SecretKeySpec key = (SecretKeySpec) inputKey;
            signer = new HmacJwsSigner(key);
        } else if (alg.startsWith("ES")) {
            ECPrivateKey key = (ECPrivateKey) inputKey;
            signer = new EcdsaJwsSigner(key);
        } else if (alg.startsWith("RS")) {
            RSAPrivateKey key = (RSAPrivateKey) inputKey;
            signer = new RsaJwsSigner(key);
        } else if (alg.startsWith("PS")) {
            RSAPrivateKey key = (RSAPrivateKey) inputKey;
            signer = new RsassapssJwsSigner(key);
        } else if (alg.startsWith(JwaAlgorithms.PLAIN_TEXT.getSpecName())) {
            signer = new NoneJwsSigner();
        } else
            throw new SecurityException("no signer detected!");
        additionallySign(signer, protectedHeader, unprotectedHeader);
    }

    public boolean verifySignatureWith(Key inputKey, int signatureIndex) {
        String alg;
        alg = this.signatureEntries.get(signatureIndex).getHeaderAlgorithm();

        JwsVerifier validator = null;
        if (alg.startsWith("HS")) {
            SecretKeySpec key = (SecretKeySpec) inputKey;
            validator = new HmacJwsVerifier(key);
        } else if (alg.startsWith("ES")) {
            ECPublicKey key = (ECPublicKey) inputKey;
            validator = new EcdsaJwsVerifier(key);
        } else if (alg.startsWith("PS")) {
            RSAPublicKey key = (RSAPublicKey) inputKey;
            validator = new RsassapssJwsVerifier(key);
        } else if (alg.startsWith("RS")) {
            RSAPublicKey key = (RSAPublicKey) inputKey;
            validator = new RsaJwsVerifier(key);
        } else if (alg.startsWith(JwaAlgorithms.PLAIN_TEXT.getSpecName())) {
            validator = new NoneJwsVerifier();
        } else
            throw new SecurityException("no verifier detected!");
        return verifySignatureWith(validator, signatureIndex);
    }

}
