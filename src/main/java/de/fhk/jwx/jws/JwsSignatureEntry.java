/*
 * Copyright 2015 Peter Leo Gorski, Luigi Lo Iacono, Daniel Behnam Torkian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fhk.jwx.jws;

import java.util.Map;

import de.fhk.jwx.util.JoseUtils;

/**
 * this class represents a single signature entry in a JWS document.
 *
 */
public class JwsSignatureEntry {

	private String encodedJwsPayload;
    private String encodedProtectedHeader;
    private String encodedSignature;
    private JwsProtectedHeader protectedHeader;
    private JwsUnprotectedHeader unprotectedHeader;
    
    public JwsSignatureEntry(String encodedJwsPayload, JwsProtectedHeader protectedHeader){
    	this(encodedJwsPayload, protectedHeader, null);
    }
    
    public JwsSignatureEntry(String encodedJwsPayload, JwsUnprotectedHeader unprotectedHeader) {
    	this(encodedJwsPayload, null, unprotectedHeader);
    }
    
    public JwsSignatureEntry(String encodedJwsPayload, JwsProtectedHeader protectedHeader, JwsUnprotectedHeader unprotectedHeader){
    	this.setEncodedJwsPayload(encodedJwsPayload);
    	this.protectedHeader = protectedHeader;
    	this.unprotectedHeader = unprotectedHeader;
    	if (this.protectedHeader != null) this.encodedProtectedHeader = protectedHeader.getBase64URLEncodedHeaders(); 
    }
    
    public String getProtectedHeader(){
		return this.encodedProtectedHeader;
    }
    public String getUnprotectedHeader(){
		if (unprotectedHeader!=null)
            return unprotectedHeader.getHeaders();
        else return null;
    }
    public Map<String, Object> getUnprotectedHeaderAsUnprotectedHeader(){
        if (unprotectedHeader!=null) return this.unprotectedHeader.getAllHeaders();
        else return null;
    }
	public String getSignature() {
		return new String(this.encodedSignature);
	}

	public void setSignature(byte[] sign) {
		setSignature(sign, false);	
	}
	public void setSignature(byte[] sign, boolean encoded) {
		if (encoded) this.encodedSignature = new String(sign); 
		else this.encodedSignature = new String(JoseUtils.bytesToBase64URL(sign));	
	}

	public String getEncodedJwsPayload() {
		return encodedJwsPayload;
	}
	public void setEncodedJwsPayload(String encodedPayload) {
		this.encodedJwsPayload = encodedPayload;
	}
    public void setSignatureText(String signatureText) {
        setEncodedSignature(JoseUtils.stringToBase64URL(signatureText));
    }
    
    public void setSignatureBytes(byte[] signatureOctets) {
        setEncodedSignature(JoseUtils.bytesToBase64URLString(signatureOctets));
    }
    private void setEncodedSignature(String sig) {
        this.encodedSignature = sig;
    }
    
    public int sizeOfProtectedHeader(){
        return this.protectedHeader.getNumberOfEntries();
    }
    
    public int sizeOfUnprotectedHeader(){
        return this.unprotectedHeader.getNumberOfEntries();
    }
    public String getProtectedHeaderAlgorithm(){
        return this.protectedHeader.getAlgorithm();
    }

    public String getHeaderAlgorithm() {
        JwsHeaders allHeaders= new JwsHeaders();
        if (protectedHeader!=null) allHeaders.setHeaders(this.protectedHeader.getAllHeaders());
        if (unprotectedHeader!=null) allHeaders.setHeaders(this.unprotectedHeader.getAllHeaders());
        
        return allHeaders.getAlgorithm();
    }

}
