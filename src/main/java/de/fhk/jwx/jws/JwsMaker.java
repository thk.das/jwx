/*
 * Copyright 2015 Peter Leo Gorski, Luigi Lo Iacono, Daniel Behnam Torkian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fhk.jwx.jws;

import java.security.Key;

import com.fasterxml.jackson.databind.JsonNode;

import de.fhk.jwx.jwk.JwkKey;
import de.fhk.jwx.jws.crypto.JwsSigner;

/**
 * this class is able to produce JWS Documents from Payload and Signer oder from
 * a valid JWS Document.
 *
 */
public final class JwsMaker {

    @SuppressWarnings("unused")
	private String payload;
    private JwsDocument jws;
    private JwsProtectedHeader protectedHeader = null;
    private JwsUnprotectedHeader unprotectedHeader = null;

    private JwsMaker(String payload){
        this.payload = new String(payload);
        this.jws = new JwsDocument(payload, null, JwsType.PRODUCER);
    }

    public static JwsDocument generateFromPayload(String payload,
            JwsProtectedHeader protectedHeader, JwkKey key)
            {
        return generateFromPayload(payload, protectedHeader, null, key);
    }

    public static JwsDocument generateFromPayload(String payload,
            JwsUnprotectedHeader unprotectedHeader, JwkKey key)
            {
        return generateFromPayload(payload, null, unprotectedHeader, key);
    }

    public static JwsDocument generateFromPayload(String payload,
            JwsProtectedHeader protectedHeader, JwsSigner signer)
             {
        return generateFromPayload(payload, protectedHeader, null, signer);
    }

    public static JwsDocument generateFromPayload(String payload,
            JwsUnprotectedHeader unprotectedHeader, JwsSigner signer)
             {
        return generateFromPayload(payload, null, unprotectedHeader, signer);
    }
    
    public static JwsDocument generateFromPayload(String payload,
            JwsProtectedHeader protectedHeader,
            Key key)
            {
        return generateFromPayload(payload, protectedHeader,  null, key);
    }
    public static JwsDocument generateFromPayload(String payload,
            JwsUnprotectedHeader unprotectedHeader,
            Key key)
            {
        return generateFromPayload(payload, null, unprotectedHeader, key);
    }
    
    public static JwsDocument generateFromPayload(String payload,
            JwsProtectedHeader protectedHeader,
            JwsUnprotectedHeader unprotectedHeader, Key key)
            {
        JwsMaker primer = new JwsMaker(new String(payload));
        primer.setProtectedHeader(protectedHeader);
        primer.setUnprotectedHeader(unprotectedHeader);
        return primer.sign(key);
    }

    private JwsDocument sign(Key key) {
        this.jws.additionallySign(key, this.protectedHeader,
                this.unprotectedHeader);
        return this.jws;
    }

    public static JwsDocument generateFromPayload(String payload,
            JwsProtectedHeader protectedHeader,
            JwsUnprotectedHeader unprotectedHeader, JwsSigner signer)
            {
        JwsMaker primer = new JwsMaker(new String(payload));
        primer.setProtectedHeader(protectedHeader);
        primer.setUnprotectedHeader(unprotectedHeader);
        return primer.sign(signer);
    }

    public static JwsDocument generateFromPayload(String payload,
            JwsProtectedHeader protectedHeader,
            JwsUnprotectedHeader unprotectedHeader, JwkKey key)
            {
        JwsMaker primer = new JwsMaker(new String(payload));
        primer.setProtectedHeader(protectedHeader);
        primer.setUnprotectedHeader(unprotectedHeader);
        return primer.sign(key);
    }

    public static JwsDocument generateFromJws(String jws)
           {
        return new JwsDocument(jws, null, JwsType.CONSUMER);
    }

    public static JwsDocument generateFromJws(JsonNode jws)
           {
        return new JwsDocument(jws.asText(), null, JwsType.CONSUMER);
    }

    public static JwsDocument generateFromJws(String detachedJws,
            String encodedPayload) {
        return new JwsDocument(detachedJws, encodedPayload, JwsType.CONSUMER);
    }

    public static JwsDocument generateFromJws(JsonNode detachedJws,
            String encodedPayload) {
        return new JwsDocument(detachedJws.asText(), encodedPayload,
                JwsType.CONSUMER);
    }

    private void setProtectedHeader(JwsProtectedHeader protectedHeader) {
        this.protectedHeader = protectedHeader;
    }

    private void setUnprotectedHeader(JwsUnprotectedHeader unprotectedHeader) {
        this.unprotectedHeader = unprotectedHeader;
    }

    private JwsDocument sign(JwsSigner signer) {
        this.jws.additionallySign(signer, this.protectedHeader,
                this.unprotectedHeader);
        return this.jws;

    }

    private JwsDocument sign(JwkKey key){
        
        this.jws.additionallySignWith(key, this.protectedHeader,
                this.unprotectedHeader);
        return this.jws;
    }

}
