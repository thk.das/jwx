/*
 * Copyright 2015 Peter Leo Gorski, Luigi Lo Iacono, Daniel Behnam Torkian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhk.jwx.jwe;

import java.net.URL;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import de.fhk.jwx.JoseHeaders;
import de.fhk.jwx.jwa.JwaAlgorithms;
import de.fhk.jwx.jwk.JwkKey;
import de.fhk.jwx.util.JoseUtils;

public class JweHeaders implements JoseHeaders {
    private ObjectMapper mapper = new ObjectMapper();
    private LinkedHashMap<String, Object> headers = new LinkedHashMap<String, Object>();

    public JweHeaders() {
    }

    public final void setHeader(String key, Object value) {
        this.headers.put(key, value);
    }

    public final void setHeaders(Map<String, Object> headers) {
        this.headers.putAll(headers);
    }

    public final Object getHeader(String key) {
        return headers.get(key);
    }

    public final String getHeaders() throws JsonProcessingException {
        return getHeaders(false);
    }

    public final String getHeaders(boolean prettyPrint) {
        String headers = null;
        if (prettyPrint)
            try {
                headers = mapper.writerWithDefaultPrettyPrinter()
                        .writeValueAsString(this.headers);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        else
            try {
                headers = mapper.writeValueAsString(this.headers);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        return headers;
    }

    public final String getBase64URLEncodedHeaders() {
        String header = null;
        try {
            header = JoseUtils.stringToBase64URL(mapper
                    .writeValueAsString(headers));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return header;
    }

    public final LinkedHashMap<String, Object> getAllHeaders() {
        return this.headers;
    }

    public void setAlgorithm(String alg) {
        setHeader(JoseHeaders.HEADER_ALGORITHM, alg);
    }

    public final void setAlgorithm(JwaAlgorithms alg) {
        setHeader(JoseHeaders.HEADER_ALGORITHM, alg.getSpecName());
    }

    public final String getAlgorithm() {
        return (String) getHeader(JoseHeaders.HEADER_ALGORITHM);
    }

    public final void setContentEncryptionAlgorithm(JwaAlgorithms enc) {
        setHeader(JoseHeaders.JWE_HEADER_CONTENT_ENC_ALGORITHM,
                enc.getSpecName());
    }

    public final void setContentEncryptionAlgorithm(String enc) {
        setHeader(JoseHeaders.JWE_HEADER_CONTENT_ENC_ALGORITHM, enc);
    }

    public final String getContentEncryptionAlgorithm() {
        return (String) getHeader(JoseHeaders.JWE_HEADER_CONTENT_ENC_ALGORITHM);
    }

    public final void setJSONWebKeySet(URL url) {
        setHeader(HEADER_JSON_WEB_KEY_SET_URL, url);
    }

    public final URL getJSONWebKeySet() {
        return (URL) getHeader(JoseHeaders.HEADER_JSON_WEB_KEY_SET_URL);
    }

    public final void setJSONWebKey(JwkKey key) {
        setHeader(HEADER_JSON_WEB_KEY, key);
    }

    public final JwkKey getJSONWebKey() {
        return (JwkKey) getHeader(JoseHeaders.HEADER_JSON_WEB_KEY);
    }

    public final void setKeyId(String kid) {
        setHeader(JoseHeaders.HEADER_KEY_ID, kid);
    }

    public final String getKeyId() {
        return (String) getHeader(JoseHeaders.HEADER_KEY_ID);
    }

    public final void setX509Url(URL x509Url) {
        setHeader(JoseHeaders.HEADER_X509_URL, x509Url);
    }

    public final URL getX509Url() {
        return (URL) getHeader(JoseHeaders.HEADER_X509_URL);
    }

    public final void setX509Chain(List<String> x509Chain) {
        setHeader(JoseHeaders.HEADER_X509_CHAIN, x509Chain);
    }

    @SuppressWarnings("unchecked")
    public final List<String> getX509Chain() {
        return (List<String>) getHeader(JoseHeaders.HEADER_X509_CHAIN);
    }

    public final void setX509Thumbprint(String x509Thumbprint) {
        setHeader(JoseHeaders.HEADER_X509_THUMBPRINT, x509Thumbprint);
    }

    public final String getX509Thumbprint() {
        return (String) getHeader(JoseHeaders.HEADER_X509_THUMBPRINT);
    }

    public final void setX509ThumbprintSHA256(String x509Thumbprint) {
        setHeader(JoseHeaders.HEADER_X509_THUMBPRINT_SHA256, x509Thumbprint);
    }

    public final String getX509ThumbprintSHA256() {
        return (String) getHeader(JoseHeaders.HEADER_X509_THUMBPRINT_SHA256);
    }

    public final void setType(String type) {
        setHeader(JoseHeaders.HEADER_TYPE, type);
    }

    public final String getType() {
        return (String) getHeader(JoseHeaders.HEADER_TYPE);
    }

    public final void setContentType(String type) {
        setHeader(JoseHeaders.HEADER_CONTENT_TYPE, type);
    }

    public final String getContentType() {
        return (String) getHeader(JoseHeaders.HEADER_CONTENT_TYPE);
    }

    public int getNumberOfEntries() {
        return headers.size();
    }

    public boolean contains(Object key) {
        return headers.containsKey(key);
    }

    public void removeHeaderEntry(String key) {
        this.headers.remove(key);
    }

    public void setPbesSalt(String saltinBase64URL) {
        setHeader(JoseHeaders.JWE_HEADER_PBES_SALT, saltinBase64URL);
    }

    public String getPbesSalt() {
        return (String) this.headers.get(JoseHeaders.JWE_HEADER_PBES_SALT);
    }

    public void setPbesIterationCount(int iteration) {
        setHeader(JoseHeaders.JWE_HEADER_PBES_ITERATION_COUNT, (iteration));
    }

    public int getPbesIterationCount() {
        return (int) this.headers
                .get(JoseHeaders.JWE_HEADER_PBES_ITERATION_COUNT);
    }

    public void setEcdhEsEphemeralPublicKey(Object epk) {
        // TODO
        // It MUST contain only public key
        // parameters and SHOULD contain only the minimum JWK parameters
        // necessary to represent the key; other JWK parameters included can be
        // checked for consistency and honored, or they can be ignored.
        setHeader(JoseHeaders.JWE_HEADER_ECDHES_EPHEMERAL_PUBLIC_KEY, epk);
    }

    public Object getEcdhEsEphemeralPublicKey() {
        return this.headers
                .get(JoseHeaders.JWE_HEADER_ECDHES_EPHEMERAL_PUBLIC_KEY);
    }

    public void setEcdhPartyUInfo(String base64UrlApu) {
        setHeader(JoseHeaders.JWE_HEADER_ECDHES_AGREEMENT_PARTYUINFO,
                base64UrlApu);
    }

    public String getEcdhPartyUInfo() {
        return (String) this.headers
                .get(JoseHeaders.JWE_HEADER_ECDHES_AGREEMENT_PARTYUINFO);
    }

    public void setEcdhPartyVInfo(String base64UrlApv) {
        setHeader(JoseHeaders.JWE_HEADER_ECDHES_AGREEMENT_PARTYVINFO,
                base64UrlApv);
    }

    public String getEcdhPartyVInfo() {
        return (String) this.headers
                .get(JoseHeaders.JWE_HEADER_ECDHES_AGREEMENT_PARTYVINFO);
    }

    public void setGcmKwIv(String ivInBase64URL) {
        setHeader(JoseHeaders.JWE_HEADER_GCMKW_KEY_WRAP_INITIALIZATION_VECTOR,
                ivInBase64URL);
    }

    public String getGcmKwIv() {
        return (String) this.headers
                .get(JoseHeaders.JWE_HEADER_GCMKW_KEY_WRAP_INITIALIZATION_VECTOR);
    }

    public void setGcmKwtag(String tagInBase64URL) {
        setHeader(JoseHeaders.JWE_HEADER_GCMKW_KEY_WRAP_AUTHENTICATION_TAG,
                tagInBase64URL);
    }

    public String getGcmKwtag() {
        return (String) this.headers
                .get(JoseHeaders.JWE_HEADER_GCMKW_KEY_WRAP_AUTHENTICATION_TAG);
    }
}