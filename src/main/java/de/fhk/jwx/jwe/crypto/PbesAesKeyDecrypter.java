/*
 * Copyright 2015 Peter Leo Gorski, Luigi Lo Iacono, Daniel Behnam Torkian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fhk.jwx.jwe.crypto;

import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import org.bouncycastle.pqc.math.linearalgebra.ByteUtils;

import de.fhk.jwx.jwa.JwaAlgorithms;
import de.fhk.jwx.jwe.JweHeaders;
import de.fhk.jwx.jwe.JwePerRecipientUnprotectedHeader;
import de.fhk.jwx.jwe.JweRecipient;
import de.fhk.jwx.jwk.JwkKey;
import de.fhk.jwx.util.JoseUtils;

public class PbesAesKeyDecrypter extends AbstractJweKeyDecrypter
{

    private String keyFactoryInstance;
    private String keyDecryptionInstance;
    private JweHeaders headers;
    private int derivedKeyLength;

    public PbesAesKeyDecrypter(JweRecipient recipient)
    {
        JweHeaders headers = recipient.getMergedHeader();

        if (headers.getAlgorithm().equals(JwaAlgorithms.PBES2_HS512_A256KW.getSpecName()))
        {
            keyFactoryInstance = "PBKDF2WithHmacSHA512";
            keyDecryptionInstance = JwaAlgorithms.AES_256_KW.getSpecName();
            derivedKeyLength = 32 * 8;
            
        } else if (headers.getAlgorithm().equals(JwaAlgorithms.PS_SHA_256.getSpecName()))
        {
            keyFactoryInstance = "PBKDF2WithHmacSHA256";
            keyDecryptionInstance = JwaAlgorithms.AES_256_KW.getSpecName();
            derivedKeyLength = 16 * 8;
            
        } else if (headers.getAlgorithm().equals(JwaAlgorithms.PBES2_HS384_A192KW.getSpecName()))
        {
            keyFactoryInstance = "PBKDF2WithHmacSHA384";
            keyDecryptionInstance = JwaAlgorithms.AES_192_KW.getSpecName();
            derivedKeyLength = 24 * 8;
        }
        
        this.headers = headers;
    }

    @Override
    public SecretKeySpec decryptKey(byte[] encryptedSharedSymmetricKey, JwkKey keyDecryptionKey)
    {
        // PBES2 Salt Input
        byte[] saltInput = JoseUtils.base64URLTobytes(headers.getPbesSalt());

        // A Salt Input value containing 8 or more octets MUST be used.
        if (saltInput.length < 8)
        {
            throw new SecurityException("A Salt Input value containing 8 or more octets MUST be used.");
        }

        // salt value computation, (UTF8(Alg) || 0x00 || Salt Input)
        byte[] part1 = ByteUtils.concatenate(headers.getAlgorithm().getBytes(), new byte[]
        { 0x00 });
        byte[] saltValue = ByteUtils.concatenate(part1, saltInput);

        // PBKDF2 iteration count
        // TODO A minimum iteration count of 1000 is RECOMMENDED.
        int iterationCount = headers.getPbesIterationCount();

        SecretKeyFactory factory = null;
        try
        {
            factory = SecretKeyFactory.getInstance(keyFactoryInstance);
        } catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }

        KeySpec keySpec = new PBEKeySpec(new String(JoseUtils.base64URLTobytes((String) keyDecryptionKey.getProperty("k"))).toCharArray(), saltValue,
                iterationCount, derivedKeyLength);

        Key kek = null;

        try
        {
            kek = factory.generateSecret(keySpec);
        } catch (InvalidKeySpecException e)
        {
            e.printStackTrace();
        }

        JwePerRecipientUnprotectedHeader header = new JwePerRecipientUnprotectedHeader();
        header.setAlgorithm(keyDecryptionInstance);
        JweRecipient keyDecryptionRecipient = new JweRecipient(null, null);
        keyDecryptionRecipient.setMergedHeader(header);

        AbstractJweKeyDecrypter keyDecrypter = JweKeyDecrypterAssigning.getJweKeyDecrypter(keyDecryptionRecipient);

        String jwk = "{" + "\"kty\":\"oct\"," + "\"use\":\"enc\"," + "\"k\":\"" + JoseUtils.bytesToBase64URLString(kek.getEncoded()) + "\"}\"";

        JwkKey derJwkKey = new JwkKey(jwk);

        SecretKeySpec decryptedKey = keyDecrypter.decryptKey(encryptedSharedSymmetricKey, derJwkKey);

        return decryptedKey;
    }
}
