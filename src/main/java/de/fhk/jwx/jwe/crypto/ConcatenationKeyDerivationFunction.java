/*
 * Copyright 2015 Peter Leo Gorski, Luigi Lo Iacono, Daniel Behnam Torkian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fhk.jwx.jwe.crypto;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Implementation of 5.8.1 Concatenation Key Derivation Function (Approved
 * Alternative 1) An Approved key derivation function, based on concatenation.
 * 
 * http://csrc.nist.gov/publications/nistpubs/800-56A/SP800-56A_Revision1_Mar08-
 * 2007.pdf NIST SP 800-56A, Recommendation for Pair-Wise Key Establishment
 * Schemes Using Discrete Logarithm Cryptography March, 2007
 */

public class ConcatenationKeyDerivationFunction
{
    // max_hash_inputlen: an integer that indicates the maximum length (in bits)
    // of the bit string(s) input to the hash function.
    private static final long MAX_HASH_INPUTLEN = Long.MAX_VALUE;

    // 2^32-1 = 4294967295
    private static final long UNSIGNED_INT_MAX_VALUE = 4294967295L;
    private static MessageDigest h;

    public ConcatenationKeyDerivationFunction(String hashAlg) throws NoSuchAlgorithmException
    {
        // Auxiliary Function H: an Approved hash function.
        h = MessageDigest.getInstance(hashAlg);
    }

    /**
     * @param z
     *            a byte string that is the shared secret.
     * @param keydatalen
     *            An integer that indicates the length (in bits) of the secret
     *            keying material to be generated
     * @param algorithmID
     *            A bit string that indicates how the derived keying material
     *            will be parsed and for which algorithm(s) the derived secret
     *            keying material will be used. For example, AlgorithmID might
     *            indicate that bits 1-80 are to be used as an 80-bit HMAC key
     *            and that bits 81-208 are to be used as a 128-bit AES key.
     * @param partyUInfo
     *            A bit string containing public information that is required by
     *            the application using this KDF to be contributed by party U to
     *            the key derivation process.
     * @param partyVInfo
     *            PartyVInfo: A bit string containing public information that is
     *            required by the application using this KDF to be contributed
     *            by party V to the key derivation process.
     * @param suppPubInfo
     *            A bit string containing additional, mutually-known public
     *            information.
     * @param suppPrivInfo
     *            A bit string containing additional, mutually-known private
     *            information (for example, a shared secret symmetric key that
     *            has been communicated through a separate channel).
     * @return The bit string DerivedKeyingMaterial of length keydatalen bits
     *         (or an error indicator).
     */
    public byte[] concatKDF(byte[] z, int keyDataLen, byte[] algorithmID, byte[] partyUInfo, byte[] partyVInfo, byte[] suppPubInfo,
            byte[] suppPrivInfo)
    {
        // hashlen: an integer that indicates the length (in bits) of the output
        // of the hash function used to derive blocks of secret keying material.
        int hashLen = h.getDigestLength() * 8;

        // Any scheme attempting to call this key derivation function with
        // keydatalen greater than or equal to hashlen × (2^32 −1) shall
        // output an error indicator and stop without outputting
        // DerivedKeyingMaterial.
        if (keyDataLen >= (long) hashLen * UNSIGNED_INT_MAX_VALUE)
        {
            throw new IllegalArgumentException("keydatalen is too large");
        }

        // only SuppPubInfo and SuppPrivInfo are optional
        if (algorithmID == null || partyUInfo == null || partyVInfo == null)
        {
            throw new ConcatKdfException("Required parameter is null");
        }

        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        // OtherInfo: A bit string equal to the following concatenation:
        // AlgorithmID || PartyUInfo || PartyVInfo {|| SuppPubInfo }{||
        // SuppPrivInfo }

        try
        {
            baos.write(algorithmID);
            baos.write(partyUInfo);
            baos.write(partyVInfo);
            if (suppPubInfo != null)
            {
                baos.write(suppPubInfo);
            }
            if (suppPrivInfo != null)
            {
                baos.write(suppPrivInfo);
            }
        } catch (IOException e)
        {
            e.printStackTrace();
        }

        byte[] otherInfo = baos.toByteArray();

        return kdf(z, keyDataLen, otherInfo);
    }

    /**
     * Function call: kdf (Z, OtherInput), where OtherInput is keydatalen and
     * OtherInfo.
     * 
     * @param z
     *            a byte string that is the shared secret.
     * @param keyDataLen
     *            An integer that indicates the length (in bits) of the secret
     *            keying material to be generated
     * @param otherInfo
     *            OtherInfo: A bit string equal to the following concatenation:
     *            AlgorithmID || PartyUInfo || PartyVInfo {|| SuppPubInfo } {||
     *            SuppPrivInfo }
     * @return The bit string DerivedKeyingMaterial of length keydatalen bits
     *         (or an error indicator).
     */
    private byte[] kdf(byte[] z, int keyDataLen, byte[] otherInfo)
    {
        final byte[] hHash = new byte[keyDataLen];
        int hashLen = h.getDigestLength();

        // 1. reps = ⎡ keydatalen / hashlen⎤
        int reps = keyDataLen / hashLen;

        // 2. If reps > (2^32 −1), then ABORT: output an error indicator and
        // stop.
        if (reps > UNSIGNED_INT_MAX_VALUE)
        {
            throw new IllegalArgumentException("Key derivation failed");
        }

        // 3. Initialize a 32-bit, big-endian bit string counter as 00000001_16.
        final int counter = 1;
        final byte[] counterInBytes = bigEndian32BitCounter(counter);

        // 4. If counter || Z || OtherInfo is more than max_hash_inputlen bits
        // long, then ABORT: output an error indicator and stop.
        if ((counterInBytes.length + z.length + otherInfo.length) * 8 > MAX_HASH_INPUTLEN)
        {
            throw new IllegalArgumentException("Key derivation failed");
        }

        // 5. For i = 1 to reps by 1, do the following:
        for (int i = 0; i <= reps; i++)
        {
            // 5.1 Compute Hash_i = H(counter || Z || OtherInfo).
            h.reset();
            h.update(bigEndian32BitCounter(i + 1));
            h.update(z);
            h.update(otherInfo);

            // 5.2 Increment counter (modulo 232), treating it as an unsigned
            // 32-bit integer.

            final byte[] hashReps = h.digest();

            // 6. Let Hhash be set to Hash_reps if (keydatalen / hashlen) is an
            // integer;
            if (i < reps)
            {
                System.arraycopy(hashReps, 0, hHash, hashLen * i, hashLen);
                // 6. otherwise, let Hhash be set to the (keydatalen mod
                // hashlen) leftmost bits of Hash_reps.
            } else
            {
                System.arraycopy(hashReps, 0, hHash, hashLen * i, keyDataLen % hashLen);
            }
        }

        // 7. Set DerivedKeyingMaterial = Hash1 || Hash2 || … ||
        // Hash_(reps-1) || Hhash.
        return hHash;
    }

    private static byte[] bigEndian32BitCounter(int length)
    {
        final ByteBuffer big_endian_32_bit_counter = ByteBuffer.allocate(4);
        big_endian_32_bit_counter.putInt(length);
        return big_endian_32_bit_counter.array();
    }
}