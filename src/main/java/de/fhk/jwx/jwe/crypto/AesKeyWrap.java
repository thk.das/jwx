/*
 * Copyright 2015 Peter Leo Gorski, Luigi Lo Iacono, Daniel Behnam Torkian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fhk.jwx.jwe.crypto;

import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import de.fhk.jwx.jwe.JoseException;
import de.fhk.jwx.jwe.JweRecipient;
import de.fhk.jwx.jwk.JwkKey;
import de.fhk.jwx.util.JoseUtils;
import de.fhk.jwx.util.JwkUtils;

class AesKeyWrap extends AbstractJweKeyEncrypter
{
    int determinedKeyLength;

    AesKeyWrap(JweRecipient recipient)
    {
        if (recipient.getMergedHeader().getAlgorithm().equals("A128KW")){
            determinedKeyLength = 128;
        }
        else if (recipient.getMergedHeader().getAlgorithm().equals("A192KW")){
            determinedKeyLength = 192;
        }
        else if (recipient.getMergedHeader().getAlgorithm().equals("A256KW")){
            determinedKeyLength = 256;
        }
    }

    public byte[] encryptKey(Key contentEncryptionKey, JwkKey recipientKey)
    {
        int actualKeySize = JoseUtils.base64URLTobytes((String) recipientKey.getProperty("k")).length * 8;
        if (actualKeySize != determinedKeyLength){
            throw new JoseException("Illegal key size " + actualKeySize + " bit.");
        }
        
        Key keyEncryptionKey = (SecretKey) JwkUtils.convertJwkToSecretKeySpec(recipientKey);

        byte[] temp = null;

        Cipher encryptedKey = null;

        try
        {
            encryptedKey = Cipher.getInstance("AESWrap");
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e)
        {
            e.printStackTrace();
        }
        try
        {
            encryptedKey.init(Cipher.WRAP_MODE, keyEncryptionKey);
        } catch (InvalidKeyException e)
        {
            e.printStackTrace();
        }
        try
        {
            temp = encryptedKey.wrap(contentEncryptionKey);
        } catch (InvalidKeyException | IllegalBlockSizeException e)
        {
            e.printStackTrace();
        }
        
        return temp;
    }
}
