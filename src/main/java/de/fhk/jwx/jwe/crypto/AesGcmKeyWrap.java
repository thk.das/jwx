/*
 * Copyright 2015 Peter Leo Gorski, Luigi Lo Iacono, Daniel Behnam Torkian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fhk.jwx.jwe.crypto;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.AlgorithmParameterSpec;

import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.GCMParameterSpec;

import de.fhk.jwx.jwe.JoseException;
import de.fhk.jwx.jwe.JweHeaders;
import de.fhk.jwx.jwe.JweProtectedHeader;
import de.fhk.jwx.jwe.JweRecipient;
import de.fhk.jwx.jwk.JwkKey;
import de.fhk.jwx.util.JoseUtils;
import de.fhk.jwx.util.JwkUtils;

class AesGcmKeyWrap extends AbstractJweKeyEncrypter
{

    private JweProtectedHeader protectedHeader;
    private JweRecipient recipient;
    private int recipientCount;
    private JweCookbookBypass cookbookBypass;
    private int determinedKeyLength;

    AlgorithmParameterSpec spec;

    public AesGcmKeyWrap(JweRecipient recipient, int recipientCount, JweProtectedHeader protectedHeader, JweCookbookBypass cookbookBypass)
    {
        this.recipient = recipient;
        this.recipientCount = recipientCount;
        this.protectedHeader = protectedHeader;
        this.cookbookBypass = cookbookBypass;
        
        if (recipient.getMergedHeader().getAlgorithm().equals("A128GCMKW")){
            determinedKeyLength = 128;
        }
        else if (recipient.getMergedHeader().getAlgorithm().equals("A192GCMKW")){
            determinedKeyLength = 192;
        }
        else if (recipient.getMergedHeader().getAlgorithm().equals("A256GCMKW")){
            determinedKeyLength = 256;
        }
    }

    @Override
    public byte[] encryptKey(Key sharedSymmetricKey, JwkKey recipientKey)
    {

        int actualKeySize = JoseUtils.base64URLTobytes((String) recipientKey.getProperty("k")).length * 8;
        if (actualKeySize != determinedKeyLength){
            throw new JoseException("Illegal key size " + actualKeySize + " bit.");
        }
        
        Key keyEncryptionKey = (SecretKey) JwkUtils.convertJwkToSecretKeySpec(recipientKey);
        
        
        Cipher keyEncrypter = null;

        // Initialization vector
        // generateIV();
        //
        if (cookbookBypass != null)
        {
            // setIV(JoseUtils.base64URLTobytes("KkYT0GX_2jHlfqN_"));
            setIV(JoseUtils.base64URLTobytes(cookbookBypass.getBypassInitializationVectorForKeyEncryption()));
        } else
        {
            generateIV();
        }

        try
        {
            keyEncrypter = Cipher.getInstance("AES/GCM/NoPadding");
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e)
        {
            e.printStackTrace();
        }

        try
        {
            keyEncrypter.init(Cipher.WRAP_MODE, keyEncryptionKey, spec);
        } catch (InvalidKeyException | InvalidAlgorithmParameterException e1)
        {
            e1.printStackTrace();
        }

        byte[] gcmOutput = null;
        try
        {
            gcmOutput = keyEncrypter.wrap(sharedSymmetricKey);
        } catch (InvalidKeyException | IllegalBlockSizeException e)
        {
            e.printStackTrace();
        }

        byte[] ciphertext = new byte[gcmOutput.length - 16];

        System.arraycopy(gcmOutput, 0, ciphertext, 0, gcmOutput.length - 16);

        byte[] authenticationTag = new byte[16];

        System.arraycopy(gcmOutput, gcmOutput.length - 16, authenticationTag, 0, 16);

        if (recipientCount == 1)
        {
            if (protectedHeader == null){
                protectedHeader = new JweProtectedHeader();
            }
            
            protectedHeader.setGcmKwtag(JoseUtils.bytesToBase64URLString(authenticationTag));
            protectedHeader.setGcmKwIv(JoseUtils.bytesToBase64URLString(getIV()));

            if (cookbookBypass != null)
            {
                String enc = protectedHeader.getContentEncryptionAlgorithm();
                protectedHeader.removeHeaderEntry(JweHeaders.JWE_HEADER_CONTENT_ENC_ALGORITHM);
                protectedHeader.setContentEncryptionAlgorithm(enc);
            }
        } else
        {
            recipient.getHeader().setGcmKwtag(JoseUtils.bytesToBase64URLString(authenticationTag));
            recipient.getHeader().setGcmKwIv(JoseUtils.bytesToBase64URLString(getIV()));
        }
        return ciphertext;
    }

    private void generateIV()
    {
        if (spec == null)
        {
            SecureRandom random = new SecureRandom();

            // Use of an Initialization Vector (IV) of size 96 bits is REQUIRED
            // with
            // this algorithm.
            byte iv[] = new byte[12];
            random.nextBytes(iv);

            // The requested size of the Authentication Tag output MUST be 128
            // bits,
            // regardless of the key size.
            this.spec = new GCMParameterSpec(128, iv);
        }
    }

    private byte[] getIV()
    {
        if (spec == null){
            generateIV();
        }
        return ((GCMParameterSpec) spec).getIV();
    }

    private boolean setIV(byte[] iv)
    {
        if (spec == null)
        {
            this.spec = new GCMParameterSpec(128, iv);
            return true;
        }
        return false;
    }
}
