/*
 * Copyright 2015 Peter Leo Gorski, Luigi Lo Iacono, Daniel Behnam Torkian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fhk.jwx.jwe.crypto;

import java.security.Key;

import javax.crypto.SecretKey;

public class JweCookbookBypass
{
    private String ceBase64UrlIv;
    private String keBase64UrlIv;
    private SecretKey cek;
    private Key ecdhEphemeralPublicKey;
    private Key ecdhEphemeralPrivateKey;
    private String pbes2Salt;
    private Integer pbes2IterationCount;

    public JweCookbookBypass()
    {
    }

    public void setBypassInitializationVectorForContentEncryption(String base64UrlIv)
    {
        this.ceBase64UrlIv = base64UrlIv;
    }

    public String getBypassInitializationVectorForContentEncryption()
    {
        return ceBase64UrlIv;
    }

    public void setBypassInitializationVectorForKeyEncryption(String base64UrlIv)
    {
        this.keBase64UrlIv = base64UrlIv;
    }

    public String getBypassInitializationVectorForKeyEncryption()
    {
        return keBase64UrlIv;
    }

    public void setBypassContentEncryptionKey(SecretKey cek)
    {
        this.cek = cek;
    }

    public SecretKey getBypassContentEncryptionKey()
    {
        return cek;
    }

    public Key getBypassEcdhEphemeralPublicKey()
    {
        return ecdhEphemeralPublicKey;
    }

    public void setBypassEcdhEphemeralPublicKey(Key ephemeralPublicKey)
    {
        this.ecdhEphemeralPublicKey = ephemeralPublicKey;
    }

    public Key getBypassEcdhEphemeralPrivateKey()
    {
        return ecdhEphemeralPrivateKey;
    }

    public void setBypassEcdhEphemeralPrivateKey(Key ephemeralPrivateKey)
    {
        this.ecdhEphemeralPrivateKey = ephemeralPrivateKey;
    }

    public String getBypassPbes2Salt()
    {
        return pbes2Salt;
    }

    public void setBypassPbes2Salt(String pbes2Salt)
    {
        this.pbes2Salt = pbes2Salt;
    }

    public Integer getBypassPbes2IterationCount()
    {
        return pbes2IterationCount;
    }

    public void setBypassPbes2IterationCount(int pbes2IterationCount)
    {
        this.pbes2IterationCount = pbes2IterationCount;
    }
}
